<?php
/**
 * File StoreAssignedCompaniesInterface
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Api\Data;

/**
 * Class StoreAssignedCompaniesInterface
 */
interface StoreAssignedCompaniesInterface
{
    /**
     * Relation id
     *
     * @var string
     */
    const RELATION_ID = 'entity_id';

    /**
     * Store id
     *
     * @var string
     */
    const STORE_ID = 'store_id';

    /**
     * Company id
     *
     * @var string
     */
    const COMPANY_ID = 'company_id';

    /**
     * Created at date
     *
     * @var string
     */
    const CREATED_AT = 'created_at';

    /**
     * Updated at date
     *
     * @var string
     */
    const UPDATED_AT = 'updated_at';

    /**
     * Get relation id
     *
     * @return integer|null
     */
    public function getId();

    /**
     * Get store id
     *
     * @return integer
     */
    public function getStoreId();

    /**
     * Get company id
     *
     * @return integer
     */
    public function getCompanyId();

    /**
     * Get created at date
     *
     * @return integer
     */
    public function getCreatedAt();

    /**
     * Get updated at date
     *
     * @return integer
     */
    public function getUpdatedAt();

    /**
     * Set Id.
     *
     * @param integer $id Id
     *
     * @return \Lambda\CompanyAccount\Api\Data\StoreAssignedCompaniesInterface
     */
    public function setId($id);

    /**
     * Set parent company id
     *
     * @param integer $storeId Store id
     *
     * @return \Lambda\CompanyAccount\Api\Data\StoreAssignedCompaniesInterface
     */
    public function setStoreId($storeId);

    /**
     * Set child company id
     *
     * @param integer $companyId Company id
     *
     * @return \Lambda\CompanyAccount\Api\Data\StoreAssignedCompaniesInterface
     */
    public function setCompanyId($companyId);
}
