<?php
/**
 * File CompanyInterface
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Api\Data;

/**
 * Class CompanyInterface
 */
interface CompanyInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{
    /**
     * Company entity type code
     */
    const ENTITY_TYPE_CODE = 'company';

    /**
     * Company entity type code
     */
    const EAV_ENTITY_TYPE_CODE = 'lambda_company';

    /**
     * Company entity type id
     */
    const ENTITY_TYPE_ID = 'entity_type_id';

    /**
     * Company attribute set id
     */
    const ATTRIBUTE_SET_ID = 'attribute_set_id';

    /**
     * Company billing address
     */
    const COMPANY_BILLING_ADDRESS = 'company_billing_address';

    /**
     * Company id
     *
     * @var string
     */
    const COMPANY_ID = 'entity_id';

    /**
     * Parent company id
     *
     * @var string
     */
    const PARENT_COMPANY_ID = 'parent_company_id';

    /**
     * Customer group id
     *
     * @var string
     */
    const CUSTOMER_GROUP_ID = 'customer_group_id';

    /**
     * Allowed store for company
     *
     * @var string
     */
    const STORE = 'store';

    /**
     * Company status
     *
     * @var string
     */
    const STATUS = 'status';

    /**
     * Company name
     *
     * @var string
     */
    const NAME = 'name';

    /**
     * Company street
     *
     * @var string
     */
    const STREET = 'street';

    /**
     * Company city
     *
     * @var string
     */
    const CITY = 'city';

    /**
     * Company postcode
     *
     * @var string
     */
    const POSTCODE = 'postcode';

    /**
     * Company country id
     *
     * @var string
     */
    const COUNTRY_ID = 'country_id';

    /**
     * Company region id
     *
     * @var string
     */
    const REGION_ID = 'region_id';

    /**
     * Company region
     *
     * @var string
     */
    const REGION = 'region';

    /**
     * Company telephone
     *
     * @var string
     */
    const TELEPHONE = 'telephone';

    /**
     * Company relation to salesforce account
     *
     * @var string
     */
    const SALESFORCE_ACCOUNT_ID = 'salesforce_account_id';

    /**
     * Get created at time
     *
     * @var string|null
     */
    const CREATED_AT = 'created_at';

    /**
     * Get updated at time
     *
     * @var string|null
     */
    const UPDATED_AT = 'updated_at';

    /**
     * Company disabled status
     *
     * @var string
     */
    const STATUS_DISABLED = 0;

    /**
     * Company enabled status
     *
     * @var string
     */
    const STATUS_ACTIVE = 1;

    /**
     * Get Id.
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get parent company id
     *
     * @return integer|null
     */
    public function getParentCompanyId();

    /**
     * Get customer group id
     *
     * @return integer|null
     */
    public function getCustomerGroupId();

    /**
     * Get status.
     *
     * @return int|null
     */
    public function getStatus();

    /**
     * Get company name.
     *
     * @return string|null
     */
    public function getName();

    /**
     * Get street.
     *
     * @return string|null
     */
    public function getStreet();

    /**
     * Get city.
     *
     * @return string|null
     */
    public function getCity();

    /**
     * Get postcode.
     *
     * @return string|null
     */
    public function getPostcode();

    /**
     * Get country.
     *
     * @return string|null
     */
    public function getCountryId();

    /**
     * Get country region id.
     *
     * @return string|null
     */
    public function getRegionId();

    /**
     * Get country region.
     *
     * @return string|null
     */
    public function getRegion();

    /**
     * Get telephone.
     *
     * @return string|null
     */
    public function getTelephone();

    /**
     * Get company entity type id
     *
     * @return int|null
     */
    public function getEntityTypeId();

    /**
     * Get attribute set id
     *
     * @return int|null
     */
    public function getAttributeSetId();

    /**
     * Get created at time
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Get updated at time
     *
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Get Salesforce Account Id
     *
     * @return string|null
     */
    public function getSalesforceAccountId();

    /**
     * Set Id.
     *
     * @param integer $id Id
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setId($id);

    /**
     * Set parent company id
     *
     * @param null|integer $parentCompanyId Parent company id
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setParentCompanyId($parentCompanyId);

    /**
     * Set customer group id
     *
     * @param integer $customerGroupId Customer group id
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setCustomerGroupId($customerGroupId);

    /**
     * Set status.
     *
     * @param integer $status Status
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setStatus($status);

    /**
     * Set company name.
     *
     * @param string $name Name
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setName($name);

    /**
     * Set street.
     *
     * @param string $street Street
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setStreet($street);

    /**
     * Set city.
     *
     * @param string $city City
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setCity($city);

    /**
     * Set country.
     *
     * @param string $country Country
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setCountryId($country);
    /**
     * Set region id.
     *
     * @param string $regionId Country
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setRegionId($regionId);
    /**
     * Set region.
     *
     * @param string $region Country
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setRegion($region);

    /**
     * Set postcode.
     *
     * @param string $postcode Postcode
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setPostcode($postcode);

    /**
     * Set telephone.
     *
     * @param string $telephone Telephone
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setTelephone($telephone);

    /**
     * Set company entity type id
     *
     * @param int $entityTypeId
     * @return $this
     */
    public function setEntityTypeId($entityTypeId);

    /**
     * Set attribute set id
     *
     * @param int $attributeSetId
     * @return $this
     */
    public function setAttributeSetId($attributeSetId);

    /**
     * Set created at time
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Set updated at time
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Set Salesforce Account Id
     *
     * @param string|null $salesforceAccountId Salesforce account id
     *
     * @return $this
     */
    public function setSalesforceAccountId($salesforceAccountId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     *
     * @param \Lambda\CompanyAccount\Api\Data\CompanyExtensionInterface $extensionAttributes Extension attributes
     *
     * @return $this
     */
    public function setExtensionAttributes(
        \Lambda\CompanyAccount\Api\Data\CompanyExtensionInterface $extensionAttributes
    );

    /**
     * Set Company billing address object.
     *
     * @param CompanyBillingAddressInterface $billingAddress Company Billing Address
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setCompanyBillingAddress($billingAddress);

    /**
     * Get Company billing address object
     *
     * @return CompanyBillingAddressInterface
     */
    public function getCompanyBillingAddress();
}
