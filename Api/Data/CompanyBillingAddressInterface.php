<?php
/**
 * File CompanyBillingAddressInterface
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Api\Data;

/**
 * Class CompanyBillingAddressInterface
 */
interface CompanyBillingAddressInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    /**
     * Billing Company id
     *
     * @var string
     */
    const BILLING_COMPANY_ID = 'billing_company_id';

    /**
     * Company billing name
     *
     * @var string
     */
    const BILLING_NAME = 'billing_name';

    /**
     * Company billing street
     *
     * @var string
     */
    const BILLING_STREET = 'billing_street';

    /**
     * Company billing city
     *
     * @var string
     */
    const BILLING_CITY = 'billing_city';

    /**
     * Company billing country id
     *
     * @var string
     */
    const BILLING_COUNTRY_ID = 'billing_country_id';

    /**
     * Company billing region id
     *
     * @var string
     */
    const BILLING_REGION_ID = 'billing_region_id';

    /**
     * Company billing region
     *
     * @var string
     */
    const BILLING_REGION = 'billing_region';

    /**
     * Company billing postcode
     *
     * @var string
     */
    const BILLING_POSTCODE = 'billing_postcode';

    /**
     * Company billing telephone
     *
     * @var string
     */
    const BILLING_TELEPHONE = 'billing_telephone';

    /**
     * Company billing email
     *
     * @var string
     */
    const BILLING_EMAIL = 'billing_email';

    /**
     * Company billing email cc
     *
     * @var string
     */
    const BILLING_EMAIL_CC = 'billing_email_cc';

    /**
     * Get Id.
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get company billing id
     *
     * @return integer|null
     */
    public function getCompanyBillingId();

    /**
     * Get company billing name
     *
     * @return string|null
     */
    public function getCompanyBillingName();

    /**
     * Get company billing street
     *
     * @return string|null
     */
    public function getCompanyBillingStreet();

    /**
     * Get company billing city.
     *
     * @return string|null
     */
    public function getCompanyBillingCity();

    /**
     * Get company country id.
     *
     * @return string|null
     */
    public function getCompanyBillingCountryId();

    /**
     * Get company region id.
     *
     * @return int|null
     */
    public function getCompanyBillingRegionId();

    /**
     * Get company billing region.
     *
     * @return string|null
     */
    public function getCompanyBillingRegion();

    /**
     * Get company billing postcode.
     *
     * @return string|null
     */
    public function getCompanyBillingPostcode();

    /**
     * Get company billing telephone.
     *
     * @return string|null
     */
    public function getCompanyBillingTelephone();

    /**
     * Get company billing email.
     *
     * @return string|null
     */
    public function getCompanyBillingEmail();

    /**
     * Get company billing email cc
     *
     * @return string|null
     */
    public function getCompanyBillingEmailCc();

    /**
     * Set Id.
     *
     * @param integer $companyid company billing Id
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyBillingAddressInterface
     */
    public function setCompanyBillingId($companyid);

    /**
     * Set company billing name
     *
     * @param string $billingName company billing name
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyBillingAddressInterface
     */
    public function setCompanyBillingName($billingName);

    /**
     * Set company billing street
     *
     * @param string $billingStreet company billing street
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyBillingAddressInterface
     */
    public function setCompanyBillingStreet($billingStreet);

    /**
     * Set company billing city.
     *
     * @param string $billingCity company billing city
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyBillingAddressInterface
     */
    public function setCompanyBillingCity($billingCity);

    /**
     * Set company billing country.
     *
     * @param string $billingCountryId Company billing country
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyBillingAddressInterface
     */
    public function setCompanyBillingCountryId($billingCountryId);

    /**
     * Set company billing region id.
     *
     * @param integer $billingRegionId Company billing region id
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyBillingAddressInterface
     */
    public function setCompanyBillingRegionId($billingRegionId);

    /**
     * Set company billing region.
     *
     * @param string $billingRegion Company billing region
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyBillingAddressInterface
     */
    public function setCompanyBillingRegion($billingRegion);

    /**
     * Set company billing postcode.
     *
     * @param string $billingPostcode Company billing postcode
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyBillingAddressInterface
     */
    public function setCompanyBillingPostcode($billingPostcode);

    /**
     * Set company billing telephone.
     *
     * @param string $billingTelephone Company billing telephone
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyBillingAddressInterface
     */
    public function setCompanyBillingTelephone($billingTelephone);

    /**
     * Set company billing email.
     *
     * @param string $billingEmail Company billing email
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyBillingAddressInterface
     */
    public function setCompanyBillingEmail($billingEmail);

    /**
     * Set company billing email cc.
     *
     * @param string $billingEmailCc Company billing email cc
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyBillingAddressInterface
     */
    public function setCompanyBillingEmailCc($billingEmailCc);
}
