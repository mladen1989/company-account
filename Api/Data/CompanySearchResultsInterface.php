<?php
/**
 * File CompanySearchResultsInterface
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Class CompanySearchResultsInterface
 */
interface CompanySearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get companies list
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface[]
     */
    public function getItems();

    /**
     * Set companies list
     *
     * @param \Lambda\CompanyAccount\Api\Data\CompanyInterface[] $items Items
     *
     * @return $this
     */
    public function setItems(array $items);
}
