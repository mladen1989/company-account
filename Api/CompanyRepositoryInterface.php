<?php
/**
 * File CompanyRepositoryInterface
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Api;

/**
 * Class CompanyRepositoryInterface
 */
interface CompanyRepositoryInterface
{
    /**
     * Create or update a company account.
     *
     * @param \Lambda\CompanyAccount\Api\Data\CompanyInterface $company Company
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     *
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\InputException
     */
    public function save(\Lambda\CompanyAccount\Api\Data\CompanyInterface $company);

    /**
     * Returns company details.
     *
     * @param integer $companyId Company id
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($companyId);

    /**
     * Returns company details.
     *
     * @param integer $groupId Customer Group Id
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function getByGroupId($groupId);

    /**
     * Returns company details.
     *
     * @param string $companyName Company name
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function getByName($companyName);

    /**
     * Removes company entity and all the related links from the system.
     *
     * @param \Lambda\CompanyAccount\Api\Data\CompanyInterface $company Company
     *
     * @return boolean
     *
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(\Lambda\CompanyAccount\Api\Data\CompanyInterface $company);

    /**
     * Delete a company. Customers belonging to a company are not deleted with this request.
     *
     * @param int $companyId Comapny id
     *
     * @return boolean
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function deleteById($companyId);

    /**
     * Returns the list of companies.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria Search criteria
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanySearchResultsInterface
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}
