<?php
/**
 * File AlignCustomerGroupToCompanyUpdates
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Observer;

use Magento\Framework\Event\{Observer, ObserverInterface};
use Magento\Customer\Model\GroupFactory as CustomerGroupFactory;
use Magento\Customer\Model\ResourceModel\GroupRepository;
use Lambda\CompanyAccount\Helper\CompanyParentHierarchy;
use Lambda\CompanyAccount\Api\Data\CompanyInterface;
use Lambda\CompanyAccount\Api\CompanyRepositoryInterface;

/**
 * Class AlignCustomerGroupToCompanyUpdates
 */
class AlignCustomerGroupToCompanyUpdates implements ObserverInterface
{
    /**
     * Company parent hierarchy helper
     *
     * @var CompanyParentHierarchy
     */
    protected $companyParentHierarchy;

    /**
     * Customer group factory
     *
     * @var CustomerGroupFactory
     */
    protected $customerGroupFactory;

    /**
     * Company repository
     *
     * @var CompanyRepositoryInterface
     */
    protected $companyRepository;

    /**
     * AlignCustomerGroupToCompanyUpdates constructor.
     *
     * @param CompanyParentHierarchy     $companyParentHierarchy Company hierarchy helper
     * @param CustomerGroupFactory       $customerGroupFactory   Customer group model
     * @param CompanyRepositoryInterface $companyRepository      Company repository
     */
    public function __construct(
        CompanyParentHierarchy $companyParentHierarchy,
        CustomerGroupFactory $customerGroupFactory,
        CompanyRepositoryInterface $companyRepository
    ) {
        $this->companyParentHierarchy = $companyParentHierarchy;
        $this->customerGroupFactory = $customerGroupFactory;
        $this->companyRepository = $companyRepository;
    }

    /**
     * Align customer group, after company changes (renaming or parent company change)
     *
     * @param Observer $observer Observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /**
         * Company
         *
         * @var $company CompanyInterface
         */
        $company = $observer->getData('company');
        $customerGroupNameAfterUpdate = $this->companyParentHierarchy->companyBelongsToCustomerGroupName($company);
        $customerGroup = $this->customerGroupFactory->create()->load($customerGroupNameAfterUpdate, 'customer_group_code');
        if (!$customerGroup->getId()) {
            $customerGroup = $this->customerGroupFactory->create();
            $customerGroup->setTaxClassId(GroupRepository::DEFAULT_TAX_CLASS_ID);
            $customerGroup->setCode($customerGroupNameAfterUpdate)
                ->save();
            $customerGroupId = $customerGroup->getId();
        } else {
            $customerGroupId = $customerGroup->getId();
        }
        try {
            $company->setCustomerGroupId($customerGroupId);
            $this->companyRepository->save($company);
        } catch (\Exception $e) {
            //do nothing for now
        }
    }
}
