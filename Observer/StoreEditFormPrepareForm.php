<?php
/**
 * File StoreEditFormPrepareForm
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Observer;

use Lambda\CompanyAccount\Api\Data\CompanyInterface;
use Lambda\CompanyAccount\Model\ResourceModel\StoreAssignedCompanies\CollectionFactory
    as StoreAssignedCompaniesCollection;
use Lambda\CompanyAccount\Model\ResourceModel\Company\CollectionFactory as CompaniesCollection;
use Magento\Backend\Block\System\Store\Edit\Form\Store;

/**
 * Class StoreEditFormPrepareForm
 */
class StoreEditFormPrepareForm implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * Collection of the assignments companies to stores
     *
     * @var StoreAssignedCompaniesCollection
     */
    protected $storeCompaniesCollection;

    /**
     * Companies collection
     *
     * @var CompaniesCollection
     */
    protected $companiesCollection;

    /**
     * Request
     *
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * Message manager
     *
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * StoreEditFormPrepareForm constructor.
     *
     * @param StoreAssignedCompaniesCollection            $storeCompaniesCollection Collection
     * @param CompaniesCollection                         $companiesCollection      Collection
     * @param \Magento\Framework\App\RequestInterface     $request                  Request
     * @param \Magento\Framework\Message\ManagerInterface $messageManager           Message manager
     */
    public function __construct(
        StoreAssignedCompaniesCollection $storeCompaniesCollection,
        CompaniesCollection $companiesCollection,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->storeCompaniesCollection = $storeCompaniesCollection;
        $this->companiesCollection = $companiesCollection;
        $this->request = $request;
        $this->messageManager = $messageManager;
    }

    /**
     * Main observer callable method
     *
     * @param \Magento\Framework\Event\Observer $observer Observer object
     *
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        /**
         * Store
         *
         * @var Store $block
         */
        $block = $observer->getEvent()->getData('block');
        if (is_object($block) && ($block instanceof Store)) {
            $storeActionHiddenField = $block->getForm()->getElement('store_action');
            if ($storeActionHiddenField && 'edit' == $storeActionHiddenField->getData('value')) {
                $form = $block->getForm();
                $form->addFieldset('companies_fieldset', ['legend' => __('Companies Information')])->addField(
                    'companies',
                    'multiselect',
                    [
                        'name' => "companies[]",
                        'label' => __('Company'),
                        'value' => $this->getAssignedCompaniesList(),
                        'values' => $this->getAllCompaniesList(),
                        'required' => false,
                        'disabled' => false,
                    ]
                );
            }
            if ($storeActionHiddenField && 'add' == $storeActionHiddenField->getData('value')) {
                $this->messageManager->addNoticeMessage(
                    __('After Store View creating will be visible block with assigning companies')
                );
            }
        }
    }

    /**
     * Get current store assigned companies list
     *
     * @return array
     */
    protected function getAssignedCompaniesList()
    {
        $currStoreId = $this->request->getParam('store_id');
        if (empty($currStoreId)) {
            return [];
        }
        $collection = $this->storeCompaniesCollection->create();
        return $collection
            ->addFieldToFilter(\Lambda\CompanyAccount\Model\StoreAssignedCompanies::STORE_ID, $currStoreId)
            ->addFieldToSelect(\Lambda\CompanyAccount\Model\StoreAssignedCompanies::COMPANY_ID)
            ->getColumnValues(\Lambda\CompanyAccount\Model\StoreAssignedCompanies::COMPANY_ID);
    }

    /**
     * Get list of all active companies
     *
     * @return array
     */
    protected function getAllCompaniesList()
    {
        $collection = $this->companiesCollection->create();
        return $collection->addFieldToFilter(
            CompanyInterface::STATUS,
            CompanyInterface::STATUS_ACTIVE
        )->addFieldToSelect(
            [
                CompanyInterface::COMPANY_ID,
                CompanyInterface::NAME
            ]
        )->toOptionArray();
    }
}
