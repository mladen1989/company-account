<?php
/**
 * File CompanyDeleteAfter
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Observer;

use Magento\Framework\Event\{Observer, ObserverInterface, ManagerInterface};
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Customer\Api\{CustomerRepositoryInterface, GroupRepositoryInterface};
use Lambda\CompanyAccount\Api\Data\CompanyInterface;
use Lambda\CompanyAccount\Api\CompanyRepositoryInterface;
use Lambda\CompanyAccount\Helper\Data;
use Lambda\CompanyAccount\Model\Company;

/**
 * Class CompanyDeleteAfter
 */
class CompanyDeleteAfter implements ObserverInterface
{
    /**
     * Customer repository
     *
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * Search Criteria Builder
     *
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $criteriaBuilder;

    /**
     * Company repository
     *
     * @var CompanyRepositoryInterface
     */
    protected $companyRepository;

    /**
     * Customer group repository
     *
     * @var GroupRepositoryInterface
     */
    protected $customerGroupRepository;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $eventManager;

    /**
     * CompanyDeleteAfter constructor.
     *
     * @param GroupRepositoryInterface    $customerGroupRepository Customer group repository
     * @param CustomerRepositoryInterface $customerRepository      Customer repository
     * @param SearchCriteriaBuilder       $criteriaBuilder         Search criteria builder
     * @param CompanyRepositoryInterface  $companyRepository       Company repository
     * @param ManagerInterface            $eventManager            Events manager
     */
    public function __construct(
        GroupRepositoryInterface $customerGroupRepository,
        CustomerRepositoryInterface $customerRepository,
        SearchCriteriaBuilder $criteriaBuilder,
        CompanyRepositoryInterface $companyRepository,
        ManagerInterface $eventManager
    ) {
        $this->customerGroupRepository = $customerGroupRepository;
        $this->customerRepository = $customerRepository;
        $this->criteriaBuilder = $criteriaBuilder;
        $this->companyRepository = $companyRepository;
        $this->eventManager = $eventManager;
    }

    /**
     * Observer execute method. Note: this event works for both, frontend and backend sides
     *
     * @param Observer $observer Observer object
     *
     * @return void
     */
    public function execute(Observer $observer): void
    {
        /**
         * Company
         *
         * @var $company Company
         */
        $company = $observer->getData('company');
        try {
            if (!$company->getParentCompanyId()) {
                throw new \Exception('This parent is TOP');
            }
            $parentCompany = $this->companyRepository->get($company->getParentCompanyId());
            $criteria = $this->criteriaBuilder->addFilter(Data::CUSTOMER_ATTRIBUTE_COMPANY_ID, $company->getId(), 'eq')
                ->create();
            $customers = $this->customerRepository->getList($criteria);
            if ($customers->getTotalCount()) {
                foreach ($customers->getItems() as $customer) {
                    /**
                     * Customer
                     *
                     * @var $customer \Magento\Customer\Model\Data\Customer
                     */
                    $customer->setCustomAttribute(Data::CUSTOMER_ATTRIBUTE_COMPANY_ID, $parentCompany->getId());
                    $customer->setGroupId($parentCompany->getCustomerGroupId());
                    $this->customerRepository->save($customer);
                    $this->eventManager->dispatch(
                        'customer_account_edited',
                        ['email' => $customer->getEmail()]
                    );
                }
            }
            $this->moveChildrenCompanies1LevelUpper($company);
            $this->alignCustomerGroupsOfAllChildrenCompanies($company);
        } catch (\Exception $e) {

        }
    }

    /**
     * Move 1 level children of deleting company to 1 level upper
     *
     * @param Company $deletingCompany Deleting Company
     *
     * @return void
     */
    protected function moveChildrenCompanies1LevelUpper(Company $deletingCompany)
    {
        $criteria = $this->criteriaBuilder->addFilter(CompanyInterface::PARENT_COMPANY_ID, $deletingCompany->getId(), 'eq')
            ->create();
        $childCompanies = $this->companyRepository->getList($criteria);
        if (!$childCompanies->getTotalCount()) {
            return;
        }
        foreach ($childCompanies->getItems() as $childCompany) {
            $childCompany->setParentCompanyId($deletingCompany->getParentCompanyId());
            $updatedCompany = $this->companyRepository->save($childCompany);
            $this->eventManager->dispatch(
                'lambda_company_save_after_custom',
                ['company' => $updatedCompany]
            );
        }
    }

    /**
     * Align Customer groups hierarchy names for all children companies
     *
     * @param Company $deletingCompany Company
     *
     * @return void
     */
    protected function alignCustomerGroupsOfAllChildrenCompanies(Company $deletingCompany)
    {
        $deletingCompanyName = $deletingCompany->getName();
        $criteria = $this->criteriaBuilder->addFilter('customer_group_code', '%' . $deletingCompanyName . ' - %', 'like')
            ->create();
        $customerGroups = $this->customerGroupRepository->getList($criteria);
        if (!$customerGroups->getTotalCount()) {
            return;
        }
        foreach ($customerGroups->getItems() as $customerGroup) {
            $newCustomerGroupCode = str_replace($deletingCompanyName . ' - ', '', $customerGroup->getCode());
            $customerGroup->setCode($newCustomerGroupCode);
            $this->customerGroupRepository->save($customerGroup);
        }
    }
}
