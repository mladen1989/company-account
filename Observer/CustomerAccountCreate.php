<?php
/**
 * File CustomerAccountCreate
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Observer;

use Lambda\CompanyAccount\Helper\Data;
use Magento\Framework\Event\{Observer, ManagerInterface, ObserverInterface};
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\GroupFactory as CustomerGroupFactory;
use Magento\Customer\Api\Data\CustomerInterface;
use Lambda\CompanyAccount\Helper\CompanyParentHierarchy;

/**
 * Class CustomerAccountCreate
 */
class CustomerAccountCreate implements ObserverInterface
{
    /**
     * Event manager
     *
     * @var ManagerInterface
     */
    protected $eventManager;

    /**
     * Customer repository
     *
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * Customer group factory
     *
     * @var CustomerGroupFactory
     */
    protected $customerGroupFactory;

    /**
     * Company parent hierarchy helper
     *
     * @var CompanyParentHierarchy
     */
    protected $companyParentHierarchy;

    /**
     * CustomerAccountCreate constructor.
     *
     * @param ManagerInterface            $eventManager           Event manager
     * @param CustomerRepositoryInterface $customerRepository     Customer repository
     * @param CompanyParentHierarchy      $companyParentHierarchy Company hierarchy helper
     * @param CustomerGroupFactory        $customerGroupFactory   Customer group model
     */
    public function __construct(
        ManagerInterface $eventManager,
        CustomerRepositoryInterface $customerRepository,
        CompanyParentHierarchy $companyParentHierarchy,
        CustomerGroupFactory $customerGroupFactory
    ) {
        $this->eventManager = $eventManager;
        $this->customerRepository = $customerRepository;
        $this->companyParentHierarchy = $companyParentHierarchy;
        $this->customerGroupFactory = $customerGroupFactory;
    }

    /**
     * Observer execute
     *
     * @param Observer $observer Observer
     *
     * @return void
     */
    public function execute(Observer $observer): void
    {
        /**
         * Account controller
         *
         * @var \Magento\Customer\Controller\Account\CreatePost $accountController
         */
        $accountController = $observer->getEvent()->getData('account_controller');
        $companyId = $accountController->getRequest()->getParam('company_id');
        /**
         * Customer
         *
         * @var \Magento\Customer\Model\Data\Customer $customerFromObserver
         */
        $customerFromObserver = $observer->getEvent()->getData('customer');
        $customerId = $customerFromObserver->getId();
        if (!$companyId || !$customerId) {
            return;
        }
        try {
            /**
             * Customer (reloaded)
             *
             * @var CustomerInterface $customer
             */
            $customer = $this->customerRepository->getById((int)$customerId);
            $company = $this->companyParentHierarchy->getCompanyById($companyId);
            if (!$company->getId()) {
                throw new \Exception('Company with such id doesn`t exist');
            }
        } catch (\Exception $e) {
            return;
        }

        $customer->setCustomAttribute(Data::CUSTOMER_ATTRIBUTE_COMPANY_ID, $companyId);
        $customerGroupName = $this->companyParentHierarchy->companyBelongsToCustomerGroupName($company);
        $existedCustomerGroup = $this->customerGroupFactory->create()
            ->load($customerGroupName, 'customer_group_code');
        //NOTE: not creating, but assigning into group if it exists
        if ($existedCustomerGroup->getId()) {
            $customer->setGroupId($existedCustomerGroup->getId());
        }
        $this->customerRepository->save($customer);
        $this->eventManager->dispatch(
            'lambda_customer_assign_company_after',
            [
                'customer' => $customer,
                'company_id' => $companyId
            ]
        );
        $this->eventManager->dispatch(
            'lambda_customer_assign_to_group',
            [
                'customer' => $customer,
                'customer_group_id' => $existedCustomerGroup->getId()
            ]
        );
    }
}
