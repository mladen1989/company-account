<?php
/**
 * File BeforeUpdateCustomer.
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Observer\Adminhtml;

use Magento\Framework\Event\{Observer, ObserverInterface};
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Lambda\CompanyAccount\Api\CompanyRepositoryInterface;
use Lambda\CompanyAccount\Helper\Data;
use Lambda\CompanyAccount\Model\CustomerGroup;

/**
 * Class BeforeUpdateCustomer. This class is responsible for add previosly selected customer company
 * (for lambda_adminhtml_update_customer observer, when customer will change company)
 */
class BeforeUpdateCustomer implements ObserverInterface
{
    /**
     * Customer repository
     *
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    public function __construct(
        CustomerRepositoryInterface $customerRepository
    ) {
        $this->customerRepository = $customerRepository;
    }

    /**
     * Update customer observer
     *
     * @param Observer $observer Observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /**
         * Customer
         *
         * @var $customerFromEvent \Magento\Customer\Model\Data\Customer
         */
        $customerFromEvent = $observer->getEvent()->getDataByKey('customer');
        if ($customerId = $customerFromEvent->getId()) {
            $reloadedCustomer = $this->customerRepository->getById($customerId);


            $companyAttributeValue = $reloadedCustomer->getCustomAttribute(Data::CUSTOMER_ATTRIBUTE_COMPANY_ID)
                ? $reloadedCustomer->getCustomAttribute(Data::CUSTOMER_ATTRIBUTE_COMPANY_ID)->getValue()
                : null;

            $existingExtAttributes = $customerFromEvent->getExtensionAttributes();
            $existingExtAttributes->setData('company_before_update', $companyAttributeValue);
            $customerFromEvent->setExtensionAttributes($existingExtAttributes);
        }
    }
}
