<?php
/**
 * File UpdateCustomer. This class is responsible for change customer group for customer, if Admin changed company for
 * customer. If Admin changed company to any value, so customer group will be changed to the one, which belongs to
 * newly applied company.
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Observer\Adminhtml;

use Magento\Framework\Event\{Observer, ObserverInterface};
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Lambda\CompanyAccount\Api\CompanyRepositoryInterface;
use Lambda\CompanyAccount\Helper\Data;
use Lambda\CompanyAccount\Model\CustomerGroup;

/**
 * Class UpdateCustomer
 */
class UpdateCustomer implements ObserverInterface
{
    /**
     * Customer repository
     *
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * Company repository
     *
     * @var CompanyRepositoryInterface
     */
    protected $companyRepository;

    /**
     * Message manager
     *
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * Customer group model
     *
     * @var CustomerGroup
     */
    protected $customerGroup;

    /**
     * UpdateCustomer constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository Customer repository
     * @param CompanyRepositoryInterface  $companyRepository  Company repository
     * @param ManagerInterface            $messageManager     Message manager
     * @param CustomerGroup               $customerGroup      Customer group model
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        CompanyRepositoryInterface $companyRepository,
        ManagerInterface $messageManager,
        CustomerGroup $customerGroup
    ) {
        $this->customerRepository = $customerRepository;
        $this->companyRepository = $companyRepository;
        $this->messageManager = $messageManager;
        $this->customerGroup = $customerGroup;
    }

    /**
     * Update customer observer
     *
     * @param Observer $observer Observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        try {
            /**
             * Customer
             *
             * @var $customerFromEvent \Magento\Customer\Model\Data\Customer
             */
            $customerFromEvent = $observer->getEvent()->getDataByKey('customer');
            /**
             * Customer
             *
             * @var $request RequestInterface
             */
            $request = $observer->getEvent()->getDataByKey('request');
            if ($this->checkDoesCustomerCompanyChanged($request, $customerFromEvent)) {
                $customerCompanyAttribute = $customerFromEvent->getCustomAttribute(Data::CUSTOMER_ATTRIBUTE_COMPANY_ID);
                if (empty($customerCompanyAttribute)) {
                    throw new \Exception('Company attribute changed to default value');
                }
                $company = $this->companyRepository->get($customerCompanyAttribute->getValue());
                if (!$company->getCustomerGroupId()) {//if company doesn`t have customer group
                    throw new \Exception('Company doesn`t belong to any Customer Group');
                }
                $customerGroup = $this->customerGroup->load($company->getCustomerGroupId());
                if (!$customerGroup->getId()) {//if customer group doesn`t exist
                    throw new \Exception('This Customer Group doesn`t exist');
                }
                $customerFromEvent->setGroupId($customerGroup->getId());
                $this->customerRepository->save($customerFromEvent);
                $this->messageManager->addSuccessMessage(
                    __(
                        'You have changed the company to "%1" and group to "%2" for the customer',
                        $company->getName(),
                        $customerGroup->getCustomerGroupCode()
                    )
                );
            }
        } catch (\Exception $e) {
            //Need to undestand how to cover next cases:
            //- case, when company doesn`t have related customer group
            //- case, when company has relation to not existing customer group
//            $customerFromEvent->setGroupId(\Magento\Customer\Model\Group::NOT_LOGGED_IN_ID);
//            $this->customerRepository->save($customerFromEvent);
        }
    }

    /**
     * Check, does customer company attribute was changed
     *
     * @param RequestInterface  $request           Request
     * @param CustomerInterface $customerFromEvent Customer model
     *
     * @return boolean
     */
    protected function checkDoesCustomerCompanyChanged(RequestInterface $request, CustomerInterface $customerFromEvent)
    {
        $companyIdBeforeUpdate = $customerFromEvent->getExtensionAttributes()->__toArray()['company_before_update'] ?? null;
        $customerCompanyIdFromRequest = $request->getParam('customer')['company_id'] ?? null;
        if (empty($companyIdBeforeUpdate)) {
            return false;
        }

        return ((integer)$companyIdBeforeUpdate !== (integer)$customerCompanyIdFromRequest);
    }
}
