<?php
/**
 * File Index
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Block\Profile;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Lambda\CompanyAccount\Helper\{FrontParentCompany, CustomerIsCompanyAdmin, CurrentStoreCompaniesList};

/**
 * Class Index
 */
class Index extends Template
{
    /**
     * Customer Is Admin helper
     *
     * @var CustomerIsCompanyAdmin
     */
    protected $customerIsAdminHelper;

    /**
     * Helper for get list of possible parent companies
     *
     * @var FrontParentCompany
     */
    protected $frontParentCompanyHelper;

    /**
     * Current store companies helper
     *
     * @var CurrentStoreCompaniesList
     */
    protected $companiesListHelper;

    /**
     * Index constructor.
     *
     * @param CurrentStoreCompaniesList $companiesListHelper      Current store companies helper
     * @param FrontParentCompany        $frontParentCompanyHelper Helper for get list of possible parent companies
     * @param CustomerIsCompanyAdmin    $customerIsAdminHelper    Customer is admin helper
     * @param Context                   $context                  Context
     * @param array                     $data                     Data
     */
    public function __construct(
        CurrentStoreCompaniesList $companiesListHelper,
        FrontParentCompany $frontParentCompanyHelper,
        CustomerIsCompanyAdmin $customerIsAdminHelper,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->companiesListHelper = $companiesListHelper;
        $this->customerIsAdminHelper = $customerIsAdminHelper;
        $this->frontParentCompanyHelper = $frontParentCompanyHelper;
    }

    /**
     * List of all sub companies
     *
     * @return array
     */
    public function getCompanies(): array
    {
        $recursivelyAllChildrenCompaniesId = $this->frontParentCompanyHelper->getAllChildrenByParentCompany(
            $this->customerIsAdminHelper->getAdminCompanyId()
        );
        //add parent company
        array_unshift($recursivelyAllChildrenCompaniesId, (string)$this->customerIsAdminHelper->getAdminCompanyId());
        $currentStoreAllCompaniesId = array_keys($this->companiesListHelper->getCurrentStoreCompanies());
        $neededCompaniesIds = [];
        foreach ($recursivelyAllChildrenCompaniesId as $companyId) {
            if (in_array($companyId, $currentStoreAllCompaniesId)) {
                $neededCompaniesIds[] = $companyId;
            }
        }
        return $this->frontParentCompanyHelper->getDetailedCompaniesInfoByIds($neededCompaniesIds);
    }
}
