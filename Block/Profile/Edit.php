<?php
/**
 * File Edit
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Block\Profile;

use Magento\Directory\Model\ResourceModel\Country\CollectionFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Lambda\CompanyAccount\Helper\CustomerIsCompanyAdmin;
use Lambda\CompanyAccount\Model\CompanyFactory;
use \Lambda\CompanyAccount\Model\ResourceModel\Company as CompanyResource;

/**
 * Class Edit
 */
class Edit extends Template
{
    /**
     * Customer Is Admin helper
     *
     * @var CustomerIsCompanyAdmin
     */
    protected $customerIsAdminHelper;

    /**
     * Company model
     *
     * @var CompanyFactory
     */
    protected $companyModel;

    /**
     * Company
     *
     * @var \Lambda\CompanyAccount\Model\Company
     */
    private $adminCompany;

    /**
     * Country collection
     *
     * @var CollectionFactory
     */
    protected $countryCollectionFactory;

    /** @var CompanyResource  */
    private $companyResource;

    /**
     * Index constructor.
     *
     * @param CollectionFactory       $countryCollectionFactory Country collection
     * @param CustomerIsCompanyAdmin  $customerIsAdminHelper    Customer is admin helper
     * @param CompanyFactory          $companyModel             Company model
     * @param Context                 $context                  Context
     * @param array                   $data                     Data
     */
    public function __construct(
        CollectionFactory $countryCollectionFactory,
        CustomerIsCompanyAdmin $customerIsAdminHelper,
        CompanyFactory $companyModel,
        CompanyResource $companyResource,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->countryCollectionFactory = $countryCollectionFactory;
        $this->customerIsAdminHelper = $customerIsAdminHelper;
        $this->companyModel = $companyModel;
        $this->companyResource = $companyResource;
        $this->getCompany();
    }

    /**
     * Get current company data
     *
     * @return void
     */
    private function getCompany(): void
    {
        $companyModel = $this->companyModel->create();
        $this->companyResource->load($companyModel, $this->getRequest()->getParam('id'));
        $this->adminCompany = $companyModel;
    }

    /**
     * Get company id
     *
     * @return integer
     */
    public function getCompanyId(): int
    {
        return (integer)$this->adminCompany->getId();
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): string
    {
        return (string)$this->adminCompany->getName();
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet(): string
    {
        return (string)$this->adminCompany->getStreet();
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity(): string
    {
        return (string)$this->adminCompany->getCity();
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode(): string
    {
        return (string)$this->adminCompany->getPostcode();
    }

    /**
     * Get possible countries list
     *
     * @return array
     */
    public function getCountriesList(): array
    {
        $countries = [];
        foreach ($this->countryCollectionFactory->create()->loadByStore() as $country) {
            /**
             * Country
             *
             * @var \Magento\Directory\Model\Country $country
             */
            if (!$country->getName()) {
                //skip countries without name
                continue;
            }
            $countries[] = [
                'name' => $country->getName(),
                'code' => $country->getCountryId(),
                'selected' => ($country->getCountryId() == $this->getCountryId()) ? true : false
            ];
        }

        return $countries;
    }

    /**
     * Get country id
     *
     * @return string
     */
    public function getCountryId(): string
    {
        return (string)$this->adminCompany->getCountryId();
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone(): string
    {
        return (string)$this->adminCompany->getTelephone();
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return (integer)$this->adminCompany->getStatus();
    }
}
