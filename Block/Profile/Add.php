<?php
/**
 * File Add
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Block\Profile;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Directory\Model\ResourceModel\Country\CollectionFactory;
use Lambda\CompanyAccount\Helper\{FrontParentCompany, CustomerIsCompanyAdmin, CurrentStoreCompaniesList};

/**
 * Class Add
 */
class Add extends Template
{
    /**
     * Customer Is Admin helper
     *
     * @var CustomerIsCompanyAdmin
     */
    protected $customerIsAdminHelper;

    /**
     * Country collection
     *
     * @var CollectionFactory
     */
    protected $countryCollectionFactory;

    /**
     * Helper for get list of possible parent companies
     *
     * @var FrontParentCompany
     */
    protected $frontParentCompanyHelper;

    /**
     * Current store companies helper
     *
     * @var CurrentStoreCompaniesList
     */
    protected $companiesListHelper;

    /**
     * Index constructor.
     *
     * @param CurrentStoreCompaniesList $companiesListHelper      Current store companies helper
     * @param CustomerIsCompanyAdmin    $customerIsAdminHelper    Customer Is Admin helper
     * @param FrontParentCompany        $frontParentCompanyHelper Helper for get list of possible parent companies
     * @param CollectionFactory         $countryCollectionFactory Country collection
     * @param Context                   $context                  Context
     * @param array                     $data                     Data
     */
    public function __construct(
        CurrentStoreCompaniesList $companiesListHelper,
        CustomerIsCompanyAdmin $customerIsAdminHelper,
        FrontParentCompany $frontParentCompanyHelper,
        CollectionFactory $countryCollectionFactory,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->companiesListHelper = $companiesListHelper;
        $this->customerIsAdminHelper = $customerIsAdminHelper;
        $this->frontParentCompanyHelper = $frontParentCompanyHelper;
        $this->countryCollectionFactory = $countryCollectionFactory;
    }

    /**
     * Get possible countries list
     *
     * @return array
     */
    public function getCountriesList(): array
    {
        $countries = [];
        foreach ($this->countryCollectionFactory->create()->loadByStore() as $country) {
            /**
             * Country
             *
             * @var \Magento\Directory\Model\Country $country
             */
            if (!$country->getName()) {
                //skip countries without name
                continue;
            }
            $countries[] = [
                'name' => $country->getName(),
                'code' => $country->getCountryId()
            ];
        }

        return $countries;
    }

    /**
     * List of all companies
     *
     * @return array
     */
    public function getCompanies(): array
    {
        $companiesIds =  $this->frontParentCompanyHelper->getAllChildrenByParentCompany(
            $this->customerIsAdminHelper->getAdminCompanyId()
        );
        //add parent company
        array_unshift($companiesIds, $this->customerIsAdminHelper->getAdminCompanyId());
        $activeCompaniesIds = array_map(function ($activeCompany) {
            return $activeCompany['value'];
        }, $this->companiesListHelper->getCurrentStoreActiveCompanies());

        if (!$activeCompaniesIds) {
            return [];
        }
        $companiesDataIds = [];
        foreach ($companiesIds as $companyId) {
            if (in_array($companyId, $activeCompaniesIds)) {
                $companiesDataIds[] = $companyId;
            }
        }

        return $this->frontParentCompanyHelper->getCompaniesByIds($companiesDataIds);
    }
}
