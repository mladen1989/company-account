<?php
/**
 * File CompaniesList
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Lambda\CompanyAccount\Helper\CurrentStoreCompaniesList;

/**
 * Class CompaniesList
 */
class CompaniesList extends Template
{
    /**
     * Current store companies helper
     *
     * @var CurrentStoreCompaniesList
     */
    protected $companiesListHelper;

    /**
     * Config
     *
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * CompaniesList constructor.
     *
     * @param ScopeConfigInterface      $scopeConfig         Scope config
     * @param CurrentStoreCompaniesList $companiesListHelper Helper
     * @param Context                   $context             Context
     * @param array                     $data                Additional data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        CurrentStoreCompaniesList $companiesListHelper,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->companiesListHelper = $companiesListHelper;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Get companies list
     *
     * @return array
     */
    public function getCompanies(): array
    {
        return $this->companiesListHelper->getCurrentStoreActiveCompanies();
    }

    /**
     * Get Company label from xml arguments or default
     *
     * @return string
     */
    public function getCompanyLabel()
    {
        return (string)$this->scopeConfig->getValue('company_account_config/general/label');
    }

    /**
     * Check, show or no legend block
     *
     * @return boolean
     */
    public function isShowLegend()
    {
        return $this->_data['data']['show_legend'] ?? true;
    }

    /**
     * Get Companies dropdown default label
     *
     * @return \Magento\Framework\Phrase
     */
    public function getDropdownEmptyLabel()
    {
        return __('Please select %1', $this->getCompanyLabel());
    }

    /**
     * Check, do Company field is required
     *
     * @return boolean
     */
    public function isCompanyRequiredField()
    {
        return $this->scopeConfig->isSetFlag('company_account_config/general/is_required');
    }
        /**
     * Check, do Company field is enabled
     *
     * @return boolean
     */
    public function isCompanyEnabledField()
    {
        return $this->scopeConfig->isSetFlag('company_account_config/general/is_enabled');
    }
}
