<?php
/**
 * File Company
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Block\Customer\Account\Edit;

use Lambda\CompanyAccount\Helper\{CurrentStoreCompaniesList, Data, FrontParentCompany};
use Lambda\CompanyAccount\Model\Company as CompanyModel;
use Magento\Framework\View\Element\{Template, Template\Context};
use Magento\Customer\Model\Session;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Message\ManagerInterface;
use Lambda\CompanyAccount\Api\Data\CompanyInterface;

/**
 * Class Company
 */
class Company extends Template
{
    /**
     * Companies list helper
     *
     * @var CurrentStoreCompaniesList
     */
    protected $companiesListHelper;

    /**
     * Customer session
     *
     * @var Session
     */
    protected $customerSession;

    /**
     * Customer repository
     *
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * Company model
     *
     * @var Company
     */
    protected $companyModel;

    /**
     * Message manager
     *
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * Helper for get list of possible parent companies
     *
     * @var FrontParentCompany
     */
    protected $frontParentCompanyHelper;

    /**
     * Company constructor.
     *
     * @param ManagerInterface            $messageManager      Message manager
     * @param CompanyModel                $companyModel        Company model
     * @param Session                     $customerSession     Customer session
     * @param CurrentStoreCompaniesList   $companiesListHelper Companies list helper
     * @param CustomerRepositoryInterface $customerRepository  Customer repository
     * @param Context                     $context             Context
     * @param array                       $data                Data
     */
    public function __construct(
        FrontParentCompany $frontParentCompanyHelper,
        ManagerInterface $messageManager,
        CompanyModel $companyModel,
        Session $customerSession,
        CurrentStoreCompaniesList $companiesListHelper,
        CustomerRepositoryInterface $customerRepository,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->frontParentCompanyHelper = $frontParentCompanyHelper;
        $this->messageManager = $messageManager;
        $this->companyModel = $companyModel;
        $this->customerSession = $customerSession;
        $this->companiesListHelper = $companiesListHelper;
        $this->customerRepository = $customerRepository;
    }

    /**
     * Get list of all companies, assigned to current Store View and marked selected by customer company
     *
     * @return array
     */
    public function getCompaniesList(): array
    {
        $customer = $this->customerRepository->getById($this->customerSession->getCustomerId());
        $customerSelectedCompany = $customer->getCustomAttribute(Data::CUSTOMER_ATTRIBUTE_COMPANY_ID);
        if (!$customerSelectedCompany) {
            return [];
        }
        $companiesIds = $this->frontParentCompanyHelper->getAllChildrenByParentCompany(
            $customerSelectedCompany->getValue()
        );
        //add parent company
        array_unshift($companiesIds, $customerSelectedCompany->getValue());
        $activeCompaniesIds = array_map(function ($activeCompany) {
            return $activeCompany['value'];
        }, $this->companiesListHelper->getCurrentStoreActiveCompanies());
        $companiesList = $this->frontParentCompanyHelper->getCompaniesByIds($companiesIds);
        foreach ($companiesList as $index => $company) {
            $isSelectedByCustomer = 0;
            if ($customerSelectedCompany) {
                $isSelectedByCustomer = ($company['value'] == $customerSelectedCompany->getValue());
            }
            //mark selected by customer company
            $companiesList[$index]['selected'] = $isSelectedByCustomer ? true : false;
            if (!$company['status'] && $isSelectedByCustomer) {
                $companiesList[$index]['name'] .= __(' (disabled)');
                $companiesList[$index]['value'] = '';
            }
            if (!$company['status'] && !$isSelectedByCustomer) {
                unset($companiesList[$index]);
            }
            if (!in_array($company['value'], $activeCompaniesIds)) {
                unset($companiesList[$index]);
            }
        }
        //case, when selected by customer company has been unassigned from current Store View, or is deleted
        if ($customerSelectedCompany
            && (0 != $customerSelectedCompany->getValue())
            && !in_array($customerSelectedCompany->getValue(), array_keys($companiesList))
        ) {
            if (!($company = $this->addNotValidSelectedByCustomerCompanyDetails($customerSelectedCompany->getValue()))
            ) {
                $this->messageManager->addNoticeMessage(
                    __('Your selected Company is no longer exists.')
                );
                return $companiesList;
            }
            $companiesList[$customerSelectedCompany->getValue()] = $company;
        }

        return $companiesList;
    }

    /**
     * Add existing, but, or:
     * - unassigned from current store view
     * - disabled
     * company, or add nothing
     *
     * @param integer $companyId Company Id
     *
     * @return array
     */
    protected function addNotValidSelectedByCustomerCompanyDetails($companyId)
    {
        $company = $this->companyModel->load($companyId);
        if (!$company->getId()) {
            return [];
        }
        $nameForDropdown = $company->getName() . __(' (not available)');
        if (CompanyInterface::STATUS_ACTIVE != $company->getStatus()) {
            $nameForDropdown .= __(' (disabled)');
        }

        return [
            'name' => $nameForDropdown,
            'value' => '',
            'selected' => true,
        ];
    }
}
