<?php
/**
 * File DeleteButton
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Block\Adminhtml\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class DeleteButton
 */
class DeleteButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * Button data
     *
     * @return array
     */
    public function getButtonData()
    {
        if (!$this->getRequest()->getParam('id')) {
            return [];
        }
        $data = [
            'label' => __('Delete Company'),
            'class' => 'delete',
            'id' => 'company-edit-delete-button',
            'data_attribute' => [
                'mage-init' => [
                    'Lambda_CompanyAccount/edit/post-wrapper' => ['url' => $this->getDeleteUrl()],
                ]
            ],
            'on_click' => '',
            'sort_order' => 20,
        ];
        return $data;
    }

    /**
     * Get company delete button
     *
     * @return string
     */
    public function getDeleteUrl()
    {
        $companyId = $this->getRequest()->getParam('id');
        return $this->getUrl('*/*/delete', ['id' => $companyId]);
    }
}
