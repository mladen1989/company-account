<?php
/**
 * File SaveButton
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Block\Adminhtml\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Magento\Ui\Component\Control\Container;

/**
 * Class SaveButton
 */
class SaveButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * Get button data.
     *
     * @return array
     */
    public function getButtonData()
    {
        $data = [
            'label' => __('Save'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => [
                    'buttonAdapter' => [
                        'actions' => [
                            [
                                'targetName' => 'company_form.company_form',
                                'actionName' => 'save',
                                'params' => [
                                    false
                                ]
                            ]
                        ]
                    ]
                ],
                'form-role' => 'save',
            ],
            'sort_order' => 90,
            'class_name' => Container::SPLIT_BUTTON,
            'options' => $this->getOptions()
        ];

        return $data;
    }

    /**
     * Get options.
     *
     * @return array
     */
    // @codingStandardsIgnoreStart
    private function getOptions()
    {// @codingStandardsIgnoreEnd
        return [
            [
                'id_hard' => 'save_and_close',
                'label' => __('Save & Close'),
                'data_attribute' => [
                    'mage-init' => [
                        'buttonAdapter' => [
                            'actions' => [
                                [
                                    'targetName' => 'company_form.company_form',
                                    'actionName' => 'save',
                                    'params' => [
                                        true
                                    ]
                                ]
                            ]
                        ]
                    ],
                ],
            ]
        ];
    }
}
