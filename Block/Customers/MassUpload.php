<?php
/**
 * File MassUpload
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Block\Customers;

use Magento\Framework\View\Element\Template;
use Lambda\CompanyAccount\Helper\Data;

/**
 * Class MassUpload
 */
class MassUpload extends Template
{

    /**
     * Available for import file formats
     *
     * @return string
     */
    public function getAvailableFilesFormatAsString()
    {
        return rtrim(implode(', ', Data::AVAILABLE_FILE_FORMATS), ', ');
    }

    /**
     * Available for import file formats, for input type file, html validation
     *
     * @return string
     */
    public function getAvailableFilesFormatAsStringForInput()
    {
        $result = '';
        foreach (Data::AVAILABLE_FILE_FORMATS as $item) {
            $result .= '.' . $item . ', ';
        }

        return rtrim($result, ', ');
    }
}
