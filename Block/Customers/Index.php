<?php
/**
 * File Index
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Block\Customers;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\UrlInterface;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;
use Lambda\CompanyAccount\Helper\{CustomerIsCompanyAdmin, Data, FrontParentCompany};
use Lambda\CompanyAccount\Model\Company;
use Lambda\CompanyAccount\Model\ResourceModel\Company\CollectionFactory as CompaniesCollection;
use Lambda\CompanyAccount\Api\Data\CompanyInterface;

/**
 * Class Index
 */
class Index extends Template
{
    /**
     * Customer Is Admin helper
     *
     * @var CustomerIsCompanyAdmin
     */
    protected $customerIsAdminHelper;

    /**
     * Collection
     *
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Companies collection
     *
     * @var CompaniesCollection
     */
    protected $companiesCollection;

    /**
     * FrontParent company helper
     *
     * @var FrontParentCompany
     */
    protected $frontParentCompanyHelper;

    /**
     * Url builder
     *
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * Customers constructor.
     *
     * @param CompaniesCollection    $companiesCollection      Companies collection
     * @param CustomerIsCompanyAdmin $customerIsAdminHelper    Customer is admin helper
     * @param FrontParentCompany     $frontParentCompanyHelper FrontParent company helper
     * @param CollectionFactory      $collectionFactory        Customers collection
     * @param UrlInterface           $urlBuilder               Url builder
     * @param Context                $context                  Context
     * @param array                  $data                     Data
     */
    public function __construct(
        CompaniesCollection $companiesCollection,
        CustomerIsCompanyAdmin $customerIsAdminHelper,
        FrontParentCompany $frontParentCompanyHelper,
        CollectionFactory $collectionFactory,
        UrlInterface $urlBuilder,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->companiesCollection = $companiesCollection;
        $this->customerIsAdminHelper = $customerIsAdminHelper;
        $this->frontParentCompanyHelper = $frontParentCompanyHelper;
        $this->collectionFactory = $collectionFactory;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Get list of sub companies, of the admin (parent company)
     *
     * @return array
     */
    private function getCompaniesIds(): array
    {
        $companiesIds = $this->frontParentCompanyHelper->getAllChildrenByParentCompany(
            $this->customerIsAdminHelper->getAdminCompanyId()
        );
        array_push($companiesIds, $this->customerIsAdminHelper->getAdminCompanyId());
        return $companiesIds;
    }

    /**
     * Get companies information, by array of companies ids
     *
     * @param array $companiesIds Companies ids
     *
     * @return array
     */
    protected function getCompaniesDataByIds(array $companiesIds): array
    {
        $companies = [];
        $companiesCollection = $this->companiesCollection->create();
        $companiesData = $companiesCollection->addFieldToSelect([CompanyInterface::COMPANY_ID, CompanyInterface::NAME])
            ->addFieldToFilter(CompanyInterface::COMPANY_ID, ['in' => $companiesIds])
            ->toArray([CompanyInterface::COMPANY_ID, Company::NAME]);
        foreach ($companiesData as $item) {
            $companies[$item[CompanyInterface::COMPANY_ID]] = $item[CompanyInterface::NAME];
        }

        return $companies;
    }

    /**
     * Get List of all customers, assigned into the list of all sub companies
     *
     * @return array
     */
    public function getCustomers(): array
    {
        $result = [];
        $customersCollection = $this->collectionFactory->create();
        $customersCollection->addAttributeToSelect(
            [Data::CUSTOMER_ATTRIBUTE_COMPANY_ID, Data::CUSTOMER_ATTRIBUTE_IS_COMPANY_ADMIN]
        )->addAttributeToFilter(Data::CUSTOMER_ATTRIBUTE_COMPANY_ID, ['in' => $this->getCompaniesIds()])
            ->addFieldToFilter('entity_id', ['neq' => $this->customerIsAdminHelper->getCustomerId()]);
        $companiesData = $this->getCompaniesDataByIds($this->getCompaniesIds());
        foreach ($customersCollection as $customer) {
            /**
             * Customer
             *
             * @var $customer \Magento\Customer\Model\Customer
             */
            $customerData = [
                'id' => $customer->getId(),
                'name' => $customer->getName(),
                'email' => $customer->getEmail(),
                'company' => '',
                'is_admin' => $this->getHumanReadableIsAdminAttriuteValue($customer->getIsCompanyAdmin()),
                'link_edit' => $this->urlBuilder->getUrl(
                    'company/customers/edit',
                    [
                        'id' => $customer->getId()
                    ]
                ),
                'link_delete' => $this->urlBuilder->getUrl(
                    'company/customers/delete',
                    [
                        'id' => $customer->getId()
                    ]
                )
            ];
            if (isset($companiesData[$customer->getCompanyId()])) {
                $customerData['company'] = $companiesData[$customer->getCompanyId()];
            }
            $result[] = $customerData;
        }

        return $result;
    }

    /**
     * Get label value of attribute
     *
     * @param string $value Attribute value
     *
     * @return \Magento\Framework\Phrase
     */
    protected function getHumanReadableIsAdminAttriuteValue($value)
    {
        return !empty($value) && '1' == $value ? __('Yes') : __('No');
    }
}
