<?php
/**
 * File Edit
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Block\Customers;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Lambda\CompanyAccount\Helper\Data;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Lambda\CompanyAccount\Helper\CurrentStoreCompaniesList;

/**
 * Class Edit
 */
class Edit extends Template
{
    /**
     * Customer
     *
     * @var CustomerRepositoryInterface
     */
    protected CustomerRepositoryInterface $customerRepository;

    /**
     * Customer
     *
     * @var null|CustomerInterface
     */
    private ?CustomerInterface $customer;
    private ScopeConfigInterface $scopeConfig;
    private CurrentStoreCompaniesList $companiesListHelper;

    /**
     * Edit constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository Customers collection
     * @param Context                     $context            Context
     * @param array                       $data               Additional data
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        ScopeConfigInterface $scopeConfig,
        CurrentStoreCompaniesList $companiesListHelper,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->customerRepository = $customerRepository;
        $this->companiesListHelper = $companiesListHelper;
        $this->scopeConfig = $scopeConfig;
        $this->getCustomer();
    }

    /**
     * Get customer
     *
     * @return void
     */
    private function getCustomer(): void
    {
        try {
            $customer = $this->customerRepository->getById((integer)$this->getRequest()->getParam('id'));
            $this->customer = $customer;
        } catch (\Exception $e) {
            //Exception case is covered in controller
        }
    }

    /**
     * Check, is customer an admin
     *
     * @return boolean
     */
    public function isAdmin()
    {
        $customerIsAminAttr = $this->customer->getCustomAttribute(Data::CUSTOMER_ATTRIBUTE_IS_COMPANY_ADMIN);
        if ($customerIsAminAttr && $customerIsAminAttr->getValue()) {
            return (boolean)$customerIsAminAttr->getValue();
        }
        return false;
    }

    /**
     * Get Customer Id
     *
     * @return int|null
     */
    public function getCustomerId()
    {
        return $this->customer->getId();
    }

    /**
     * Get companies list
     *
     * @return array
     */
    public function getCompanies(): array
    {
        return $this->companiesListHelper->getCurrentStoreActiveCompanies();
    }

    /**
     * Get Company label from xml arguments or default
     *
     * @return string
     */
    public function getCompanyLabel()
    {
        return (string)$this->scopeConfig->getValue('company_account_config/general/label');
    }

    /**
     * Check, show or no legend block
     *
     * @return boolean
     */
    public function isShowLegend()
    {
        return $this->_data['data']['show_legend'] ?? true;
    }

    /**
     * Get Companies dropdown default label
     *
     * @return \Magento\Framework\Phrase
     */
    public function getDropdownEmptyLabel()
    {
        return __('Remove from assigned %1', $this->getCompanyLabel());
    }

    /**
     * Check, do Company field is required
     *
     * @return boolean
     */
    public function isCompanyRequiredField()
    {
        return $this->scopeConfig->isSetFlag('company_account_config/general/is_required');
    }
}
