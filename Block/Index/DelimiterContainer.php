<?php
/**
 * File DelimiterContainer
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Block\Index;

use Magento\Customer\Block\Account\SortLinkInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Lambda\CompanyAccount\Helper\CustomerIsCompanyAdmin;

/**
 * Class Delimiter
 */
class DelimiterContainer extends Template implements SortLinkInterface
{
    /**
     * Customer Is Admin helper
     *
     * @var CustomerIsCompanyAdmin
     */
    protected $customerIsAdminHelper;

    /**
     * Flag for show/hide link
     *
     * @var boolean
     */
    protected $linkShouldBeVisible = false;

    /**
     * DelimiterContainer constructor.
     *
     * @param CustomerIsCompanyAdmin $customerIsAdminHelper Customer Is Admin helper
     * @param Context                $context               Context
     * @param array                  $data                  Data
     */
    public function __construct(
        CustomerIsCompanyAdmin $customerIsAdminHelper,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->customerIsAdminHelper = $customerIsAdminHelper;
        if ($customerIsAdminHelper->getAdminCompanyId()) {
            $this->linkShouldBeVisible = true;
        }
    }

    /**
     * If at least one child has HTML body we return it.
     *
     * @return string
     */
    // @codingStandardsIgnoreStart
    protected function _toHtml(): string
    {// @codingStandardsIgnoreEnd
        $html = '';
        if (!$this->linkShouldBeVisible) {
            return $html;
        }
        foreach ($this->getChildNames() as $childName) {
            if ($this->getChildHtml($childName) != '') {
                $html = $this->getChildHtml($childName);
            }
        }

        return $html;
    }

    /**
     * Sort order
     *
     * @return mixed
     */
    public function getSortOrder()
    {
        return $this->getData(self::SORT_ORDER);
    }
}
