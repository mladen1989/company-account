<?php
/**
 * File SortLink
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Block\Index;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Html\Link\Current;
use Magento\Framework\App\DefaultPathInterface;
use Magento\Customer\Model\Session;
use Lambda\CompanyAccount\Helper\CustomerIsCompanyAdmin;
use Lambda\CompanyAccount\Api\CompanyRepositoryInterface;
use \Psr\Log\LoggerInterface;

/**
 * Class SortLink
 */
class SortLink extends Current implements SortLinkInterface
{
    /**
     * Customer Is Admin helper
     *
     * @var CustomerIsCompanyAdmin
     */
    protected $customerIsAdminHelper;

    /**
     * Logger
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Customer session
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * Scope config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Flag for show/hide link
     *
     * @var boolean
     */
    protected $linkShouldBeVisible = false;

    /**
     * Company repository
     *
     * @var CompanyRepositoryInterface
     */
    protected $companyRepository;

    /**
     * Position
     *
     * @return mixed
     */
    public function getSortOrder()
    {
        return $this->getData(self::SORT_ORDER);
    }

    /**
     * SortLink constructor
     *
     * @param CompanyRepositoryInterface $companyRepository     Company repository
     * @param CustomerIsCompanyAdmin     $customerIsAdminHelper Customer Is Admin helper
     * @param LoggerInterface            $logger                Logger
     * @param Session                    $customerSession       Customer session
     * @param Context                    $context               Context
     * @param DefaultPathInterface       $defaultPath           Default path
     * @param array                      $data                  Data
     */
    public function __construct(
        CompanyRepositoryInterface $companyRepository,
        CustomerIsCompanyAdmin $customerIsAdminHelper,
        LoggerInterface $logger,
        Session $customerSession,
        Context $context,
        DefaultPathInterface $defaultPath,
        array $data = []
    ) {
        $this->companyRepository = $companyRepository;
        $this->customerIsAdminHelper = $customerIsAdminHelper;
        $this->logger = $logger;
        $this->customerSession = $customerSession;
        $this->scopeConfig = $context->getScopeConfig();
        if (0 != $customerIsAdminHelper->getAdminCompanyId()) {
            $this->linkShouldBeVisible = true;
        }
        parent::__construct($context, $defaultPath, $data);
    }

    /**
     * Show/hide section by condition
     *
     * @return string
     */
    // @codingStandardsIgnoreStart
    protected function _toHtml()
    {// @codingStandardsIgnoreEnd
        try {
            if (!$companyId = $this->customerIsAdminHelper->getAdminCompanyId()) {
                throw new \Exception('Customer have no selected Company');
            }
            $this->companyRepository->get($companyId);
        } catch (\Exception $e) {
            $this->linkShouldBeVisible = false;
        }
        if ($this->linkShouldBeVisible) {
            return parent::_toHtml();
        }
        return '';
    }

    /**
     * Get current customer id or null
     *
     * @return integer|null
     */
    protected function getCustomerId()
    {
        return $this->customerSession->getCustomerId();
    }
}
