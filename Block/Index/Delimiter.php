<?php
/**
 * File Delimiter
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Block\Index;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;

/**
 * Class Delimiter
 */
class Delimiter extends Template
{
    /**
     * Resources
     *
     * @var array
     */
    private $resources;

    /**
     * CompanyLink constructor.
     *
     * @param Context $context Context
     * @param array   $data    Data
     */
    public function __construct(
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->resources = isset($data['resources']) && is_array($data['resources'])
            ? array_values($data['resources'])
            : [];
    }
}
