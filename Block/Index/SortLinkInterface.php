<?php
/**
 * File SortLinkInterface
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Block\Index;

/**
 * Class CompaniesList
 */
interface SortLinkInterface extends \Magento\Customer\Block\Account\SortLinkInterface
{
}
