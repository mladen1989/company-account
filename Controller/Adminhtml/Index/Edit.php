<?php
/**
 * File Edit
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Controller\Adminhtml\Index;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use Lambda\CompanyAccount\Controller\Adminhtml\Index;

/**
 * Class Edit
 */
class Edit extends Index implements HttpGetActionInterface
{
    /**
     * {@inheritdoc}
     */
    // @codingStandardsIgnoreStart
    protected $_publicActions = ['edit'];
    // @codingStandardsIgnoreEnd

    /**
     * Edit company action.
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /**
         * Result page
         *
         * @var \Magento\Backend\Model\View\Result\Page $resultPage
         */
        $resultPage = $this->resultPageFactory->create();
        $companyId = $this->getRequest()->getParam('id');
        try {
            $company = $this->companyRepository->get($companyId);
            $resultPage->setActiveMenu('Lambda_CompanyAccount::company_account');
            $resultPage->getConfig()->getTitle()->prepend($company->getName());
        } catch (\Exception $e) {
            $this->messageManager->addError(__('The requested company is not found'));
            return $this->resultRedirectFactory->create()->setPath('*/*/index');
        }
        return $resultPage;
    }
}
