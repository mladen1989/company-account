<?php
/**
 * File Delete
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Controller\Adminhtml\Index;

use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Lambda\CompanyAccount\Api\CompanyRepositoryInterface;
use Lambda\CompanyAccount\Controller\Adminhtml\Index;

/**
 * Class Delete
 */
class Delete extends Index implements HttpPostActionInterface
{
    /**
     * Raw result controller
     *
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $resultRawFactory;

    /**
     * Url builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * Logger
     *
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Lambda\ContractCode\Api\ContractRepositoryInterface
     */
    protected $contractRepository;

    /**
     * Delete constructor.
     *
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory Result raw conrtoller factory
     * @param \Magento\Backend\App\Action\Context $context Context
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory Result forward factory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory Result page factory
     * @param CompanyRepositoryInterface $companyRepository Company repository
     * @param \Psr\Log\LoggerInterface $logger Logger
     */
    public function __construct(
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Backend\App\Action\Context $context,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        CompanyRepositoryInterface $companyRepository,
        \Psr\Log\LoggerInterface $logger,
        \Lambda\ContractCode\Api\ContractRepositoryInterface $contractRepository
    ) {
        $this->resultRawFactory = $resultRawFactory;
        $this->urlBuilder = $context->getUrl();
        $this->logger = $logger;
        $this->contractRepository = $contractRepository;
        parent::__construct(
            $context,
            $resultForwardFactory,
            $resultPageFactory,
            $companyRepository
        );
    }

    /**
     * Delete company.
     *
     * @return \Magento\Framework\Controller\Result\Raw
     */
    public function execute()
    {
        $result = [];
        $id = $this->getRequest()->getParam('id');

        if ($id) {
            try {
                $company = $this->companyRepository->get($id);

                if (!$this->contractRepository->ifCompanyExistsInContracts($company->getId())) {

                    $this->companyRepository->deleteById($id);
                    $this->messageManager->addSuccessMessage(
                        __(
                            'You have deleted company %companyName.',
                            ['companyName' => $company ? $company->getName() : '']
                        )
                    );
                    $result = ['url' => $this->urlBuilder->getUrl('company/index')];
                    $this->logger->info(
                        __('Company "%1", with id %2, has been removed', $company->getName(), $company->getId())
                    );
                } else {
                    $this->messageManager->addErrorMessage(__('You cannot delete this company. Company "'.$company->getName().'" is used in Contracts'));
                    $result = ['url' => $this->urlBuilder->getUrl('company/index')];
                }

            } catch (NoSuchEntityException $e) {
                $this->messageManager->addErrorMessage(__('The company no longer exists.'));
                $result = ['url' => $this->urlBuilder->getUrl('company/*/')];
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage(__($e->getMessage()));
                $result = ['url' => $this->urlBuilder->getUrl('company/edit', ['id' => $id])];
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('Something went wrong. Please try again later.'));
                $result = ['url' => $this->urlBuilder->getUrl('company/edit', ['id' => $id])];
                $this->logger->critical($e);
            }
        }

        /**
         * Raw response
         *
         * @var \Magento\Framework\Controller\Result\Raw $response
         */
        $response = $this->resultRawFactory->create();
        $response->setHeader('Content-type', 'text/plain');
        $response->setContents(json_encode($result));

        return $response;
    }
}
