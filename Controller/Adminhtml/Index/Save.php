<?php
/**
 * File Save
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */
namespace Lambda\CompanyAccount\Controller\Adminhtml\Index;

use Lambda\CompanyAccount\Helper\EmailSender;
use Lambda\CompanyAccount\Helper\GetConfigValue;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Customer\Model\GroupFactory as CustomerGroupFactory;
use Magento\Customer\Model\ResourceModel\GroupRepository;
use Magento\Customer\Api\GroupRepositoryInterface;
use Lambda\CompanyAccount\Api\Data\CompanyInterface;
use Lambda\CompanyAccount\Api\Data\CompanyInterfaceFactory;
use Lambda\CompanyAccount\Api\Data\CompanyBillingAddressInterface;
use Lambda\CompanyAccount\Api\CompanyRepositoryInterface;
use Lambda\CompanyAccount\Helper\CompanyParentHierarchy;
use Lambda\CompanyAccount\Helper\FrontParentCompany;
use Lambda\CompanyAccount\Api\Data\CompanyBillingAddressInterfaceFactory;
use Lambda\CompanyAccount\Model\ResourceModel\StoreAssignedCompanies\CollectionFactory as StoreAssignedCompaniesCollection;
use Lambda\CompanyAccount\Model\StoreAssignedCompaniesFactory;
use Magento\Framework\Api\AttributeInterfaceFactory;

/**
 * Save CompanyAccount controller
 */
class Save extends Action implements HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session.
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Lambda_CompanyAccount::company_account';

    /**
     * Data object processor
     *
     * @var DataObjectProcessor
     */
    private $dataObjectProcessor;

    /**
     * Company factory
     *
     * @var CompanyInterfaceFactory
     */
    private $companyDataFactory;

    /**
     * Company repository
     *
     * @var CompanyRepositoryInterface
     */
    private $companyRepository;

    /**
     * Data object helper
     *
     * @var DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * Customer group factory
     *
     * @var CustomerGroupFactory
     */
    protected $customerGroupFactory;

    /**
     * Company parent hierarchy helper
     *
     * @var CompanyParentHierarchy
     */
    protected $companyParentHierarchy;

    /**
     * Search criteria builder
     *
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * Helper for get list of possible parent companies
     *
     * @var FrontParentCompany
     */
    protected $frontParentCompanyHelper;

    /**
     * Customer group repository
     *
     * @var GroupRepositoryInterface
     */
    protected $groupRepository;

    /** @var StoreAssignedCompaniesCollection */
    private $storeCollectionFactory;

    /** @var StoreAssignedCompaniesFactory */
    private $storeAssignedCompaniesFactory;

    /** @var CompanyBillingAddressInterfaceFactory */
    private $companyBillingAddressFactory;

    private EmailSender $emailSender;
    private $getConfig;

    /** @var AttributeInterfaceFactory  */
    private $attributeInterfaceFactory;

    /**
     * Save constructor.
     *
     * @param Context $context Context
     * @param DataObjectProcessor $dataObjectProcessor Data object processor
     * @param CompanyRepositoryInterface $companyRepository Company repository
     * @param CompanyInterfaceFactory $companyDataFactory Company factory
     * @param DataObjectHelper $dataObjectHelper Helper
     * @param CustomerGroupFactory $customerGroupFactory Customer group model
     * @param CompanyParentHierarchy $companyParentHierarchy Company parent hierarchy helper
     * @param SearchCriteriaBuilder $searchCriteriaBuilder Search criteria
     * @param FrontParentCompany $frontParentCompanyHelper Helper for get list of possible parent companies
     * @param GroupRepositoryInterface $groupRepository Customer group repository
     * @param CompanyBillingAddressInterfaceFactory $companyBillingAddressFactory
     * @param EmailSender $emailSender
     * @param GetConfigValue $getConfig
     * @param StoreAssignedCompaniesCollection $storeCollectionFactory
     * @param StoreAssignedCompaniesFactory $storeAssignedCompaniesFactory
     * @param AttributeInterfaceFactory $attributeInterfaceFactory
     */
    public function __construct(
        Context $context,
        DataObjectProcessor $dataObjectProcessor,
        CompanyRepositoryInterface $companyRepository,
        CompanyInterfaceFactory $companyDataFactory,
        DataObjectHelper $dataObjectHelper,
        CustomerGroupFactory $customerGroupFactory,
        CompanyParentHierarchy $companyParentHierarchy,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FrontParentCompany $frontParentCompanyHelper,
        GroupRepositoryInterface $groupRepository,
        CompanyBillingAddressInterfaceFactory $companyBillingAddressFactory,
        EmailSender $emailSender,
        GetConfigValue $getConfig,
        StoreAssignedCompaniesCollection $storeCollectionFactory,
        StoreAssignedCompaniesFactory $storeAssignedCompaniesFactory,
        AttributeInterfaceFactory $attributeInterfaceFactory
    ) {
        parent::__construct($context);
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->companyRepository = $companyRepository;
        $this->companyDataFactory = $companyDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->customerGroupFactory = $customerGroupFactory;
        $this->companyParentHierarchy = $companyParentHierarchy;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->frontParentCompanyHelper = $frontParentCompanyHelper;
        $this->groupRepository = $groupRepository;
        $this->companyBillingAddressFactory = $companyBillingAddressFactory;
        $this->emailSender = $emailSender;
        $this->getConfig = $getConfig;
        $this->storeCollectionFactory = $storeCollectionFactory;
        $this->storeAssignedCompaniesFactory = $storeAssignedCompaniesFactory;
        $this->attributeInterfaceFactory = $attributeInterfaceFactory;
    }

    /**
     * Store Customer Group Data to session.
     *
     * @param CompanyInterface $company Company
     *
     * @return void
     */
    private function storeCompanyDataToSession(CompanyInterface $company)
    {
        $companyData = $this->dataObjectProcessor->buildOutputDataArray(
            $company,
            CompanyInterface::class
        );
        $this->_getSession()->setCompanyData($companyData);
    }

    /**
     * Create or save customer group.
     *
     * @return Redirect
     */
    public function execute()
    {
        /**
         * Company
         *
         * @var CompanyInterface $company
         */
        $company = null;
        $request = $this->getRequest();
        $params = $request->getParams();

        if(empty($params['companies_relation']['parent_company_id'])) {
            $email = $this->getConfig->getConfig('company_account_config/email/emailTo');
            $template = $this->getConfig->getConfig('company_account_config/email/template');
            if ($email!=NULL){
                try {
                    $this->emailSender->sendEmail($email, $params['general']['name'], $template);
                } catch (\Exception $e) {
                    $this->messageManager->addExceptionMessage($e, __('Something went wrong with email notification. Try again later.'));
                }
            }
        }

        $id = $request->getParam('id') ? $request->getParam('id') : null;
        try {
            $company = $this->saveCompany($id);

            // update allowed stores for company
            $storeIds = $params['general']['store'];
            $this->removeCompanyToStoreAssignments($company->getId());
            if ($storeIds != "") {
                $this->createCompanyToStoreAssignments($company->getId(), $storeIds);
            }
            // end update allowed stores for company

            $this->messageManager->addSuccessMessage(
                $id
                    ? __('You have saved company %1.', $company->getName())
                    : __('You have created company %1.', $company->getName())
            );
            $returnToEdit = (bool)$this->getRequest()->getParam('back', false);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $returnToEdit = true;
            $this->messageManager->addErrorMessage($e->getMessage());
            if ($company instanceof CompanyInterface) {
                $this->storeCompanyDataToSession($company);
            }
        } catch (\Exception $e) {
            $returnToEdit = true;
            $this->messageManager->addExceptionMessage($e, __('Something went wrong. Please try again later.'));
            if ($company instanceof CompanyInterface) {
                $this->storeCompanyDataToSession($company);
            }
        }
        return $this->getRedirect($returnToEdit, $company);
    }

    /**
     * Get redirect object depending on $returnToEdit and is company new.
     *
     * @param boolean               $returnToEdit Flag
     * @param CompanyInterface|null $company      Company
     *
     * @return Redirect
     */
    private function getRedirect($returnToEdit, CompanyInterface $company = null)
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($returnToEdit) {
            if (($company != null) && $company->getId()) {
                $resultRedirect->setPath(
                    'company/index/edit',
                    ['id' => $company->getId()]
                );
            } else {
                $resultRedirect->setPath(
                    'company/index/new'
                );
            }
        } else {
            $resultRedirect->setPath('company/index');
        }
        return $resultRedirect;
    }

    /**
     * Filter request to get just list of fields.
     *
     * @return array
     */
    private function extractData()
    {
        $allFormFields = [
            CompanyInterface::COMPANY_ID,
            CompanyInterface::ENTITY_TYPE_ID,
            CompanyInterface::ATTRIBUTE_SET_ID,
            CompanyInterface::PARENT_COMPANY_ID,
            CompanyInterface::STATUS,
            CompanyInterface::NAME,
            CompanyInterface::STREET,
            CompanyInterface::CITY,
            CompanyInterface::POSTCODE,
            CompanyInterface::COUNTRY_ID,
            CompanyInterface::REGION_ID,
            CompanyInterface::REGION,
            CompanyInterface::TELEPHONE,
            'extension_attributes',
        ];
        $result = [];
        $request = $this->getRequest()->getParams();
        unset($request['use_default']);
        if (is_array($request)) {
            foreach ($request as $fields) {
                if (!is_array($fields)) {
                    continue;
                }
                $result = array_merge_recursive($result, $fields);
            }
        }
        $result = array_intersect_key($result, array_flip($allFormFields));
        return $result;
    }

    /**
     * Create/load company, set request data, set default role for a new company.
     *
     * @param integer $id Company id
     *
     * @return CompanyInterface
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function saveCompany($id): CompanyInterface
    {
        $companyRenamed = false;
        $parentCompanyChanged = false;
        $childCompaniesIds = [];
        $data = $this->extractData();
        if ($id !== null) {
            $company = $this->companyRepository->get((integer)$id);
            $requestGeneralGroup = $this->getRequest()->getParam('general');
            $requestCompanyName = isset($requestGeneralGroup['name']) ? $requestGeneralGroup['name'] : null;
            if ($requestCompanyName != $company->getName()) {
                $companyRenamed = true;
            }
            $requestCompaniesRelationGroup = $this->getRequest()->getParam('companies_relation');
            $requestParentCompanyId = !empty($requestCompaniesRelationGroup['parent_company_id'])
                ? $requestCompaniesRelationGroup['parent_company_id']
                : null;
            if ($company->getParentCompanyId() != $requestParentCompanyId) {
                $parentCompanyChanged = true;
                $childCompaniesIds = $this->frontParentCompanyHelper->getAllChildrenByParentCompany(
                    $company->getId()
                );
                $this->resetCompanyParentIdWhereCurrentCompanyIsParentByCondition(
                    $company,
                    $childCompaniesIds,
                    $requestParentCompanyId
                );
            }
        } else {
            $company = $this->companyDataFactory->create();
        }
        $this->setCompanyRequestData($company, $data);

        // set company custom attributes
        $companyCustomAttributes = $this->getRequest()->getParam('company_custom_attributes');
        if ($companyCustomAttributes) {
            $attributes = $this->validateCompanyCustomAttributes($companyCustomAttributes);
            $company->setCustomAttributes($attributes);
        }

        $this->setCompanyBillingAddress($company);
        $savedCompany = $this->companyRepository->save($company);
        if ($id === null) {
            if ($customerGroup = $this->createCustomerGroupForCreatedCompany($savedCompany)) {
                $savedCompany->setCustomerGroupId($customerGroup);
                $savedCompany = $this->companyRepository->save($savedCompany);
            }
        }
        if ($companyRenamed || $parentCompanyChanged) {
            $customerGroup = $this->groupRepository->getById($savedCompany->getCustomerGroupId());
            $customerGroup->setCode($this->companyParentHierarchy->companyBelongsToCustomerGroupName($savedCompany));
            $this->groupRepository->save($customerGroup);
            if ($companyRenamed) {
                $childCompaniesIds = $this->frontParentCompanyHelper->getAllChildrenByParentCompany(
                    $savedCompany->getId()
                );
            }
            $this->renameCustomerGroupForAllChildren($childCompaniesIds);
            $this->_eventManager->dispatch(
                'lambda_customer_group_align',
                ['company' => $savedCompany]
            );
        }
        $this->_eventManager->dispatch(
            'lambda_company_save_after_custom',
            ['company' => $savedCompany]
        );

        return $savedCompany;
    }

    /**
     * Create Customer Group for creating Company
     *
     * @param CompanyInterface $company Company
     *
     * @return integer
     */
    protected function createCustomerGroupForCreatedCompany(CompanyInterface $company)
    {
        $customerGroupName = $this->companyParentHierarchy->companyBelongsToCustomerGroupName($company);
        $existedCustomerGroup = $this->customerGroupFactory->create()->load($customerGroupName, 'customer_group_code');
        if (!$existedCustomerGroup->getId()) {
            $createdCustomerGroup = $this->customerGroupFactory->create()->setCode($customerGroupName)
                ->setTaxClassId(GroupRepository::DEFAULT_TAX_CLASS_ID)
                ->save();
            return $createdCustomerGroup->getId();
        }

        return $existedCustomerGroup->getId();
    }

    /**
     * Populate company object with request data.
     *
     * @param CompanyInterface $company Company
     * @param array            $data    Data
     *
     * @return CompanyInterface
     */
    public function setCompanyRequestData(
        CompanyInterface $company,
        array $data
    ): CompanyInterface {
        $this->dataObjectHelper->populateWithArray(
            $company,
            $data,
            CompanyInterface::class
        );
        if (!$company->getParentCompanyId()) {
            $company->setParentCompanyId(null);
        }
        return $company;
    }

    /**
     * Reset parent id for companies, where particular company is a parent one
     *
     * @param CompanyInterface $editingCompany         Company
     * @param array            $childCompaniesIds      Child companies ids
     * @param null             $requestParentCompanyId New Parent Company id
     *
     * @return void
     */
    protected function resetCompanyParentIdWhereCurrentCompanyIsParentByCondition(
        CompanyInterface $editingCompany,
        $childCompaniesIds,
        $requestParentCompanyId
    ) {
        if (in_array($requestParentCompanyId, $childCompaniesIds)) {
            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilter(CompanyInterface::PARENT_COMPANY_ID, $editingCompany->getId(), 'eq')
                ->create();
            $companies = $this->companyRepository->getList($searchCriteria);
            $newParenCompanyForChildren =
                $editingCompany->getParentCompanyId() ? $editingCompany->getParentCompanyId() : null;
            foreach ($companies->getItems() as $company) {
                $company->setParentCompanyId($newParenCompanyForChildren);
                $savedCompany = $this->companyRepository->save($company);
                $this->_eventManager->dispatch(
                    'lambda_company_save_after_custom',
                    ['company' => $savedCompany]
                );
            }
        }
    }

    /**
     * Affect all children companies, of the editing one, to align their customer group
     *
     * @param array $companiesIds Child companies, before reset
     *
     * @return void
     */
    protected function affectAllCompanyChildrenRebuildCustomerGroups($companiesIds)
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(CompanyInterface::COMPANY_ID, $companiesIds, 'in')
            ->create();
        $companies = $this->companyRepository->getList($searchCriteria);

        foreach ($companies->getItems() as $company) {
            $this->_eventManager->dispatch(
                'lambda_customer_group_align',
                ['company' => $company]
            );
        }
    }

    /**
     * Rename customer group for all children companies
     *
     * @param array $companiesIds Child companies, before reset
     *
     * @return void
     */
    protected function renameCustomerGroupForAllChildren($companiesIds)
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(CompanyInterface::COMPANY_ID, $companiesIds, 'in')
            ->create();
        $companies = $this->companyRepository->getList($searchCriteria);
        foreach ($companies->getItems() as $company) {
            $customerGroup = $this->groupRepository->getById($company->getCustomerGroupId());
            $customerGroup->setCode($this->companyParentHierarchy->companyBelongsToCustomerGroupName($company));
            $this->groupRepository->save($customerGroup);
        }
    }

    protected function setCompanyBillingAddress(CompanyInterface $company)
    {
        $companyBillingData = $this->getRequest()->getParam('company_billing');
        if (!empty($companyBillingData[CompanyBillingAddressInterface::BILLING_REGION_ID])) {
            $companyBillingData[CompanyBillingAddressInterface::BILLING_REGION] = null;
        } else {
            $companyBillingData[CompanyBillingAddressInterface::BILLING_REGION_ID] = null;
        }
        $address = $company->getCompanyBillingAddress();
        if ($address instanceof \Lambda\CompanyAccount\Model\CompanyBillingAddress) {
            $address->addData($companyBillingData);
        } else {
            $address = $this->companyBillingAddressFactory->create(['data' => $companyBillingData]);
        }
        $company->setCompanyBillingAddress($address);
    }

    /**
     * @param $companyId
     */
    protected function removeCompanyToStoreAssignments($companyId)
    {
        $collection = $this->storeCollectionFactory->create();
        $collection->addFieldToFilter('company_id', $companyId);
        $collection->walk('delete');
    }

    /**
     * @param $companyId
     * @param array $storeIds
     * @throws \Exception
     */
    protected function createCompanyToStoreAssignments($companyId, array $storeIds)
    {
        foreach ($storeIds as $storeId) {
            $assignment = $this->storeAssignedCompaniesFactory->create();
            $assignment->setCompanyId($companyId)
                ->setStoreId($storeId)
                ->save();
        }
    }

    /**
     * @param array $attributes
     * @return \Magento\Framework\Api\AttributeInterface[]
     */
    private function validateCompanyCustomAttributes(array $attributes): array
    {
        $companyAttributes = [];
        foreach ($attributes as $attributeCode => $attributeValue) {
            $customAttribute = $this->attributeInterfaceFactory->create();
            $companyAttributes[] = $customAttribute->setAttributeCode($attributeCode)->setValue($attributeValue);
        }
        return $companyAttributes;
    }
}
