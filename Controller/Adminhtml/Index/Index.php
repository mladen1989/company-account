<?php
/**
 * File Index
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Controller\Adminhtml\Index;

use Magento\Framework\App\Action\{HttpPostActionInterface, HttpGetActionInterface};
use Lambda\CompanyAccount\Controller\Adminhtml\Index as CompanyAction;

/**
 * Class Index
 */
class Index extends CompanyAction implements HttpGetActionInterface, HttpPostActionInterface
{
    /**
     * Controller execute
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /**
         * Result page
         *
         * @var \Magento\Backend\Model\View\Result\Page $resultPage
         */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Lambda_CompanyAccount::company_account')
            ->getConfig()->getTitle()->prepend(__('Companies'));
        return $resultPage;
    }
}
