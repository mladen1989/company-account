<?php
/**
 * File MassAssignCompany
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Controller\Adminhtml\Index;

use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use \Psr\Log\LoggerInterface;
use Lambda\CompanyAccount\Model\Company;
use Lambda\CompanyAccount\Helper\CompanyParentHierarchy;

/**
 * Class MassAssignCompany
 */
class MassAssignCompany extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    /**
     * Customer repository
     *
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * Customer collection
     *
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Logger
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Company hierarchy helper
     *
     * @var CompanyParentHierarchy
     */
    protected $companyParentHierarchy;

    /**
     * MassAssignCompany constructor.
     *
     * @param Context                     $context                Context
     * @param LoggerInterface             $logger                 Logger
     * @param CollectionFactory           $collectionFactory      Customer collection
     * @param CustomerRepositoryInterface $customerRepository     Customer repository
     * @param CompanyParentHierarchy      $companyParentHierarchy Company hierarchy helper
     */
    public function __construct(
        Context $context,
        LoggerInterface $logger,
        CollectionFactory $collectionFactory,
        CustomerRepositoryInterface $customerRepository,
        CompanyParentHierarchy $companyParentHierarchy
    ) {
        parent::__construct($context);
        $this->logger = $logger;
        $this->customerRepository = $customerRepository;
        $this->collectionFactory = $collectionFactory;
        $this->companyParentHierarchy = $companyParentHierarchy;
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     *
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $selectedCustomers = $this->getRequest()->getParam('selected');
        $companyId = (string)$this->getRequest()->getParam('company');
        /**
         * Redirect
         *
         * @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect
         */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        if (empty($selectedCustomers) || empty($companyId)) {
            return $resultRedirect->setPath('customer/index/index');
        }
        $company = $this->getCompanyById($companyId);
        $successfullyUpdatedCustomers = [];
        $customersUpdated = 0;
        $customersUFailed = 0;
        foreach ($selectedCustomers as $customerId) {
            try {
                /**
                 * Customer
                 *
                 * @var $customer CustomerInterface
                 */
                $customer = $this->customerRepository->getById($customerId);
                $customer->setCustomAttribute(Company::COMPANY_ID, $companyId);
                if ($company && $groupId = $company->getCustomerGroupId()) {
                    $customer->setGroupId($groupId);
                }
                $this->customerRepository->save($customer);
                $successfullyUpdatedCustomers [] = $customer;
                $customersUpdated++;
            } catch (\Exception $e) {
                $customersUFailed++;
                $this->logger->error(
                    'During mass assigning customer (id ' . $customer->getId() . ') to new Company happens an error: ' .
                    $e->getMessage()
                );
                continue;
            }
        }
        if ($successfullyUpdatedCustomers && $company) {
            $this->_eventManager->dispatch(
            'lambda_customers_massassigned_company',
                [
                    'customers' => $successfullyUpdatedCustomers,
                    'company' => $company
                ]
            );
        }

        if ($customersUFailed) {
            $this->messageManager->addSuccessMessage(
                __('%1 record(s) were failed', $customersUFailed)
            );
        }

        if ($customersUpdated) {
            $this->messageManager->addSuccessMessage(
                __('A total of %1 record(s) were updated successfully.', $customersUpdated)
            );
        }

        return $resultRedirect->setPath('customer/index/index');
    }

    /**
     * Get customer group by company
     *
     * @param string $companyId Company Id
     *
     * @return null|\Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    private function getCompanyById($companyId)
    {
        if (empty($companyId)) {
            return null;
        }
        $company = $this->companyParentHierarchy->getCompanyById($companyId);
        if (!$company) {
            return null;
        }
        return $company;
    }
}
