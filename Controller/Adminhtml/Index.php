<?php
/**
 * File Index
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Controller\Adminhtml;

use Lambda\CompanyAccount\Api\CompanyRepositoryInterface;

/**
 * Class Index
 */
abstract class Index extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Lambda_CompanyAccount::company_account';

    /**
     * Result format Factory
     *
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * Result page factory
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * Company repository
     *
     * @var CompanyRepositoryInterface
     */
    protected $companyRepository;

    /**
     * Initialize Group Controller
     *
     * @param \Magento\Backend\App\Action\Context               $context              Context
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory Result forward factory
     * @param \Magento\Framework\View\Result\PageFactory        $resultPageFactory    Result page
     * @param CompanyRepositoryInterface                        $companyRepository    Company repository
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        CompanyRepositoryInterface $companyRepository
    ) {
        $this->companyRepository = $companyRepository;
        parent::__construct($context);
        $this->resultForwardFactory = $resultForwardFactory;
        $this->resultPageFactory = $resultPageFactory;
    }
}
