<?php
/**
 * File AbstractBase
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Controller;

use Magento\Framework\App\Action\{Action, Context};
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Lambda\CompanyAccount\Helper\CustomerIsCompanyAdmin;
use Lambda\CompanyAccount\Api\CompanyRepositoryInterface;


/**
 * Class Index
 */
abstract class AbstractBase extends Action
{
    /**
     * Customer Is Admin helper
     *
     * @var CustomerIsCompanyAdmin
     */
    protected $customerIsAdminHelper;

    /**
     * Page view
     *
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Company Account
     *
     * @var CompanyRepositoryInterface
     */
    protected $companyRepository;

    /**
     * AbstractBase constructor.
     *
     * @param CustomerIsCompanyAdmin     $customerIsAdminHelper Customer is admin helper
     * @param Context                    $context               Context
     * @param PageFactory                $resultPageFactory     Result page
     * @param CompanyRepositoryInterface $companyRepository     Company repository
     */
    public function __construct(
        CustomerIsCompanyAdmin $customerIsAdminHelper,
        Context $context,
        PageFactory $resultPageFactory,
        CompanyRepositoryInterface $companyRepository
    ) {
        parent::__construct($context);
        $this->customerIsAdminHelper = $customerIsAdminHelper;
        $this->resultPageFactory = $resultPageFactory;
        $this->companyRepository = $companyRepository;
    }

    /**
     * Validate action before execute
     *
     * @param RequestInterface $request Request
     *
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function dispatch(RequestInterface $request)
    {
        $adminCompany = $this->customerIsAdminHelper->getAdminCompanyId();
        //if not admin, this section is not allowed
        if (!$adminCompany) {
            $this->_redirect('customer/account/index');
            return $this->_response;
        }
        try {
            $this->companyRepository->get($adminCompany);
        } catch (NoSuchEntityException $e) {
            $this->_redirect('customer/account/index');
            return $this->_response;
        }

        return parent::dispatch($request);
    }
}
