<?php
declare(strict_types=1);

namespace Lambda\CompanyAccount\Controller\Customers;

use Lambda\CompanyAccount\Controller\AbstractBase;

/**
 * Track progress of customers in my company
 *
 * @author mladen
 */
class Progress extends AbstractBase
{
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__("Customers Progress"));
        $navigationBlock = $resultPage->getLayout()->getBlock('customer_account_navigation');
        if ($navigationBlock) {
            $navigationBlock->setActive('company/customers/progress');
        }
        return $resultPage;
    }
}
