<?php
/**
 * File MassUploadPost
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Controller\Customers;

use \Psr\Log\LoggerInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\File\UploaderFactory;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\File\Csv;
use Magento\Framework\Math\Random;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Api\{CustomerRepositoryInterface, Data\CustomerInterface};
use Magento\Customer\Model\GroupFactory as CustomerGroupFactory;
use Lambda\CompanyAccount\Controller\AbstractBase;
use Lambda\CompanyAccount\Helper\{CustomerIsCompanyAdmin, CompanyParentHierarchy, Data};
use Lambda\CompanyAccount\Api\CompanyRepositoryInterface;
use Lambda\CompanyAccount\Model\Config\Email\Template;

/**
 * Class MassUploadPost
 */
class MassUploadPost extends AbstractBase
{
    /**
     * Directory path for uploading files, related to Magento root directory
     *
     * @var string
     */
    const FILES_DIRECTORY_NAME = 'lambda_company_account/customers_import';

    /**
     * Encryptor
     *
     * @var EncryptorInterface
     */
    private $encryptor;

    /**
     * File Uploader factory
     *
     * @var UploaderFactory
     */
    protected $uploaderFactory;

    /**
     * Directory List
     *
     * @var DirectoryList
     */
    protected $dir;

    /**
     * Csv parser
     *
     * @var Csv
     */
    protected $csv;

    /**
     * Store manager
     *
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Customer repository
     *
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;

    /**
     * Customer
     *
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * Company repository
     *
     * @var CompanyRepositoryInterface
     */
    protected $companyRepository;

    /**
     * Logger
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Company hierarchy helper
     *
     * @var CompanyParentHierarchy
     */
    protected $companyParentHierarchy;

    /**
     * Customer group factory
     *
     * @var CustomerGroupFactory
     */
    protected $customerGroupFactory;

    /**
     * Random
     *
     * @var Random
     */
    private $mathRandom;

    /**
     * Transport builder
     *
     * @var TransportBuilder
     */
    protected $transportBuilder;

    /**
     * Config getter
     *
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * MassUploadPost constructor.
     *
     * @param CustomerIsCompanyAdmin      $customerIsAdminHelper  Customer is admin helper
     * @param Context                     $context                Context
     * @param PageFactory                 $resultPageFactory      Result page factory
     * @param UploaderFactory             $uploaderFactory        Uploader factory
     * @param DirectoryList               $dir                    Directory list
     * @param Csv                         $csv                    Csv worker
     * @param StoreManagerInterface       $storeManager           Store manager
     * @param CustomerFactory             $customerFactory        Customer factory
     * @param EncryptorInterface          $encryptor              Encryptor
     * @param CustomerRepositoryInterface $customerRepository     Customer repository
     * @param CompanyRepositoryInterface  $companyRepository      Company repository
     * @param LoggerInterface             $logger                 Logger
     * @param CompanyParentHierarchy      $companyParentHierarchy Parent company helper
     * @param CustomerGroupFactory        $customerGroupFactory   Customer group model factory
     * @param Random                      $mathRandom             Math random
     * @param TransportBuilder            $transportBuilder       Transport builder
     * @param ScopeConfigInterface        $scopeConfig            Config getter
     */
    public function __construct(
        CustomerIsCompanyAdmin $customerIsAdminHelper,
        Context $context,
        PageFactory $resultPageFactory,
        UploaderFactory $uploaderFactory,
        DirectoryList $dir,
        Csv $csv,
        StoreManagerInterface $storeManager,
        CustomerFactory $customerFactory,
        EncryptorInterface $encryptor,
        CustomerRepositoryInterface $customerRepository,
        CompanyRepositoryInterface $companyRepository,
        LoggerInterface $logger,
        CompanyParentHierarchy $companyParentHierarchy,
        CustomerGroupFactory $customerGroupFactory,
        Random $mathRandom,
        TransportBuilder $transportBuilder,
        ScopeConfigInterface $scopeConfig
    ) {
        parent::__construct($customerIsAdminHelper, $context, $resultPageFactory, $companyRepository);
        $this->uploaderFactory = $uploaderFactory;
        $this->dir = $dir;
        $this->csv = $csv;
        $this->storeManager = $storeManager;
        $this->customerFactory = $customerFactory;
        $this->encryptor = $encryptor;
        $this->customerRepository = $customerRepository;
        $this->companyRepository = $companyRepository;
        $this->logger = $logger;
        $this->companyParentHierarchy = $companyParentHierarchy;
        $this->customerGroupFactory = $customerGroupFactory;
        $this->mathRandom = $mathRandom;
        $this->transportBuilder = $transportBuilder;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Mass upload customers
     *
     * @return mixed
     */
    public function execute()
    {
        $result = [
            'message'=> '',
            'success'=> true
        ];
        $dir = $this->getFilesDirectoryPath();
        $fileInfoFromRequest = $this->getRequest()->getFiles()['file'];
        $uploader = $this->uploaderFactory->create(['fileId' => $fileInfoFromRequest]);
        $uploadedFileData = $uploader->save($dir, $this->getNewFileName($fileInfoFromRequest));
        if (empty($uploadedFileData['path']) || empty($uploadedFileData['file'])) {
            $result['message'] = __('Couldn`t upload file, cause some technical reasons. Please contact support.');
            $result['success'] = false;
        }
        if ($result['success']) {
            $importResult = $this->parseFileAndCreateCustomers($uploadedFileData['path'] . $uploadedFileData['file']);
            $result['message'] = $importResult['message'];
            $result['success'] = $importResult['status'];
        }
        /**
         * Response
         *
         * @var $response \Magento\Framework\Controller\Result\Json\Interceptor
         */
        $response = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        return $response->setData($result);
    }

    /**
     * Get uploading files directory (create, if doesn`t exists)
     *
     * @return string
     */
    private function getFilesDirectoryPath()
    {
        $rootVarDirectoryPath = $this->dir->getPath('var');
        $directoryForFiles = $rootVarDirectoryPath . '/' . self::FILES_DIRECTORY_NAME . '/';
        if (!file_exists($directoryForFiles) && !is_dir($directoryForFiles)) {
            mkdir($directoryForFiles, 0777, true);
        }

        return $directoryForFiles;
    }

    /**
     * @param array $fileInfoFromRequest File from request
     *
     * @return string
     */
    protected function getNewFileName($fileInfoFromRequest)
    {
        return time() . '_' . $fileInfoFromRequest['name'];
    }

    /**
     * Parse imported file
     *
     * @param string $pathToFile Path to parsing file
     *
     * @return array
     */
    private function parseFileAndCreateCustomers($pathToFile)
    {
        if (empty($pathToFile)) {
            return [
                'status' => false,
                'message' => __('File could not be parsed')
            ];
        }
        $csvData = $this->csv->getData($pathToFile);
        //remove extra spaces
        foreach ($csvData[0] as $index => $titles) {
            $csvData[0][$index] = trim($titles, ' ');
        }
        $columnTitles = array_flip($csvData[0]);
        try {
            $firstNameColumnIndex = $columnTitles['First Name'];
            $lastNameColumnIndex = $columnTitles['Last Name'];
            $emailColumnIndex = $columnTitles['Email'];
            $companyNameColumnIndex = $columnTitles['Company Name'];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'message' => __('File columns format is not correct')
            ];
        }
        // Get Website ID
        $websiteId = $this->storeManager->getWebsite()->getWebsiteId();
        $existedCompanies = [];
        $existedCustomersEmail = [];
        $successfullyUpdatedCustomers = [];
        $emailTemplateIdentifier = $this->scopeConfig->getValue('company_account_config/email/template');
        foreach ($csvData as $index => $data) {
            if (0 == $index) {
                //skip first row, cause it contents column titles
                continue;
            }
            $firstName = trim($data[$firstNameColumnIndex], ' ');
            $lastName = trim($data[$lastNameColumnIndex], ' ');
            $email = trim($data[$emailColumnIndex], ' ');
            $companyName = trim($data[$companyNameColumnIndex], ' ');
            /**
             * Customer
             *
             * @var $customer \Magento\Customer\Model\Customer
             */
            $customer = $this->customerFactory->create();
            $customer->setWebsiteId($websiteId);
            $customer->setEmail($email);
            $customer->setFirstname($firstName);
            $customer->setLastname($lastName);
            $pass = \Lambda\CompanyAccount\Helper\Data::generateRandomString();
            $customer->setPassword($this->encryptor->getHash($pass, true));
            $newPasswordToken = $this->mathRandom->getUniqueHash();
            $customer->changeResetPasswordLinkToken($newPasswordToken);
            try {
                //using deprecated model save method, cause other are unsuitable:
                // - customerRepository variant needs another object -> \Magento\Customer\Api\Data\CustomerInterface
                // - AccountManagement->createAccount will send default emails
                $customer->save();
                $this->sendAccountConfirmationEmail($customer, $emailTemplateIdentifier);
                if (!in_array($companyName, $existedCompanies)) {
                    $company = $this->companyRepository->getByName($companyName);
                    if (!$company->getId()) {
                        try {
                            $company = $this->companyRepository->get($this->customerIsAdminHelper->getAdminCompanyId());
                        } catch (\Exception $e) {

                        }
                    }
                    if ($company->getId()) {
                        $existedCompanies[$company->getName()] = $company;
                        $reloadedCustomer = $this->customerRepository->getById($customer->getId());
                        $reloadedCustomer->setCustomAttribute(Data::CUSTOMER_ATTRIBUTE_COMPANY_ID, $company->getId());
                        $reloadedCustomer->setGroupId($company->getCustomerGroupId());
                        $this->customerRepository->save($reloadedCustomer);
                        $successfullyUpdatedCustomers[] = $reloadedCustomer;
                    }
                }
            } catch (\Exception $e) {
                $this->logger->error('Couldn`t save customer cause: ' . $e->getMessage());
                $existedCustomersEmail[] = $email;
                continue;
            }
        }
        if ($successfullyUpdatedCustomers) {
            $this->_eventManager->dispatch(
                'lambda_masscreated_customers',
                [
                    'customers' => $successfullyUpdatedCustomers
                ]
            );
        }

        if (!empty($existedCustomersEmail)) {
            return [
                'status' => false,
                'message' => __('Customer(s) with next email, are already exists: ' . rtrim(implode(', ', $existedCustomersEmail), ', '))
            ];
        }

        return [
            'status' => true,
            'message' => __('All customers has been successfully imported')
        ];
    }

    /**
     * Send email to customer
     *
     * @param CustomerInterface $customer                Customer
     * @param string            $emailTemplateIdentifier Email template identifier
     *
     * @return void
     */
    private function sendAccountConfirmationEmail($customer, $emailTemplateIdentifier = Template::DEFAULT_EMAIL_IDENTIFIER)
    {
        $transport = $this->transportBuilder->setTemplateIdentifier(
            $emailTemplateIdentifier
        )->setTemplateOptions(
            ['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $this->storeManager->getStore()->getId()]
        )->setTemplateVars(
            ['customer' => $customer, 'store' => $this->storeManager->getStore()]
        )->setFrom(
            $this->scopeConfig->getValue(
                \Magento\Customer\Model\Customer::XML_PATH_REGISTER_EMAIL_IDENTITY,
                ScopeInterface::SCOPE_STORE,
                $this->storeManager->getStore()->getId()
            )
        )->addTo(
            $customer->getEmail(),
            $customer->getFirstname()
        )->getTransport();
        $transport->sendMessage();
    }
}
