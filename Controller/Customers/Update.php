<?php
/**
 * File Update
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Controller\Customers;

use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Customer\Model\Data\Customer;
use Magento\Customer\Model\ResourceModel\Group\CollectionFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Lambda\CompanyAccount\Api\CompanyRepositoryInterface;
use Lambda\CompanyAccount\Controller\AbstractBase;
use Lambda\CompanyAccount\Helper\{CustomerIsCompanyAdmin, Customers, FrontParentCompany, Data};

/**
 * Class Update
 */
class Update extends AbstractBase
{
    /**
     * Customer repository
     *
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * Customer helper
     *
     * @var Customers
     */
    protected $customersHelper;

    /**
     * FrontParent company helper
     *
     * @var FrontParentCompany
     */
    protected $frontParentCompanyHelper;
    private GroupRepositoryInterface $groupRepository;
    private CollectionFactory $groupFactory;

    /**
     * @var \Lambda\ContractCode\Api\ContractLearnerRepositoryInterface
     */
    protected $contractLearnerRepository;

    /**
     * @var \Lambda\ContractCode\Api\ContractManagementInterface
     */
    protected $contractManagement;

    /**
     * Edit constructor.
     *
     * @param CustomerIsCompanyAdmin      $customerIsAdminHelper    Customer is admin helper
     * @param Customers                   $customersHelper          Customer helper
     * @param Context                     $context                  Context
     * @param PageFactory                 $resultPageFactory        Result page
     * @param CustomerRepositoryInterface $customerRepository       Customer repository
     * @param FrontParentCompany          $frontParentCompanyHelper Front parent Company helper
     * @param CompanyRepositoryInterface  $companyRepository        Company repository
     */
    public function __construct(
        CustomerIsCompanyAdmin $customerIsAdminHelper,
        Customers $customersHelper,
        Context $context,
        PageFactory $resultPageFactory,
        CustomerRepositoryInterface $customerRepository,
        FrontParentCompany $frontParentCompanyHelper,
        GroupRepositoryInterface $groupRepository,
        CollectionFactory $customerGroupCollection,
        CompanyRepositoryInterface $companyRepository,
        \Lambda\ContractCode\Api\ContractLearnerRepositoryInterface $contractLearnerRepository,
        \Lambda\ContractCode\Api\ContractManagementInterface $contractManagement
    ) {
        parent::__construct($customerIsAdminHelper, $context, $resultPageFactory, $companyRepository);
        $this->customerRepository = $customerRepository;
        $this->customersHelper = $customersHelper;
        $this->groupRepository = $groupRepository;
        $this->groupFactory = $customerGroupCollection;
        $this->frontParentCompanyHelper = $frontParentCompanyHelper;
        $this->contractLearnerRepository = $contractLearnerRepository;
        $this->contractManagement = $contractManagement;
    }

    /**
     * Update Customer data
     *
     * @return Redirect|Page
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create()->setPath('*/*/index');
        $customerIdFromRequest = (integer)$this->getRequest()->getParam('customer_id');
        $isAdminValue = $this->getRequest()->getParam('is_admin', '0');
        $groupName = $this->getRequest()->getParam('group', '0');
        $groupId = $this->getGroupId($groupName);
        $companyId = $this->getRequest()->getParam('company_id', '0');
        if (!$customerIdFromRequest) {
            $this->messageManager->addErrorMessage('Customer has not been updated.');
            return $resultRedirect;
        }
        try {
            /**
             * Customer
             *
             * @var $customer Customer
             */
            $customer = $this->customerRepository->getById($customerIdFromRequest);
            $allChildrenCompaniesIds = $this->frontParentCompanyHelper->getAllChildrenByParentCompany(
                $this->customerIsAdminHelper->getAdminCompanyId()
            );
            array_push($allChildrenCompaniesIds, $this->customerIsAdminHelper->getAdminCompanyId());
            $customersAllowedToBeEditable = $this->customersHelper
                ->getCustomersIdsByCompaniesIds($allChildrenCompaniesIds);
            if (!in_array($customerIdFromRequest, $customersAllowedToBeEditable)) {
                throw new \Exception('Customer is not allowed to be updated');
            }
            $customer->setCustomAttribute(Data::CUSTOMER_ATTRIBUTE_IS_COMPANY_ADMIN, $isAdminValue);
            $customer->setCustomAttribute(Data::CUSTOMER_ATTRIBUTE_COMPANY_ID, (int)$companyId);
            $customer->setGroupId((int)$groupId);
            $this->checkAndRemoveContractCode($customer->getId());
            $this->customerRepository->save($customer);
            $this->messageManager->addSuccessMessage('Customer has been successfully updated');
        } catch (NoSuchEntityException $e) {
            $this->messageManager->addErrorMessage(
                __('Customer with such Id doesn`t exist')
            );
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(
                __('You cannot update this customer data')
            );
        }

        return $this->resultRedirectFactory->create()->setPath('*/*/index');
    }

    public function getGroupId($groupName)
    {
        if(empty($groupName))
        {
            $groupName = 'General';
        }
        $collection = $this->groupFactory->create()
            ->addFieldToSelect('customer_group_id')
            ->addFieldToFilter('customer_group_code', ['like' => '%' . $groupName . '%'])
            ->getData();
        if(!empty($collection))
        {
            return $collection[0]['customer_group_id'];
        }
    }

    public function checkAndRemoveContractCode($customerId)
    {
        if($this->contractManagement->isCustomerAssignedToContract($customerId)){
            $this->contractLearnerRepository->deleteByCustomerId($customerId);
        }
    }
}
