<?php
/**
 * File MassUpload
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Controller\Customers;

use Lambda\CompanyAccount\Controller\AbstractBase;

/**
 * Class MassUpload
 */
class MassUpload extends AbstractBase
{
    /**
     * Customers list
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /**
         * Result page factory
         *
         * @var \Magento\Framework\View\Result\Page $resultPage
         */
        $resultPage = $this->resultPageFactory->create();
        /**
         * Navigation block
         *
         * @var \Magento\Framework\View\Element\Html\Links $navigationBlock
         */
        $navigationBlock = $resultPage->getLayout()->getBlock('customer_account_navigation');
        if ($navigationBlock) {
            $navigationBlock->setActive('company/customers/index');
        }
        return $resultPage;
    }
}
