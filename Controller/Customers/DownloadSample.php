<?php
/**
 * File DownloadSample
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Controller\Customers;

use Magento\Framework\Component\ComponentRegistrar;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\App\Response\Http\FileFactory;
use Lambda\CompanyAccount\Api\CompanyRepositoryInterface;
use Lambda\CompanyAccount\Helper\CustomerIsCompanyAdmin;
use Lambda\CompanyAccount\Controller\AbstractBase;

/**
 * Class DownloadSample
 */
class DownloadSample extends AbstractBase
{
    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var \Magento\Framework\Component\ComponentRegistrar
     */
    private $componentRegistrar;

    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * DownloadSample constructor.
     *
     * @param CustomerIsCompanyAdmin     $customerIsAdminHelper
     * @param Context                    $context
     * @param PageFactory                $resultPageFactory
     * @param CompanyRepositoryInterface $companyRepository,
     * @param RawFactory                 $resultRawFactory,
     * @param ComponentRegistrar         $componentRegistrar,
     * @param FileFactory                $fileFactory
     */
    public function __construct(
        CustomerIsCompanyAdmin $customerIsAdminHelper,
        Context $context,
        PageFactory $resultPageFactory,
        CompanyRepositoryInterface $companyRepository,
        RawFactory $resultRawFactory,
        ComponentRegistrar $componentRegistrar,
        FileFactory $fileFactory
    ) {
        $this->resultRawFactory = $resultRawFactory;
        $this->componentRegistrar = $componentRegistrar;
        $this->fileFactory = $fileFactory;
        parent::__construct($customerIsAdminHelper, $context, $resultPageFactory, $companyRepository);
    }


    public function execute()
    {
        $fileName = 'customer.csv';
        $moduleDir = $this->componentRegistrar->getPath(ComponentRegistrar::MODULE, 'Lambda_CompanyAccount');
        $sampleFileFullPath = $moduleDir . '/Files/Sample/' . $fileName;

        $fileContents = (string)file_get_contents($sampleFileFullPath);

        $this->fileFactory->create(
            $fileName,
            null,
            DirectoryList::VAR_DIR,
            'application/octet-stream',
            filesize($sampleFileFullPath)
        );

        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        $resultRaw = $this->resultRawFactory->create();
        $resultRaw->setContents($fileContents);

        return $resultRaw;
    }
}
