<?php
/**
 * File Delete
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Controller\Customers;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\View\Element\Html\Links;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Lambda\CompanyAccount\Api\CompanyRepositoryInterface;
use Lambda\CompanyAccount\Controller\AbstractBase;
use Lambda\CompanyAccount\Helper\{CustomerIsCompanyAdmin, Customers, FrontParentCompany};

/**
 * Class Delete
 */
class Delete extends AbstractBase
{
    /**
     * Customer repository
     *
     * @var CustomerRepositoryInterface
     */
    protected CustomerRepositoryInterface $customerRepository;

    /**
     * Customer helper
     *
     * @var Customers
     */
    protected Customers $customersHelper;

    /**
     * FrontParent company helper
     *
     * @var FrontParentCompany
     */
    protected FrontParentCompany $frontParentCompanyHelper;

    /**
     * Delete constructor.
     *
     * @param CustomerIsCompanyAdmin      $customerIsAdminHelper    Customer is admin helper
     * @param Customers                   $customersHelper          Customer helper
     * @param Context                     $context                  Context
     * @param PageFactory                 $resultPageFactory        Result page
     * @param CustomerRepositoryInterface $customerRepository       Customer repository
     * @param FrontParentCompany          $frontParentCompanyHelper Front parent Company helper
     * @param CompanyRepositoryInterface  $companyRepository        Company repository
     */
    public function __construct(
        CustomerIsCompanyAdmin $customerIsAdminHelper,
        Customers $customersHelper,
        Context $context,
        PageFactory $resultPageFactory,
        CustomerRepositoryInterface $customerRepository,
        FrontParentCompany $frontParentCompanyHelper,
        CompanyRepositoryInterface $companyRepository
    ) {
        parent::__construct($customerIsAdminHelper, $context, $resultPageFactory, $companyRepository);
        $this->customerRepository = $customerRepository;
        $this->customersHelper = $customersHelper;
        $this->frontParentCompanyHelper = $frontParentCompanyHelper;
    }

    /**
     * Delete Customer
     *
     * @return Redirect|Page
     */
    public function execute()
    {
        try {
            $customerIdFromRequest = (integer)$this->getRequest()->getParam('id');
            //if user not exists, throws NoSuchEntityException
            $this->customerRepository->getById($customerIdFromRequest);
            $allChildrenCompaniesIds = $this->frontParentCompanyHelper->getAllChildrenByParentCompany(
                $this->customerIsAdminHelper->getAdminCompanyId()
            );
            array_push($allChildrenCompaniesIds, $this->customerIsAdminHelper->getAdminCompanyId());
            $customersAllowedToBeEditable = $this->customersHelper
                ->getCustomersIdsByCompaniesIds($allChildrenCompaniesIds);
            if (!in_array($customerIdFromRequest, $customersAllowedToBeEditable)) {
                throw new \Exception('Customer is not allowed to be editable');
            }
        } catch (NoSuchEntityException $e) {
            $this->messageManager->addErrorMessage(
                __('Customer with such Id doesn`t exist')
            );
            return $this->resultRedirectFactory->create()->setPath('*/*/index');
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(
                __('You cannot edit this customer data')
            );
            return $this->resultRedirectFactory->create()->setPath('*/*/index');
        }
        /**
         * Result page factory
         *
         * @var Page $resultPage
         */
        $resultPage = $this->resultPageFactory->create();
        /**
         * Navigation block
         *
         * @var Links $navigationBlock
         */
        $navigationBlock = $resultPage->getLayout()->getBlock('customer_account_navigation');
        if ($navigationBlock) {
            $navigationBlock->setActive('company/customers/index');
        }
        return $resultPage;
    }
}
