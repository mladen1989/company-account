<?php
/**
 * File Assign
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Controller\Customers;

use Lambda\CompanyAccount\Helper\Data;
use \Psr\Log\LoggerInterface;
use Magento\Framework\App\Action\{Context, Action};
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Lambda\CompanyAccount\Helper\CurrentStoreCompaniesList;
use Lambda\CompanyAccount\Api\CompanyRepositoryInterface;

/**
 * Class Assign
 */
class Assign extends Action
{
    /**
     * Customer repository
     *
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * Search Criteria Builder
     *
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $criteriaBuilder;

    /**
     * Logger
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Current store companies helper
     *
     * @var CurrentStoreCompaniesList
     */
    protected $companiesListHelper;

    /**
     * Company repository
     *
     * @var CompanyRepositoryInterface
     */
    protected $companyRepository;

    /**
     * Assign constructor.
     *
     * @param Context                     $context             Context
     * @param CustomerRepositoryInterface $customerRepository  Customer repository
     * @param SearchCriteriaBuilder       $criteriaBuilder     Search criteria builder
     * @param LoggerInterface             $logger              Logger
     * @param CurrentStoreCompaniesList   $companiesListHelper Companies helper
     * @param CompanyRepositoryInterface  $companyRepository   Company repository
     */
    public function __construct(
        Context $context,
        CustomerRepositoryInterface $customerRepository,
        SearchCriteriaBuilder $criteriaBuilder,
        LoggerInterface $logger,
        CurrentStoreCompaniesList $companiesListHelper,
        CompanyRepositoryInterface $companyRepository
    ) {
        parent::__construct($context);
        $this->customerRepository = $customerRepository;
        $this->criteriaBuilder = $criteriaBuilder;
        $this->logger = $logger;
        $this->companiesListHelper = $companiesListHelper;
        $this->companyRepository = $companyRepository;
    }

    /**
     * Assign customer(s) to company
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        try {
            /**
             * Response
             *
             * @var $response \Magento\Framework\Controller\Result\Json
             */
            $response = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $customersIds = $this->getRequest()->getParam('customers');
            $companyId = $this->getRequest()->getParam('company');
            if (empty($customersIds) || empty($companyId)) {
                throw new \Exception('Request doen`t have needed data');
            }
            //check if we are allowed to save this company for customer(s)
            $allowedCompaniesIds = array_map(function ($company) {
                return $company['value'];
            }, $this->companiesListHelper->getCurrentStoreActiveCompanies());
            if (!in_array($companyId, $allowedCompaniesIds)) {
                throw new \Exception(
                    sprintf('Company with %s id is not allowed for assignment', $companyId)
                );
            }
            /**
             * Company
             *
             * @var $company \Lambda\CompanyAccount\Api\Data\CompanyInterface
             */
            $company = $this->companyRepository->get($companyId);
            $successfullyUpdatedCustomers = [];
            $criteria = $this->criteriaBuilder->addFilter('entity_id', $customersIds, 'in')
                ->create();
            $customers = $this->customerRepository->getList($criteria);
            foreach ($customers->getItems() as $customer) {
                /**
                 * Customer
                 *
                 * @var $customer \Magento\Customer\Model\Data\Customer
                 */
                $customer->setCustomAttribute(Data::CUSTOMER_ATTRIBUTE_COMPANY_ID, $company->getId());
                $customer->setGroupId($company->getCustomerGroupId());
                $this->customerRepository->save($customer);
                $successfullyUpdatedCustomers[] = $customer;
            }
            if ($successfullyUpdatedCustomers && $company) {
                $this->_eventManager->dispatch(
                    'lambda_customers_massassigned_company',
                    [
                        'customers' => $successfullyUpdatedCustomers,
                        'company' => $company
                    ]
                );
            }
            return $response->setData(
                [
                    'success' => 'true',
                    'message' => __('Customer(s) successfully assigned to company')
                ]
            );
        } catch (\Exception $e) {
            $this->logger->error('During assigning customer(s) to company happens an error: ' . $e->getMessage());
            return $response->setData(
                [
                    'success' => 'false',
                    'message' => __('Cause some technical issues, not all customer has been assigned to company')
                ]
            );
        }
    }
}
