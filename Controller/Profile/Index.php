<?php
/**
 * File Index
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Controller\Profile;

use Lambda\CompanyAccount\Controller\AbstractBase;

/**
 * Class Index
 */
class Index extends AbstractBase
{
    /**
     * Manage admin company profile
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /**
         * Result page factory
         *
         * @var \Magento\Framework\View\Result\Page $resultPage
         */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__("Company Profiles"));
        /**
         * Navigation block
         *
         * @var \Magento\Framework\View\Element\Html\Links $navigationBlock
         */
        $navigationBlock = $resultPage->getLayout()->getBlock('customer_account_navigation');
        if ($navigationBlock) {
            $navigationBlock->setActive('company/profile/index');
        }
        return $resultPage;
    }
}
