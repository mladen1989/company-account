<?php
/**
 * File Customers
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Controller\Profile;

use Magento\Framework\App\Action\{Context, Action};
use Magento\Framework\Controller\ResultFactory;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollection;
use Lambda\CompanyAccount\Helper\Data;

/**
 * Class Customers
 */
class Customers extends Action
{
    /**
     * Customer collection
     *
     * @var CustomerCollection
     */
    protected $customersCollectionFactory;

    /**
     * ListAction constructor.
     *
     * @param Context            $context                   Context
     * @param CustomerCollection $customersCollectionFactory Customer collection
     */
    public function __construct(
        Context $context,
        CustomerCollection $customersCollectionFactory
    ) {
        parent::__construct($context);
        $this->customersCollectionFactory = $customersCollectionFactory;
    }

    /**
     * Get list of available Companies
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $companyId = $this->getRequest()->getParam('company_id');
        /**
         * Response
         *
         * @var $response \Magento\Framework\Controller\Result\Json
         */
        $response = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $customers = [];
        if (!$companyId) {
            return $response->setData(
                [
                    'customers' => $customers
                ]
            );
        }
        $customersCollection = $this->customersCollectionFactory->create();
        $customersCollection
            ->addAttributeToFilter(Data::CUSTOMER_ATTRIBUTE_COMPANY_ID, ['eq' => $companyId]);
        foreach ($customersCollection as $customer) {
            /**
             * Customer
             *
             * @var $customer \Magento\Customer\Model\Customer
             */
            $customers[] = [
                'id' => $customer->getId(),
                'email' => $customer->getEmail(),
                'name' => $customer->getName()
            ];
        }

        return $response->setData(
            [
                'customers' => $customers
            ]
        );
    }
}
