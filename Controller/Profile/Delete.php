<?php
/**
 * File Delete
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Controller\Profile;

use Magento\Framework\App\Action\{Action, Context};
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;
use Lambda\CompanyAccount\Helper\{CustomerIsCompanyAdmin, FrontParentCompany};
use Lambda\CompanyAccount\Api\CompanyRepositoryInterface;
use Lambda\CompanyAccount\Api\Data\CompanyInterface;

/**
 * Class Delete
 */
class Delete extends Action
{
    /**
     * Company repository
     *
     * @var CompanyRepositoryInterface
     */
    protected $companyRepository;

    /**
     * Customer Is Admin helper
     *
     * @var CustomerIsCompanyAdmin
     */
    protected $customerIsAdminHelper;

    /**
     * Helper for get list of possible parent companies
     *
     * @var FrontParentCompany
     */
    protected $frontParentCompanyHelper;

    /**
     * Delete constructor.
     *
     * @param FrontParentCompany         $frontParentCompanyHelper Helper for get list of possible parent companies
     * @param CompanyRepositoryInterface $companyRepository        Company repository
     * @param CustomerIsCompanyAdmin     $customerIsAdminHelper    Customer is admin helper
     * @param Context                    $context                  Context
     */
    public function __construct(
        FrontParentCompany $frontParentCompanyHelper,
        CompanyRepositoryInterface $companyRepository,
        CustomerIsCompanyAdmin $customerIsAdminHelper,
        Context $context
    ) {
        parent::__construct($context);
        $this->companyRepository = $companyRepository;
        $this->customerIsAdminHelper = $customerIsAdminHelper;
        $this->frontParentCompanyHelper = $frontParentCompanyHelper;
    }

    /**
     * Delete company
     *
     * @return Redirect
     */
    public function execute(): Redirect
    {
        $resultRedirect = $this->resultRedirectFactory->create()->setPath('company/profile/index');
        $deletingCompanyId = $this->getRequest()->getParam('id');
        try {
            $company = $this->companyRepository->get($deletingCompanyId);
            if (!$this->isCompanyAllowedToBeDeleted($company)) {
                throw new \Exception('Company is not a Company Admin company ot it`s child');
            }
            if ($this->companyRepository->deleteById($deletingCompanyId)) {
                $this->messageManager->addSuccessMessage(
                    __('Company has been successfully deleted')
                );
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(
                __('Company couldn`t be removed.')
            );
        }

        return $resultRedirect;
    }

    /**
     * Validate action before execute
     *
     * @param RequestInterface $request Request
     *
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function dispatch(RequestInterface $request)
    {
        //if not admin, this section is not allowed
        if (!$this->customerIsAdminHelper->getAdminCompanyId()) {
            $this->_redirect('customer/account/index');
            return $this->_response;
        }
        return parent::dispatch($request);
    }

    /**
     * Check, can we delete company or no
     *
     * @param CompanyInterface $company Company
     *
     * @return boolean
     */
    protected function isCompanyAllowedToBeDeleted(CompanyInterface $company)
    {
        $companiesIds = $this->frontParentCompanyHelper->getAllChildrenByParentCompany(
            $this->customerIsAdminHelper->getAdminCompanyId()
        );
        //add parent company
        array_unshift($companiesIds, $this->customerIsAdminHelper->getAdminCompanyId());

        return in_array($company->getId(), $companiesIds);
    }
}
