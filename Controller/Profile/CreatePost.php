<?php
/**
 * File CreatePost
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Controller\Profile;

use \Psr\Log\LoggerInterface;
use Magento\Framework\App\Action\{Context, Action};
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\GroupFactory as CustomerGroupFactory;
use Magento\Customer\Model\ResourceModel\GroupRepository;
use Lambda\CompanyAccount\Api\Data\{CompanyInterface, CompanyInterfaceFactory};
use Lambda\CompanyAccount\Api\CompanyRepositoryInterface;
use Lambda\CompanyAccount\Model\Company;
use Lambda\CompanyAccount\Helper\{CustomerIsCompanyAdmin, CompanyParentHierarchy};
use Lambda\CompanyAccount\Model\StoreAssignedCompaniesFactory;

/**
 * Class CreatePost
 */
class CreatePost extends Action
{
    /**
     * Logger
     *
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Company repository
     *
     * @var CompanyRepositoryInterface
     */
    protected $companyRepository;

    /**
     * Company factory
     *
     * @var CompanyInterfaceFactory
     */
    protected $companyDataFactory;

    /**
     * Form key validator
     *
     * @var Validator
     */
    private $formKeyValidator;

    /**
     * Customer Is Admin helper
     *
     * @var CustomerIsCompanyAdmin
     */
    protected $customerIsAdminHelper;

    /**
     * Store manager
     *
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Store to company relation model
     *
     * @var StoreAssignedCompaniesFactory
     */
    protected $storeAssignedCompaniesModel;

    /**
     * Customer group factory
     *
     * @var CustomerGroupFactory
     */
    protected $customerGroupFactory;

    /**
     * Company parent hierarchy helper
     *
     * @var CompanyParentHierarchy
     */
    protected $companyParentHierarchy;

    /**
     * EditPost constructor
     *
     * @param StoreManagerInterface      $storeManager           Store manager
     * @param Context                    $context                Context
     * @param Validator                  $formKeyValidator       Form key validator
     * @param LoggerInterface            $logger                 Logger
     * @param CompanyRepositoryInterface $companyRepository      Company repository
     * @param CompanyInterfaceFactory    $companyDataFactory     Company model
     * @param CustomerIsCompanyAdmin     $customerIsAdminHelper  Customer is admin helper
     * @param CustomerGroupFactory       $customerGroupFactory   Customer group model
     * @param CompanyParentHierarchy     $companyParentHierarchy Company parent hierarchy helper
     */
    public function __construct(
        StoreAssignedCompaniesFactory $storeAssignedCompaniesModel,
        StoreManagerInterface $storeManager,
        Context $context,
        Validator $formKeyValidator,
        LoggerInterface $logger,
        CompanyRepositoryInterface $companyRepository,
        CompanyInterfaceFactory $companyDataFactory,
        CustomerIsCompanyAdmin $customerIsAdminHelper,
        CustomerGroupFactory $customerGroupFactory,
        CompanyParentHierarchy $companyParentHierarchy
    ) {
        parent::__construct($context);
        $this->storeAssignedCompaniesModel = $storeAssignedCompaniesModel;
        $this->storeManager = $storeManager;
        $this->formKeyValidator = $formKeyValidator;
        $this->logger = $logger;
        $this->companyRepository = $companyRepository;
        $this->companyDataFactory = $companyDataFactory;
        $this->customerIsAdminHelper = $customerIsAdminHelper;
        $this->customerGroupFactory = $customerGroupFactory;
        $this->companyParentHierarchy = $companyParentHierarchy;
    }

    /**
     * Create company
     *
     * @return Redirect
     */
    public function execute(): Redirect
    {
        $request = $this->getRequest();
        $resultRedirect = $this->resultRedirectFactory->create()->setPath('*/profile/add');
        try {
            if (!$this->formKeyValidator->validate($request)) {
                $this->messageManager->addErrorMessage(__('Form key is no longer valid, please refresh the page'));
                throw new \Exception('Form key, from company profile update form, is not valid');
            }
            if (!$this->isRequestValidForCompanyCreate($request)) {
                $this->messageManager->addErrorMessage(__('Sub Company profile create request is not valid.'));
                throw new \Exception('Sub Company profile create request is not valid (id or name is not valid)');
            }
            if ($this->isAnotherCompanyWithSuchNameAlreadyExists($request)) {
                $this->messageManager->addErrorMessage(__('Another Company with such name already exists.'));
                throw new \Exception(
                    'Another Company with such name already exists [' .
                    $request->getParam(Company::NAME) . ']'
                );
            }
            $creatingCompanyData = $this->prepareCompanyDataFromRequest($request->getParams());
            $creatingCompany = $this->companyDataFactory->create($creatingCompanyData);
            $creatingCompany->setName($request->getParam(CompanyInterface::NAME));
            $creatingCompany->setStreet($request->getParam(CompanyInterface::STREET));
            $creatingCompany->setCity($request->getParam(CompanyInterface::CITY));
            $creatingCompany->setPostcode($request->getParam(CompanyInterface::POSTCODE));
            $creatingCompany->setCountryId($request->getParam(CompanyInterface::COUNTRY_ID));
            $creatingCompany->setRegionId($request->getParam(CompanyInterface::REGION_ID));
            $creatingCompany->setRegion($request->getParam(CompanyInterface::REGION));
            $creatingCompany->setTelephone($request->getParam(CompanyInterface::TELEPHONE));
            $creatingCompany->setParentCompanyId($request->getParam(CompanyInterface::PARENT_COMPANY_ID));
            $creatingCompany->setStatus(CompanyInterface::STATUS_ACTIVE);
            if ($company = $this->companyRepository->save($creatingCompany)) {
                $this->messageManager->addSuccessMessage(
                    __('New Sub Company has been successfully created')
                );
                $this->_eventManager->dispatch(
                    'lambda_company_save_after_custom',
                    ['company' => $company]
                );
                $this->assignCompanyToCurrentStoreView($company);
                if ($customerGroup = $this->createCustomerGroupForCreatedCompany($company)) {
                    $company->setCustomerGroupId($customerGroup);
                    $this->companyRepository->save($company);
                }
                $resultRedirect->setPath('*/profile/index');
            } else {
                $this->messageManager->addErrorMessage(
                    __('Something went wrong, during creating new Sub Company.')
                );
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }

        return $resultRedirect;
    }

    /**
     * Create Customer Group for creating Company
     *
     * @param CompanyInterface $company Company
     *
     * @return integer
     */
    protected function createCustomerGroupForCreatedCompany(CompanyInterface $company)
    {
        $customerGroupName = $this->companyParentHierarchy->companyBelongsToCustomerGroupName($company);
        $existedCustomerGroup = $this->customerGroupFactory->create()->load($customerGroupName, 'customer_group_code');
        if (!$existedCustomerGroup->getId()) {
            $createdCustomerGroup = $this->customerGroupFactory->create()->setCode($customerGroupName)
                ->setTaxClassId(GroupRepository::DEFAULT_TAX_CLASS_ID)
                ->save();
            return $createdCustomerGroup->getId();
        }

        return $existedCustomerGroup->getId();
    }

    /**
     * Assign company to current store view
     *
     * @param CompanyInterface $company Company
     *
     * @return void
     */
    private function assignCompanyToCurrentStoreView(CompanyInterface $company): void
    {
        $assignment = $this->storeAssignedCompaniesModel->create();
        $assignment->setStoreId($this->storeManager->getStore()->getId())
            ->setCompanyId($company->getId())
            ->save();
    }

    /**
     * Separate ony needed data from request, for saving new Company entity
     *
     * @param array $companyDataFromRequest Company data from request
     *
     * @return array
     */
    private function prepareCompanyDataFromRequest(array $companyDataFromRequest): array
    {
        $companyData = [];
        foreach ($companyDataFromRequest as $fieldKey => $fieldValue) {
            if (in_array($fieldKey, Company::LIST_OF_REQUIRED_FIELDS)) {
                $companyData[$fieldKey] = $fieldValue;
            }
        }

        return $companyData;
    }

    /**
     * Validate request
     *
     * @param RequestInterface $request Request
     *
     * @return boolean
     */
    private function isRequestValidForCompanyCreate(RequestInterface $request): bool
    {
        foreach (Company::LIST_OF_REQUIRED_FIELDS as $field) {
            if (!$request->getParam($field) || empty($request->getParam($field))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Validate company name usage (for another company)
     *
     * @param RequestInterface $request Request
     *
     * @return boolean
     */
    private function isAnotherCompanyWithSuchNameAlreadyExists(RequestInterface $request): bool
    {
        $companyName = $request->getParam('name');
        $possiblyExistedCompany = $this->companyRepository->getByName($companyName);
        if ($possiblyExistedCompany->getId()) {
            return true;
        }
        return false;
    }
}
