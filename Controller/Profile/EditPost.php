<?php
/**
 * File EditPost
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Controller\Profile;

use \Psr\Log\LoggerInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Action\{Context, Action};
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Customer\Api\GroupRepositoryInterface;
use Lambda\CompanyAccount\Api\Data\CompanyInterface;
use Lambda\CompanyAccount\Api\CompanyRepositoryInterface;
use Lambda\CompanyAccount\Model\Company;
use Lambda\CompanyAccount\Helper\{FrontParentCompany, CompanyParentHierarchy};

/**
 * Class EditPost
 */
class EditPost extends Action
{
    /**
     * Logger
     *
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Company repository
     *
     * @var CompanyRepositoryInterface
     */
    private $companyRepository;

    /**
     * Form key validator
     *
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    private $formKeyValidator;

    /**
     * Helper for get list of possible parent companies
     *
     * @var FrontParentCompany
     */
    protected $frontParentCompanyHelper;

    /**
     * Search criteria builder
     *
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * Customer group repository
     *
     * @var GroupRepositoryInterface
     */
    protected $groupRepository;

    /**
     * Company parent hierarchy helper
     *
     * @var CompanyParentHierarchy
     */
    protected $companyParentHierarchy;

    /**
     * EditPost constructor.
     *
     * @param Context                    $context                  Context
     * @param Validator                  $formKeyValidator         Form key validator
     * @param LoggerInterface            $logger                   Logger
     * @param CompanyRepositoryInterface $companyRepository        Company repository
     * @param FrontParentCompany         $frontParentCompanyHelper Helper for get list of possible parent companies
     * @param SearchCriteriaBuilder      $searchCriteriaBuilder    Search criteria builder
     * @param GroupRepositoryInterface   $groupRepository          Customer group repository
     * @param CompanyParentHierarchy     $companyParentHierarchy   Company hierarchy helper
     */
    public function __construct(
        Context $context,
        Validator $formKeyValidator,
        LoggerInterface $logger,
        CompanyRepositoryInterface $companyRepository,
        FrontParentCompany $frontParentCompanyHelper,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        GroupRepositoryInterface $groupRepository,
        CompanyParentHierarchy $companyParentHierarchy
    ) {
        parent::__construct($context);
        $this->formKeyValidator = $formKeyValidator;
        $this->logger = $logger;
        $this->companyRepository = $companyRepository;
        $this->frontParentCompanyHelper = $frontParentCompanyHelper;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->groupRepository = $groupRepository;
        $this->companyParentHierarchy = $companyParentHierarchy;
    }

    /**
     * Edit company profile form.
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $request = $this->getRequest();
        $resultRedirect = $this->resultRedirectFactory->create()->setPath('*/profile/');
        try {
            if (!$this->formKeyValidator->validate($request)) {
                $this->messageManager->addErrorMessage(__('Form key is no longer valid, please refresh the page'));
                throw new \Exception('Form key, from company profile update form, is not valid');
            }
            if (!$this->isRequestValidForCompanyUpdate($request)) {
                $this->messageManager->addErrorMessage(__('Company profile update request is not valid.'));
                throw new \Exception('Company profile update request is not valid (id or name is not valid)');
            }
            if ($this->isAnotherCompanyWithSuchNameAlreadyExists($request)) {
                $this->messageManager->addErrorMessage(__('Another Company with such name already exists.'));
                throw new \Exception(
                    'Another company with such name already exists [' .
                    $request->getParam(Company::NAME) . ']'
                );
            }
            $updatingCompany = $this->companyRepository->get($request->getParam('company_id'));
            $updatingCompanyNameBeforeUpdate = $updatingCompany->getName();
            $updatingCompany->setName($request->getParam(CompanyInterface::NAME));
            $updatingCompany->setStreet($request->getParam(CompanyInterface::STREET));
            $updatingCompany->setCity($request->getParam(CompanyInterface::CITY));
            $updatingCompany->setPostcode($request->getParam(CompanyInterface::POSTCODE));
            $updatingCompany->setCountryId($request->getParam(CompanyInterface::COUNTRY_ID));
            $updatingCompany->setRegionId($request->getParam(CompanyInterface::REGION_ID));
            $updatingCompany->setRegion($request->getParam(CompanyInterface::REGION));
            $updatingCompany->setTelephone($request->getParam(CompanyInterface::TELEPHONE));
            $updatingCompany->setStatus((integer)$request->getParam(CompanyInterface::STATUS));
            $this->companyRepository->save($updatingCompany);
            $this->_eventManager->dispatch(
                'lambda_company_save_after_custom',
                ['company' => $updatingCompany]
            );
            if ($this->getRequest()->getParam('name') != $updatingCompanyNameBeforeUpdate) {
                $customerGroup = $this->groupRepository->getById($updatingCompany->getCustomerGroupId());
                $customerGroup->setCode($this->companyParentHierarchy->companyBelongsToCustomerGroupName($updatingCompany));
                $this->groupRepository->save($customerGroup);
                $childCompaniesIds = $this->frontParentCompanyHelper->getAllChildrenByParentCompany(
                    $updatingCompany->getId()
                );
                $this->renameCustomerGroupForAllChildren($childCompaniesIds);
            }

            $this->messageManager->addSuccessMessage(
                __('The changes you made on the Company profile have been successfully saved.')
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            $this->messageManager->addErrorMessage(
                __('Something went wrong, during updating Company details.')
            );
        }

        return $resultRedirect;
    }

    /**
     * Validate request
     *
     * @param RequestInterface $request Request
     *
     * @return boolean
     */
    private function isRequestValidForCompanyUpdate(RequestInterface $request): bool
    {
        $fields = Company::LIST_OF_REQUIRED_FIELDS;
        array_push($fields, 'company_id');
        foreach ($fields as $field) {
            if (!$request->getParam($field) || empty($request->getParam($field))) {
                return false;
            }
        }

        return true;
    }

    /**
     * Validate company name usage (for another company)
     *
     * @param RequestInterface $request Request
     *
     * @return boolean
     */
    private function isAnotherCompanyWithSuchNameAlreadyExists(RequestInterface $request): bool
    {
        $companyName = $request->getParam('name');
        $companyId = $request->getParam('company_id');
        $possiblyExistedCompany = $this->companyRepository->getByName($companyName);
        if ($possiblyExistedCompany->getId() && ($possiblyExistedCompany->getId() != $companyId)) {
            return true;
        }

        return false;
    }

    /**
     * Rename customer group for all children companies
     *
     * @param array $companiesIds Child companies, before reset
     *
     * @return void
     */
    protected function renameCustomerGroupForAllChildren($companiesIds)
    {
        $searchCriteria = $criteria = $this->searchCriteriaBuilder->addFilter(CompanyInterface::COMPANY_ID, $companiesIds, 'in')
            ->create();
        $companies = $this->companyRepository->getList($searchCriteria);
        foreach ($companies->getItems() as $company) {
            $customerGroup = $this->groupRepository->getById($company->getCustomerGroupId());
            $customerGroup->setCode($this->companyParentHierarchy->companyBelongsToCustomerGroupName($company));
            $this->groupRepository->save($customerGroup);
        }
    }
}
