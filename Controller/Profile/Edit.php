<?php
/**
 * File Edit
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Controller\Profile;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Lambda\CompanyAccount\Controller\AbstractBase;
use Lambda\CompanyAccount\Api\CompanyRepositoryInterface;
use Lambda\CompanyAccount\Model\CompanyFactory;
use Lambda\CompanyAccount\Helper\CustomerIsCompanyAdmin;
use \Lambda\CompanyAccount\Model\ResourceModel\Company as CompanyResource;

/**
 * Class Edit
 */
class Edit extends AbstractBase
{
    /**
     * Company model
     *
     * @var CompanyFactory
     */
    protected $companyModel;

    /** @var CompanyResource  */
    private $companyResource;

    /**
     * Edit constructor.
     *
     * @param CustomerIsCompanyAdmin     $customerIsAdminHelper Customer is admin helper
     * @param Context                    $context               Context
     * @param PageFactory                $resultPageFactory     Result page
     * @param CompanyFactory             $companyModel          Company model
     * @param CompanyRepositoryInterface $companyRepository     Company repository
     */
    public function __construct(
        CustomerIsCompanyAdmin $customerIsAdminHelper,
        Context $context,
        PageFactory $resultPageFactory,
        CompanyFactory $companyModel,
        CompanyRepositoryInterface $companyRepository,
        CompanyResource $companyResource
    ) {
        parent::__construct($customerIsAdminHelper, $context, $resultPageFactory, $companyRepository);
        $this->companyModel = $companyModel;
        $this->companyResource = $companyResource;
    }

    /**
     * Edit sub companies(profiles)
     *
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $companyModel = $this->companyModel->create();
        $this->companyResource->load($companyModel, $this->getRequest()->getParam('id'));
        if (!$companyModel->getId()) {
            $this->messageManager->addErrorMessage(
                __('Profile with such Id doesn`t exist')
            );
            return $this->resultRedirectFactory->create()->setPath('*/*/index');
        }
        /**
         * Result page factory
         *
         * @var \Magento\Framework\View\Result\Page $resultPage
         */
        $resultPage = $this->resultPageFactory->create();
        $layout = $resultPage->getLayout();
        /**
         * Navigation block
         *
         * @var \Magento\Framework\View\Element\Html\Links $navigationBlock
         */
        $navigationBlock = $layout->getBlock('customer_account_navigation');
        if ($navigationBlock) {
            $navigationBlock->setActive('company/profile/index');
        }

        return $resultPage;
    }
}
