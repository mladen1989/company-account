<?php
/**
 * File ListAction
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Controller\Profile;

use Magento\Framework\App\Action\{Context, Action};
use Magento\Framework\Controller\ResultFactory;
use Lambda\CompanyAccount\Helper\{FrontParentCompany, CustomerIsCompanyAdmin, CurrentStoreCompaniesList};

/**
 * Class ListAction
 */
class ListAction extends Action
{
    /**
     * Customer Is Admin helper
     *
     * @var CustomerIsCompanyAdmin
     */
    protected $customerIsAdminHelper;

    /**
     * Helper for get list of possible parent companies
     *
     * @var FrontParentCompany
     */
    protected $frontParentCompanyHelper;

    /**
     * Current store companies helper
     *
     * @var CurrentStoreCompaniesList
     */
    protected $companiesListHelper;

    /**
     * ListAction constructor.
     *
     * @param Context                   $context                  Context
     * @param CustomerIsCompanyAdmin    $customerIsAdminHelper    Customer Is Admin helper
     * @param FrontParentCompany        $frontParentCompanyHelper Helper for get list of possible parent companies
     * @param CurrentStoreCompaniesList $companiesListHelper      Companies helper
     */
    public function __construct(
        Context $context,
        CustomerIsCompanyAdmin $customerIsAdminHelper,
        FrontParentCompany $frontParentCompanyHelper,
        CurrentStoreCompaniesList $companiesListHelper
    ) {
        parent::__construct($context);
        $this->customerIsAdminHelper = $customerIsAdminHelper;
        $this->frontParentCompanyHelper = $frontParentCompanyHelper;
        $this->companiesListHelper = $companiesListHelper;
    }

    /**
     * Get list of available Companies
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $companiesIds =  $this->frontParentCompanyHelper->getAllChildrenByParentCompany(
            $this->customerIsAdminHelper->getAdminCompanyId()
        );
        //add parent company at the beginning
        array_unshift($companiesIds, $this->customerIsAdminHelper->getAdminCompanyId());
        $activeCompaniesIds = array_map(function ($activeCompany) {
            return $activeCompany['value'];
        }, $this->companiesListHelper->getCurrentStoreActiveCompanies());
        $companies = [];
        if ($activeCompaniesIds) {
            foreach ($this->frontParentCompanyHelper->getCompaniesByIds($companiesIds) as $company) {
                if (in_array($company['value'], $activeCompaniesIds)) {
                    $companies[] = $company;
                }
            }
        }
        /**
         * Response
         *
         * @var $response \Magento\Framework\Controller\Result\Json
         */
        $response = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        return $response->setData(
            [
                'companies' => $companies
            ]
        );
    }
}
