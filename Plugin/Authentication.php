<?php
/**
 * File Authentication
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Plugin;

use Magento\Framework\App\{RequestInterface, ActionInterface};
use \Magento\Customer\Model\{Url, Session};

/**
 * Class Authentication
 */
class Authentication
{
    /**
     * Customer url
     *
     * @var Url
     */
    protected $customerUrl;

    /**
     * Customer session
     *
     * @var Session
     */
    protected $customerSession;

    /**
     * Authentication constructor.
     *
     * @param Url     $customerUrl     Customer url
     * @param Session $customerSession Customer session
     */
    public function __construct(
        Url $customerUrl,
        Session $customerSession
    ) {
        $this->customerUrl = $customerUrl;
        $this->customerSession = $customerSession;
    }

    /**
     * Authenticate user
     *
     * @param ActionInterface  $subject Subject
     * @param RequestInterface $request Request
     *
     * @return void
     */
    public function beforeDispatch(
        ActionInterface $subject,
        RequestInterface $request
    ) {
        $loginUrl = $this->customerUrl->getLoginUrl();
        $isLoggedIn = $this->customerSession->authenticate($loginUrl);
        if (!$isLoggedIn) {
            $subject->getActionFlag()->set('', $subject::FLAG_NO_DISPATCH, true);
        }
    }
}
