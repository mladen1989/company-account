<?php
/**
 * File CreatePostPlugin
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Plugin;

use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Customer\Controller\Account\CreatePost;
use Lambda\CompanyAccount\Helper\CurrentStoreCompaniesList;

/**
 * Validate everything before go to customer create controller
 */
class CreatePostPlugin
{
    /**
     * Result redirect controller
     *
     * @var RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * Message manager
     *
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * Companies list helper
     *
     * @var CurrentStoreCompaniesList
     */
    protected $companiesListHelper;

    /**
     * Config
     *
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * CreatePostPlugin constructor.
     *
     * @param RedirectFactory           $resultRedirectFactory Result redirect factory
     * @param ManagerInterface          $messageManager        Message helper
     * @param CurrentStoreCompaniesList $companiesListHelper   Companies list helper
     * @param ScopeConfigInterface      $scopeConfig           Scope config
     */
    public function __construct(
        RedirectFactory $resultRedirectFactory,
        ManagerInterface $messageManager,
        CurrentStoreCompaniesList $companiesListHelper,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->messageManager = $messageManager;
        $this->companiesListHelper = $companiesListHelper;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Around plugin method
     *
     * @param CreatePost $subject Controller to extend
     * @param \Closure   $proceed Closure function
     *
     * @return mixed
     */
    public function aroundExecute(
        CreatePost $subject,
        \Closure $proceed
    ) {
        $companyFieldIsRequired = $this->scopeConfig->isSetFlag('company_account_config/general/is_required');
        $companyLabel = $this->scopeConfig->getValue('company_account_config/general/label');
        $redirect = false;
        if ($companyFieldIsRequired && !$subject->getRequest()->getParam('company_id')) {
            $redirect = true;
            $this->messageManager->addErrorMessage(__('"%1" is a required value.', $companyLabel));
        }
        if ($subject->getRequest()->getParam('company_id') && !$this->companyIsValid($subject->getRequest())) {
            $redirect = true;
            $this->messageManager->addErrorMessage(__('Selected %1 is not valid.', $companyLabel));
        }
        if ($redirect) {
            /**
             * Result redirect
             *
             * @var \Magento\Framework\Controller\Result\Redirect $resultRedirect
             */
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/create');
        }

        return $proceed();
    }

    /**
     * Validate selected company
     *
     * @param RequestInterface $request Request
     *
     * @return boolean
     */
    protected function companyIsValid(
        RequestInterface $request
    ): bool {
        if (!$request->getParam('company_id')) {
            return false;
        }
        //validate company by active status
        return (in_array(
            $request->getParam('company_id'),
            array_keys($this->companiesListHelper->getCurrentStoreActiveCompanies())
        ));
    }
}
