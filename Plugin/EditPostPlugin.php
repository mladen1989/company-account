<?php
/**
 * File EditPostPlugin
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Plugin;

use Lambda\CompanyAccount\Helper\Data;
use Magento\Framework\App\RequestInterface;
use Magento\Customer\Controller\Account\EditPost;
use Magento\Customer\Model\Session;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Lambda\CompanyAccount\Api\CompanyRepositoryInterface;
use Lambda\CompanyAccount\Helper\CurrentStoreCompaniesList;

/**
 * Validate everything before go to save customer details controller (from customer account section)
 */
class EditPostPlugin
{
    /**
     * Customer repository
     *
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * Customer Session
     *
     * @var Session
     */
    protected $customerSession;

    /**
     * Companies list helper
     *
     * @var CurrentStoreCompaniesList
     */
    protected $companiesListHelper;

    /**
     * Company repository
     *
     * @var CompanyRepositoryInterface
     */
    protected $companyRepository;

    /**
     * EditPostPlugin constructor.
     *
     * @param Session                     $customerSession     Customer session
     * @param CustomerRepositoryInterface $customerRepository  Customer repository
     * @param CurrentStoreCompaniesList   $companiesListHelper Companies list helper
     * @param CompanyRepositoryInterface  $companyRepository  Company repository
     */
    public function __construct(
        Session $customerSession,
        CustomerRepositoryInterface $customerRepository,
        CurrentStoreCompaniesList $companiesListHelper,
        CompanyRepositoryInterface $companyRepository
    ) {
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
        $this->companiesListHelper = $companiesListHelper;
        $this->companyRepository = $companyRepository;
    }

    /**
     * After plugin
     *
     * @param EditPost $subject EditPost controller
     * @param mixed    $result  Result
     *
     * @return mixed
     */
    public function afterExecute(
        EditPost $subject,
        $result
    ) {
        $request = $subject->getRequest();
        $companyId = (int)$request->getParam('company');
        try {
            if (!$request->getParam('change_company')) {
                throw new \Exception('Company shouldn`t be changed');
            }
            if ((0 != $companyId) && !$this->validateCompany((int)$companyId)) {
                throw new \Exception('Company id is not valid');
            }
            $customer = $this->customerRepository->getById($this->customerSession->getCustomerId());
            $company = $this->companyRepository->get($companyId);
            if ($company->getCustomerGroupId()) {
                $customer->setGroupId($company->getCustomerGroupId());
            }
            $customer->setCustomAttribute(Data::CUSTOMER_ATTRIBUTE_COMPANY_ID, $companyId);
            $this->customerRepository->save($customer);
        } catch (\Exception $e) {

        }

        return $result;
    }

    /**
     * Validate selected company
     *
     * @param integer $companyId CompanyId
     *
     * @return boolean
     */
    protected function validateCompany($companyId)
    {
        if (!in_array($companyId, array_keys($this->companiesListHelper->getCurrentStoreCompanies()))) {
            return false;
        }
        return true;
    }

    /**
     * Define, is company should be changed for customer
     *
     * @param RequestInterface $request Request
     *
     * @return boolean
     */
    protected function isCompanyShouldBeChanged(RequestInterface $request): bool
    {
        if ($request->getParam('change_company') && $request->getParam('company')) {
            return true;
        }
        return false;
    }
}
