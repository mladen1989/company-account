<?php
/**
 * File ValidatePlugin
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Plugin\Controller\Adminhtml\Index;

use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Api\AttributeInterface;
use Magento\Customer\Controller\Adminhtml\Index\Save;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Lambda\CompanyAccount\Helper\{Data, CompanyChildrenHierarchy};
use Lambda\CompanyAccount\Api\CompanyRepositoryInterface;

/**
 * Class ValidatePlugin
 */
class SavePlugin
{
    /**
     * Redirect factory
     *
     * @var RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * Message manager
     *
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * Company repository
     *
     * @var CompanyRepositoryInterface
     */
    protected $companyRepository;

    /**
     * Company children hierarchy helper
     *
     * @var CompanyChildrenHierarchy
     */
    protected $companyChildrenHelper;

    /** @var CustomerRepositoryInterface  */
    private $customerRepository;

    /**
     * SavePlugin constructor.
     *
     * @param RedirectFactory             $resultRedirectFactory Result redirect factory
     * @param ManagerInterface            $messageManager        Message manager
     * @param CustomerRepositoryInterface $customerRepository    Customer repository
     * @param CompanyRepositoryInterface  $companyRepository     Company repository
     * @param CompanyChildrenHierarchy    $companyChildrenHelper Company children factory
     */
    public function __construct(
        RedirectFactory $resultRedirectFactory,
        ManagerInterface $messageManager,
        CustomerRepositoryInterface $customerRepository,
        CompanyRepositoryInterface $companyRepository,
        CompanyChildrenHierarchy $companyChildrenHelper
    ) {
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->messageManager = $messageManager;
        $this->customerRepository = $customerRepository;
        $this->companyRepository = $companyRepository;
        $this->companyChildrenHelper = $companyChildrenHelper;
    }

    /**
     * Changed default execute method. Implemented
     *
     * @param Save     $subject Extending class
     * @param \Closure $proceed Closure function
     *
     * @return \Magento\Framework\Controller\Result\Redirect|mixed
     */
    public function aroundExecute(
        Save $subject,
        \Closure $proceed
    ) {
        $requestParams = $subject->getRequest()->getParams();
        if (isset($requestParams['customer_id']) && (isset($requestParams['customer']['group_id'])) && (isset($requestParams['customer']['company_id']))) {
            try {
                $redirectFlag = false;
                $customer = $this->customerRepository->getById($requestParams['customer_id']);
                if (!$this->doesConditionsSatisfiedToContinue($requestParams, $customer)) {
                    $this->messageManager->addErrorMessage(__('Please check that company and group are aligned.'));
                    $redirectFlag = true;
                }
                if (!$redirectFlag && $this->newCustomerGroupNotBelongsToAnyCompanyChild($requestParams, $customer)) {
                    $this->messageManager->addErrorMessage(__('The selected customer group is not satisfied conditions.'));
                    $redirectFlag = true;
                }
                if ($redirectFlag) {
                    $resultRedirect = $this->resultRedirectFactory->create();
                    $resultRedirect->setPath(
                        'customer/*/edit',
                        ['id' => $requestParams['customer_id'], '_current' => true]
                    );
                    return $resultRedirect;
                }
            } catch (\Exception $e) {
                //do not need to cache errors
            }
        }

        return $proceed();
    }

    /**
     * Check, does new value of customer_group belongs to any sub company customer_group
     *
     * @param array             $requestParams Request params
     * @param CustomerInterface $customer      Customer
     *
     * @return boolean
     */
    protected function newCustomerGroupNotBelongsToAnyCompanyChild($requestParams, CustomerInterface $customer)
    {
        $customerCompanyAttr = $customer->getCustomAttribute(Data::CUSTOMER_ATTRIBUTE_COMPANY_ID);
        if ($this->checkDoesCustomerCompanyGoingToBeChanged($requestParams['customer']['company_id'], $customerCompanyAttr)) {
            return false;
        }
        if (!$this->checkDoesCustomerGroupGoingToBeChanged($requestParams['customer']['group_id'], $customer->getGroupId())) {
            return false;
        }

        $newCustomerGroupRelatedCompany = $this->companyRepository->getByGroupId($requestParams['customer']['group_id']);
        //old, means value which is currently exists in DB (before update)
        $oldCustomerGroupRelatedCompany = $this->companyRepository->getByGroupId($customer->getGroupId());
        if (!$newCustomerGroupRelatedCompany->getId() || !$oldCustomerGroupRelatedCompany->getId()) {
            return false;
        }
        //if new customer group is lower in hiearchy of sub companies (of the company,
        //which belongs to old customer group) - return true
        return !in_array($requestParams['customer']['group_id'], $this->companyChildrenHelper->getAllChildrenCompaniesCustomerGroupIdRecursively($oldCustomerGroupRelatedCompany));
    }

    /**
     * Check, does Admin allow to change attributes to selected values
     *
     * @param array             $requestParams Request params
     * @param CustomerInterface $customer      Customer
     *
     * @return boolean
     */
    protected function doesConditionsSatisfiedToContinue($requestParams, CustomerInterface $customer)
    {
        $customerGroupId = $customer->getGroupId();
        $customerCompanyAttr = $customer->getCustomAttribute(Data::CUSTOMER_ATTRIBUTE_COMPANY_ID);
        $customerGroupIsChanged = $this->checkDoesCustomerGroupGoingToBeChanged($requestParams['customer']['group_id'], $customer->getGroupId());
        $customerCompanyIsChanged = $this->checkDoesCustomerCompanyGoingToBeChanged($requestParams['customer']['company_id'], $customerCompanyAttr);
        if ($customerGroupIsChanged &&
            ($customerCompanyIsChanged) &&
            (!$this->requestCustomerGroupBelongsToCustomerCompany($requestParams))
        ) {
            return false;
        }
        return true;
    }

    /**
     * Check, does new value of customer_group is anywhere lower in the hirarchy inside old customer_group value
     *
     * @param array $requestParams Request params
     *
     * @return boolean
     */
    protected function requestCustomerGroupBelongsToCustomerCompany($requestParams)
    {
        try {
            $company = $this->companyRepository->get($requestParams['customer']['company_id']);
            return ($company->getCustomerGroupId() == $requestParams['customer']['group_id']);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Check, does Admin going to change customer group
     *
     * @param integer $customerGroupIdFromRequest Customer Id from request
     * @param integer $currentCustomerGroupId     Current customer group id
     *
     * @return boolean
     */
    protected function checkDoesCustomerGroupGoingToBeChanged($customerGroupIdFromRequest, $currentCustomerGroupId)
    {
        return (integer)$customerGroupIdFromRequest !== (integer)$currentCustomerGroupId;
    }

    /**
     * Check, does Admin going to change customer company
     *
     * @param integer                                        $customerCompanyIdFromRequest    Customer company id from request
     * @param \Magento\Framework\Api\AttributeInterface|null $currentCustomerCompanyAttribute Current customer company attribute
     *
     * @return boolean
     */
    protected function checkDoesCustomerCompanyGoingToBeChanged($customerCompanyIdFromRequest, $currentCustomerCompanyAttribute)
    {
        if(empty($currentCustomerCompanyAttribute) && !empty($customerCompanyIdFromRequest))
        {
            return true;
        }

        if($currentCustomerCompanyAttribute)
        {
            return ((integer)$currentCustomerCompanyAttribute->getValue() !== (integer)$customerCompanyIdFromRequest);
        }
    }
}
