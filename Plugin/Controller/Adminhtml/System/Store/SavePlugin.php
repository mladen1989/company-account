<?php
/**
 * File SavePlugin
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Plugin\Controller\Adminhtml\System\Store;

use Lambda\CompanyAccount\Model\ResourceModel\StoreAssignedCompanies\CollectionFactory
    as StoreAssignedCompaniesCollection;
use Lambda\CompanyAccount\Model\{StoreAssignedCompanies, StoreAssignedCompaniesFactory};
use Magento\Backend\Controller\Adminhtml\System\Store\Save;

/**
 * Class SavePlugin
 */
class SavePlugin
{
    /**
     * Collection of the assignments companies to stores
     *
     * @var StoreAssignedCompaniesCollection
     */
    protected $storeCompaniesCollection;

    /**
     * Store to company relation model
     *
     * @var StoreAssignedCompaniesFactory
     */
    protected $storeAssignedCompaniesModel;

    /**
     * SavePlugin constructor.
     *
     * @param StoreAssignedCompaniesCollection $storeCompaniesCollection    Collection
     * @param StoreAssignedCompaniesFactory    $storeAssignedCompaniesModel Model
     */
    public function __construct(
        StoreAssignedCompaniesCollection $storeCompaniesCollection,
        StoreAssignedCompaniesFactory $storeAssignedCompaniesModel
    ) {
        $this->storeCompaniesCollection = $storeCompaniesCollection;
        $this->storeAssignedCompaniesModel = $storeAssignedCompaniesModel;
    }

    /**
     * Main plugin method
     *
     * @param Save  $subject Subject
     * @param mixed $result  Result
     *
     * @return \Magento\Backend\Model\View\Result\Redirect\Interceptor
     */
    public function afterExecute(
        Save $subject,
        $result
    ) {
        if ('store' == $subject->getRequest()->getParam('store_type')) {
            $storeId = $subject->getRequest()->getParam('store')['store_id'];
            $companies = $subject->getRequest()->getParam('companies', []);
            if (!empty($storeId)) {
                //clear old assignments
                $this->clearStoreToCompaniesAssignments((int) $storeId);
                //create new ones
                $this->createNewStoreToCompaniesAssignments(
                    (int) $storeId,
                    $companies
                );
            }
        }

        return $result;
    }

    /**
     * Clear all assignments, related with selected store id
     *
     * @param integer $storeId Store id
     *
     * @return void
     */
    protected function clearStoreToCompaniesAssignments($storeId)
    {
        $collection = $this->storeCompaniesCollection->create();
        $collection->addFieldToFilter(StoreAssignedCompanies::STORE_ID, $storeId);
        $collection->walk('delete');
    }

    /**
     * Create new companies to store assignments
     *
     * @param integer $storeId      Store id
     * @param array   $companiesIds Companies ids
     *
     * @return void
     */
    protected function createNewStoreToCompaniesAssignments(
        $storeId,
        array $companiesIds
    ) {
        foreach ($companiesIds as $companyId) {
            $assignment = $this->storeAssignedCompaniesModel->create();
            $assignment->setStoreId($storeId)
                ->setCompanyId($companyId)
                ->save();
        }
    }
}
