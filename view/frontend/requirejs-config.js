var config = {
    map: {
        '*': {
            uploadCustomers: 'Lambda_CompanyAccount/js/upload-customers',
            changeCompany: 'Lambda_CompanyAccount/js/change-company',
            companyValidationRule: 'Lambda_CompanyAccount/js/companyValidationRule',
            customCheckbox: 'Lambda_CompanyAccount/js/custom-checkbox',
            customersGrid: 'Lambda_CompanyAccount/js/customers-grid',
        }
    }
};
