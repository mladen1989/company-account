define(
    [
    'jquery',
    'jquery/ui'
    ], function ($) {
    'use strict';

    $.widget('lambda.changeCompany', {
        options: {

        },
        selectors: {
            changeCompanyContainer: '[data-container=change-company]',
            changeCompany: '[data-role=change-company]'
        },

        /**
         * Create widget
         * @private
         */
        _create: function () {
            this._hideChangeCompanyContainer();
            this._bindDocumentEvents();
        },
        /**
         * Show container with companies list
         * @private
         */
        _showChangeCompanyContainer: function() {
            $(this.selectors.changeCompanyContainer).show();
        },
        /**
         * Hide container with companies list
         * @private
         */
        _hideChangeCompanyContainer: function() {
            $(this.selectors.changeCompanyContainer).hide();
        },
        /**
         * Bind events on Dom
         * @private
         */
        _bindDocumentEvents: function () {
            var self = this;
            $(document).on('click', this.selectors.changeCompany, function() {
                if ($(self.selectors.changeCompany).is(':checked')) {
                    self._showChangeCompanyContainer();
                } else {
                    self._hideChangeCompanyContainer();
                }
            });
        }
    });

    return $.lambda.changeCompany;
});
