define(
    [
        'jquery',
        'jquery/ui',
        'mage/validation',
        'Magento_Ui/js/modal/alert',
        'mage/url'
    ], function ($, ui, validation, alert, urlBuilder) {
        'use strict';
        $.widget('lambda.uploadCustomers', {
            options: {
                failData: {
                    title: $.mage.__('Error'),
                    message: $.mage.__('An error occurred. Please contact support')
                },
                allowedFormats: '',
                failImport: {
                    title: $.mage.__('Failed import'),
                },
                successImport: {
                    title: $.mage.__('Successful import'),
                },
            },
            selectors: {
                uploadCustomersForm: 'upload-customers',
                uploadCustomersFormSubmitButton: '#upload-customers button[type="submit"]',
                fileUploadInput: 'file'
            },
            /**
             * Create widget
             * @private
             */
            _create: function () {
                let self = this;
                $(this.selectors.uploadCustomersFormSubmitButton).on('click', function (e) {
                        e.preventDefault();
                        if (self._validateForm()) {
                            self._sendFile(self._preparedSendData());
                        } else {
                            alert({
                                title: 'something goes wrong',
                                content: 'TODO: implement correct message and action',
                            })
                        }
                    }
                );
            },
            /**
             * Validate form filling, file format
             * @returns {boolean}
             * @private
             */
            _validateForm: function() {
                let self = this,
                    dataForm = $('#'+this.selectors.uploadCustomersForm);
                dataForm.mage('validation', {});
                if (!dataForm.validation('isValid')) {
                    return false;
                }
                let selectedFile = document.getElementById(self.selectors.fileUploadInput).files[0],
                    fileExtension = selectedFile.name.split('.').slice(-1).pop();
                if (!self.options.allowedFormats.split(', ').includes(fileExtension)) {
                    return false;
                }
                return true
            },

            _sendFile: function(data) {
                let self = this;
                $.ajax(
                    {
                        method: 'POST',
                        url: $('#' + self.selectors.uploadCustomersForm).attr('action'),
                        data: data,
                        processData: false,
                        contentType: false,
                        beforeSend: function () {
                            self.actionsListBeforeSendAjax();
                        },
                        success: function (data) {
                            if (data.success) {
                                self.actionListAfterSuccess(data);
                            } else {
                                self.actionsListAfterFail(data);
                            }

                        },
                        error: function (data) {
                            self.actionsListAfterFail(data);
                        }
                    }
                );
            },

            _preparedSendData: function () {
                let self = this,
                    formData = new FormData();
                formData.append('file', $('#' + self.selectors.fileUploadInput)[0].files[0]);
                return formData;

            },
            /**
             * General list of actions, that should be run before ajax call
             * @return void
             */
            actionsListBeforeSendAjax: function () {
                this.enableLoader();
            },
            /**
             * General list of actions after SUCCESS ajax call
             *
             * @param data
             * @return void
             */
            actionListAfterSuccess: function (data) {
                this.disableLoader();
                this.successMessage(data.message);
            },
            /**
             * General list of actions, after FAIL ajax call
             *
             * @param data
             * @return void
             */
            actionsListAfterFail: function (data) {
                let self = this;
                this.disableLoader();
                this.failMessage(data.message !== undefined ? data.message : self.options.failData.message);
            },
            /**
             * Enable general loader
             * @return void
             */
            enableLoader: function () {
                $('body').loader('show');
            },
            /**
             * Disable general loader
             * @return void
             */
            disableLoader: function () {
                $('body').loader('hide');
            },
            /**
             * Show custom FAIL alert message
             * @param message
             */
            failMessage: function (message) {
                let self = this;
                alert(
                    {
                        title: self.options.failImport.title,
                        content: message,
                        actions: {
                            always: function () {
                                location.reload();
                            }
                        }
                    }
                );
            },
            /**
             * Show custom SUCCESS alert message
             * @param message
             */
            successMessage: function (message) {
                let self = this;
                alert(
                    {
                        title: self.options.successImport.title,
                        content: message,
                        actions: {
                            always: function () {
                                location.href = urlBuilder.build('company/customers/index')
                            }
                        }
                    }
                );
            },
        });

        return $.lambda.uploadCustomers;
    });
