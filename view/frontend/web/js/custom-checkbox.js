define(
    [
        'jquery'
    ], function ($) {
        'use strict';

        $.widget('lambda.customCheckbox', {
            options: {

            },
            selectors: {
                customCheckbox: '.frontend-custom-checkbox'
            },

            /**
             * Create widget
             * @private
             */
            _create: function () {
                this.initSwitcherChangeValue();
            },
            /**
             * Show container with companies list
             * @private
             */
            initSwitcherChangeValue: function() {
                $(this.selectors.customCheckbox).on('change', function() {
                    if ($(this).is(':checked')) {
                        $(this).val(1);
                    } else {
                        $(this).val(0);
                    }
                });
            }
        });

        return $.lambda.customCheckbox;
    });
