requirejs([
    'jquery',
    'jquery/ui',
    'jquery/validate',
    'mage/translate'
], function($){
    'use strict';
    $.validator.addMethod(
        'companyValidationRule',
        function(value, element) {
            if ('' === $(element).val()) {
                return false;
            }
            return true;
        },
        $.mage.__("Please select another Company, because selected one is not valid")
    );
});
