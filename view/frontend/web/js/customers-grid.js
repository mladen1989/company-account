define(
    [
        'jquery',
        'jquery/ui',
        'Magento_Ui/js/modal/alert',
        'Magento_Ui/js/modal/modal',
        'mage/url'
    ], function ($, ui, alert, modal, urlBuilder) {
        'use strict';

        $.widget('lambda.customersGrid', {
            //modal windows
            modalWindows: {
                companiesModal: null,
            },
            //css selectors
            selectors: {
                customerActionSelect: '#customers-actions',
                customerCheckbox: '[name="selected_customers[]"]',
                customerCheckboxSelected: '[name="selected_customers[]"]:checked',
                companiesDropdownWrapper: 'companies-dropdown-wrapper',
                companiesDropdown: '#companies-dropdown',
                selectAllCustomers: '#select-all-customers',
                unSelectAllCustomers: '#unselect-all-customers'
            },
            //options
            options: {
                failData: {
                    title: $.mage.__('Error'),
                    message: $.mage.__('An error occurred. Please contact support')
                },
                modalWindowOptions: {
                    type: 'popup',
                    responsive: true,
                    innerScroll: false,
                    title: 'Title'
                },
                defaultModalWindowConfiguration: {
                    type: 'popup',
                    responsive: true,
                    innerScroll: false,
                    title: 'Title',
                }
            },
            //translations
            i18n: {
                pleaseSelectAnyCustomerTitle: $.mage.__('No customer(s) selected'),
                pleaseSelectAnyCustomerMessage: $.mage.__('Please select any customer(s) before action run'),
                companiesDropdownDefaultText: $.mage.__('Please select company'),
                noAvailableCompaniesModal: {
                    message: $.mage.__('There no available for assignment Companies'),
                    cssClass: 'modal-without-title'
                }
            },
            /**
             * Create widget
             * @private
             */
            _create: function () {
                this.initGridActions();
            },
            /**
             * Init customers grid actions
             */
            initGridActions: function() {
                let self = this;
                $(this.selectors.customerActionSelect).on('change', function() {
                    if (self.checkAnyCustomerSelected() && ('' !== $(this).val())) {
                        self.getCompaniesListAndBuildModalindow($(this).val());
                        self.resetDropdown();
                    } else {
                        self.resetDropdown();
                        self.notifyAboutNotSelectedCustomers();
                    }
                });
                $(this.selectors.selectAllCustomers).on('click', function() {
                    $(self.selectors.customerCheckbox).prop('checked', 'checked');
                });
                $(this.selectors.unSelectAllCustomers).on('click', function() {
                    $(self.selectors.customerCheckbox).prop('checked', false);
                });
            },
            /**
             * Reset actions dropdown
             */
            resetDropdown: function () {
                $(this.selectors.customerActionSelect).prop('selectedIndex', null);
            },
            /**
             * Show modal window, regarding not selected any customer
             */
            notifyAboutNotSelectedCustomers: function() {
                let self = this;
                alert({
                    title: self.i18n.pleaseSelectAnyCustomerTitle,
                    content: self.i18n.pleaseSelectAnyCustomerMessage
                });
            },
            /**
             * Get list of possible companies
             * @param url
             */
            getCompaniesListAndBuildModalindow: function (url) {
                this._ajaxCall(null, 'get', url, 'buildModalWindowWithCompanies');//123123
            },
            /**
             * Check, is any customer from grid selected or no
             * @returns {boolean}
             */
            checkAnyCustomerSelected: function() {
                let self = this;
                return Boolean($(self.selectors.customerCheckboxSelected).length);
            },
            /**
             * Global method, for send ajax calls
             *
             * @param data
             * @param method
             * @param action
             * @param successMethodName
             * @param failMethodName
             * @param type
             *
             * @private
             */
            _ajaxCall: function (
                data,
                method = 'post',
                action,
                successMethodName = 'actionListAfterSuccess',
                failMethodName = 'actionsListAfterFail',
                type = 'json'
            ) {
                let self = this;
                $.ajax(
                    {
                        method: method,
                        url: action,
                        data: data,
                        dataType: type,
                        beforeSend: function () {
                            self.actionsListBeforeSendAjax();
                        },
                        success: function (data) {
                            self[successMethodName](data);
                        },
                        error: function (data) {
                            self[failMethodName](data);
                        }
                    }
                );
            },

            buildModalWindowWithCompanies: function(data) {
                let self = this;
                this.disableLoader();
                if ((data.companies) && Object.keys(data.companies).length) {
                    this.buildCompaniesDropdownAndShowModal(data.companies)
                } else {
                    alert(
                        {
                            title: '',
                            content: self.i18n.noAvailableCompaniesModal.message,
                            modalClass: self.i18n.noAvailableCompaniesModal.cssClass
                        }
                    );
                }
            },

            buildCompaniesDropdownAndShowModal: function(companies) {
                let self = this,
                    dropdown = document.createElement('select'),
                    option = document.createElement('option');
                    option.value = '';
                    option.textContent = this.i18n.companiesDropdownDefaultText;
                dropdown.setAttribute('id', 'companies-dropdown');
                dropdown.appendChild(option);


                Object.keys(companies).map(function(index) {
                    let company = companies[index];
                    option = document.createElement('option');
                    option.value = company.value;
                    option.textContent = company.name;
                    dropdown.appendChild(option);
                });
                let wrapper = document.getElementById(this.selectors.companiesDropdownWrapper);
                wrapper.appendChild(dropdown);
                self.modalWindows.companiesModal = modal({
                    type: 'popup',
                    responsive: true,
                    innerScroll: false,
                    title: $.mage.__('Please select company'),
                    closed: function () {
                        wrapper.removeChild(dropdown);
                    },
                    buttons: [{
                        text: $.mage.__('Assign'),
                        class: 'assign-button action-primary',
                        click: function() {
                            if (self.checkAnyCustomerSelected() && ('' !== $(self.selectors.companiesDropdown).val())) {
                                let data = self.prepareDataForAssignCustomersToCompany();
                                self._ajaxCall(
                                    data,
                                    'post',
                                    urlBuilder.build('company/customers/assign'),
                                    'successAssignCustomersToCompany',
                                    'failAssignCustomersToCompany'
                                );
                            } else {
                                alert({
                                    title: $.mage.__('Please select company'),
                                    content: $.mage.__('You haven`t selected a company')
                                });
                            }
                        }
                    }]
                }, wrapper);
                self.modalWindows.companiesModal.openModal();
            },

            successAssignCustomersToCompany: function(data) {
                this.disableLoader();
                this.modalWindows.companiesModal.closeModal();
                this.successMessage(data.message);
            },

            failAssignCustomersToCompany: function(data) {
                let self = this;
                this.disableLoader();
                this.modalWindows.companiesModal.closeModal();
                this.failMessage(data.message !== undefined ? data.message : self.options.failData.message);
            },
            /**
             * Prepare data for send
             * @returns {{customers: Array, company: (*|jQuery)}}
             */
            prepareDataForAssignCustomersToCompany: function () {
                let self = this,
                    selectedCustomers = [],
                    selectedCompany = $(self.selectors.companiesDropdown).val();
                $(this.selectors.customerCheckboxSelected).each( function(index, checkbox) {
                    selectedCustomers.push($(checkbox).val());
                });
                return {'customers': selectedCustomers, 'company': selectedCompany};
            },
            /**
             * General list of actions after SUCCESS ajax call
             *
             * @param data
             */
            actionListAfterSuccess: function (data) {
                this.disableLoader();
                this.successMessage(data.message);
            },
            /**
             * General list of actions, after FAIL ajax call
             *
             * @param data
             */
            actionsListAfterFail: function (data) {
                let self = this;
                this.disableLoader();
                this.failMessage(data.message !== undefined ? data.message : self.options.failData.message);
            },
            /**
             * General list of actions, that should be run before ajax call
             */
            actionsListBeforeSendAjax: function () {
                this.enableLoader();
            },
            /**
             * Enable General loader
             */
            enableLoader: function () {
                $('body').loader('show');
            },
            /**
             * Disable general loader
             */
            disableLoader: function () {
                $('body').loader('hide');
            },
            /**
             * Show custom FAIL alert message
             *
             * @param message
             */
            failMessage: function (message) {
                let self = this;
                alert(
                    {
                        title: self.options.failData.title,
                        content: message,
                        actions: {
                            always: function () {
                                //TODO: in the future, would be good to implement updating customers grid without page reload
                                location.reload();
                            }
                        }
                    }
                );
            },
            /**
             * Show custom SUCCESS alert message
             *
             * @param message
             */
            successMessage: function (message) {
                let self = this;
                alert(
                    {
                        title: self.options.successDataMessage,
                        content: message,
                        actions: {
                            always: function () {
                                //TODO: in the future, would be good to implement updating customers grid without page reload
                                location.reload();
                            }
                        }
                    }
                );
            },
        });

        return $.lambda.customersGrid;
    });
