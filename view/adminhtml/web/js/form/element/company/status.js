define(
    [
    'jquery',
    'underscore',
    'Magento_Ui/js/form/element/select',
    'Lambda_CompanyAccount/js/modal/confirm/company/status',
    'text!Lambda_CompanyAccount/template/modal/company/status/default.html',
    'mage/template'
    ], function ($, _, Select, confirm, modalDefaultTemplate, mageTemplate) {
        'use strict';

        return Select.extend(
            {
                confirm: {
                    templates: {
                        base: modalDefaultTemplate
                    },
                    handlers: {}
                },

                /**
                 * Init ui component
                 *
                 * @returns {Element}
                 */
                initialize: function () {
                    return this._super()
                    .initStateConfig();
                },

                /**
                 * Init state config
                 *
                 * @returns {Element}
                 */
                initStateConfig: function () {
                    this.initialValue = this.value();
                    return this;
                },

                /**
                 * Callback that fires when 'value' property is updated.
                 */
                onUpdate: function () {
                    this._super();

                    if (this.initialValue !== this.value()) {
                        this._confirmChanges();
                    }
                },

                /**
                 * Show confirmation popup
                 *
                 * @private
                 */
                _confirmChanges: function () {
                    var statusConfig = this.confirmation.status[this.value()];

                    confirm(this._getConfirmationConfig(this.value()))
                    .done(
                        function (data) {
                            var handler;

                            this.initialValue = this.value();
                            handler = this.confirm.handlers[statusConfig.handler];

                            if (_.isFunction(handler)) {
                                handler.call(this, data);
                            }
                        }.bind(this)
                    )
                    .fail(
                        function () {
                            this.reset();
                        }.bind(this)
                    );
                },

                /**
                 * Get confirmation config
                 *
                 * @param {String} status
                 *
                 * @returns {Object}
                 * @private
                 */
                _getConfirmationConfig: function (status) {
                    var statusConfig = this.confirmation.status[status],
                    contentTemplate = this.confirm.templates[statusConfig.template],
                    content = mageTemplate(contentTemplate, this.confirmation.status[status]),
                    confirmationConfig = _.extend(
                        {
                            content: content
                        },
                        this.confirmation.config,
                        this.confirmation.status[status]
                    );

                    return confirmationConfig;
                }
            }
        );
    }
);
