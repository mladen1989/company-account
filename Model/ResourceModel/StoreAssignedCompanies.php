<?php
/**
 * File StoreAssignedCompanies
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Model\ResourceModel;

use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class StoreAssignedCompanies
 */
class StoreAssignedCompanies extends AbstractDb
{
    /**
     * Initialize resource
     *
     * @return void
     */
    // @codingStandardsIgnoreStart
    public function _construct()
    {// @codingStandardsIgnoreEnd
        $this->_init('lambda_store_company', 'entity_id');
    }
}
