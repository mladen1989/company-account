<?php
/**
 * Copyright © Lambda Solutions. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Lambda\CompanyAccount\Model\ResourceModel;

use Magento\Eav\Model\Entity\AbstractEntity;
use Magento\Eav\Model\Entity\Context;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Validator\Exception as ValidatorException;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Lambda\CompanyAccount\Api\Data\CompanyInterface;

class Company extends AbstractEntity
{
    /** @var int */
    protected $_storeId = null;

    /** @var StoreManagerInterface */
    protected $_storeManager;

    /**
     * @param Context               $context
     * @param StoreManagerInterface $storeManager
     * @param array                 $data
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->setType(CompanyInterface::ENTITY_TYPE_CODE);
        $this->setConnection(CompanyInterface::ENTITY_TYPE_CODE . '_read', CompanyInterface::ENTITY_TYPE_CODE . '_write');
        $this->_storeManager = $storeManager;
    }

    /**
     * @param DataObject $company
     * @return $this|Company
     * @throws AlreadyExistsException
     * @throws ValidatorException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _beforeSave(\Magento\Framework\DataObject $company)
    {
        $company->setAttributeSetId($company->getAttributeSetId() ?: $this->getEntityType()->getDefaultAttributeSetId());
        $company->setEntityTypeId($company->getEntityTypeId() ?: $this->getEntityType()->getEntityTypeId());
        parent::_beforeSave($company);

        if (!$company->getName()) {
            throw new ValidatorException(__('The company name is missing. Enter and try again.'));
        }
        if (!$company->getId()) {
            $connection = $this->getConnection();
            $bind = ['name' => $company->getName()];

            $select = $connection->select()->from(
                $this->getEntityTable(),
                [$this->getEntityIdField()]
            )->where(
                'name = :name'
            );

            $bind['entity_id'] = (int)$company->getId();
            $select->where('entity_id != :entity_id');

            $result = $connection->fetchOne($select, $bind);

            if ($result) {
                throw new AlreadyExistsException(
                    __('Could not save company, because company with such name already existed.')
                );
            }
        }
        return $this;
    }

    public function getEntityType()
    {
        if (empty($this->_type)) {
            $this->setType(CompanyInterface::ENTITY_TYPE_CODE);
        }
        return parent::getEntityType();
    }

    protected function _getDefaultAttributes()
    {
        return [
            'attribute_set_id',
            'entity_type_id',
            'parent_company_id',
            'customer_group_id',
            'status',
            'name',
            'street',
            'city',
            'postcode',
            'country_id',
            'region_id',
            'region',
            'telephone',
            'created_at',
            'updated_at',
        ];
    }

    public function setStoreId($storeId)
    {
        $this->_storeId = $storeId;
        return $this;
    }

    public function getStoreId()
    {
        if ($this->_storeId === null) {
            return $this->_storeManager->getStore()->getId();
        }
        return $this->_storeId;
    }

    protected function _saveAttribute($object, $attribute, $value)
    {
        $table = $attribute->getBackend()->getTable();
        if (!isset($this->_attributeValuesToSave[$table])) {
            $this->_attributeValuesToSave[$table] = [];
        }

        $entityIdField = $attribute->getBackend()->getEntityIdField();
        $storeId = $object->getStoreId() ?: Store::DEFAULT_STORE_ID;
        $data = [
            $entityIdField => $object->getId(),
            'entity_type_id' => $object->getEntityTypeId(),
            'attribute_id' => $attribute->getId(),
            'value' => $this->_prepareValueForSave($value, $attribute),
            'store_id' => $storeId,
        ];

        if (!$this->getEntityTable() || $this->getEntityTable() == \Magento\Eav\Model\Entity::DEFAULT_ENTITY_TABLE) {
            $data['entity_type_id'] = $object->getEntityTypeId();
        }

        if ($attribute->isScopeStore()) {
            /**
             * Update attribute value for store
             */
            $this->_attributeValuesToSave[$table][] = $data;
        } elseif ($attribute->isScopeWebsite() && $storeId != Store::DEFAULT_STORE_ID) {
            /**
             * Update attribute value for website
             */
            $storeIds = $this->_storeManager->getStore($storeId)->getWebsite()->getStoreIds(true);
            foreach ($storeIds as $storeId) {
                $data['store_id'] = (int) $storeId;
                $this->_attributeValuesToSave[$table][] = $data;
            }
        } else {
            /**
             * Update global attribute value
             */
            $data['store_id'] = Store::DEFAULT_STORE_ID;
            $this->_attributeValuesToSave[$table][] = $data;
        }

        return $this;
    }

    /**
     * Load company by name
     *
     * @param \Lambda\CompanyAccount\Model\Company $company
     * @param $name
     * @return $this
     */
    public function loadByName(\Lambda\CompanyAccount\Model\Company $company, $name)
    {
        $connection = $this->getConnection();
        $bind = ['name' => $name];
        $select = $connection->select()->from(
            $this->getEntityTable(),
            [$this->getEntityIdField()]
        )->where(
            'name = :name'
        );

        $companyId = $connection->fetchOne($select, $bind);
        if ($companyId) {
            $this->load($company, $companyId);
        } else {
            $company->setData([]);
        }
        return $this;
    }

    /**
     * Load company by customer group id
     *
     * @param \Lambda\CompanyAccount\Model\Company $company
     * @param $groupId
     * @return $this
     */
    public function loadByCustomerGroupId(\Lambda\CompanyAccount\Model\Company $company, $groupId)
    {
        $connection = $this->getConnection();
        $bind = ['customer_group_id' => $groupId];
        $select = $connection->select()->from(
            $this->getEntityTable(),
            [$this->getEntityIdField()]
        )->where(
            'customer_group_id = :customer_group_id'
        );

        $companyId = $connection->fetchOne($select, $bind);
        if ($companyId) {
            $this->load($company, $companyId);
        } else {
            $company->setData([]);
        }
        return $this;
    }
}
