<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Lambda\CompanyAccount\Model\ResourceModel\CompanyBillingAddress;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Lambda\CompanyAccount\Model\CompanyBillingAddress::class,
            \Lambda\CompanyAccount\Model\ResourceModel\CompanyBillingAddress::class
        );
    }
}
