<?php
/**
 * File Collection
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Model\ResourceModel\StoreAssignedCompanies;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 */
class Collection extends AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    // @codingStandardsIgnoreStart
    public function _construct()
    {// @codingStandardsIgnoreEnd
        $this->_init(
            \Lambda\CompanyAccount\Model\StoreAssignedCompanies::class,
            \Lambda\CompanyAccount\Model\ResourceModel\StoreAssignedCompanies::class
        );
    }
}
