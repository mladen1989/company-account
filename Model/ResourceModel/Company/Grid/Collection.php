<?php
/**
 * File Collection
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Model\ResourceModel\Company\Grid;

use Lambda\CompanyAccount\Model\ResourceModel\Company\Collection as CompanyCollection;
use Lambda\CompanyAccount\Model\ResourceModel\Company as CompanyResourceModel;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult
{
    /**
     * Attributes mapper
     *
     * @var array
     */
    protected $mapper = [
        'company_id' => 'main_table.entity_id',
        'name' => 'main_table.name',
        'status' => 'main_table.status',
    ];

    /**
     * Collection constructor.
     *
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface    $entityFactory Entity collection
     * @param \Psr\Log\LoggerInterface                                     $logger        Logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy Fetch strategy
     * @param \Magento\Framework\Event\ManagerInterface                    $eventManager  Event manager
     * @param string                                                       $mainTable     Main table
     * @param string                                                       $resourceModel Resource model
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        $mainTable = 'lambda_company',
        $resourceModel = \Lambda\CompanyAccount\Model\ResourceModel\Company::class
    ) {
        parent::__construct(
            $entityFactory,
            $logger,
            $fetchStrategy,
            $eventManager,
            $mainTable,
            $resourceModel
        );
    }
}
