<?php
/**
 * File Delete
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Model\Company;

/**
 * Class Delete
 */
class Delete
{
    /**
     * No company id
     *
     * @var integer
     */
    // @codingStandardsIgnoreStart
    private $noCompanyId = 0;
    // @codingStandardsIgnoreEnd

    /**
     * Company resource model
     *
     * @var \Lambda\CompanyAccount\Model\ResourceModel\Company
     */
    // @codingStandardsIgnoreStart
    private $companyResource;
    // @codingStandardsIgnoreEnd

    /** @var \Lambda\CompanyAccount\Model\CompanyFactory  */
    private $companyModel;

    /**
     * Delete constructor.
     *
     * @param \Lambda\CompanyAccount\Model\ResourceModel\Company $companyResource Company resource model
     */
    public function __construct(
        \Lambda\CompanyAccount\Model\ResourceModel\Company $companyResource,
        \Lambda\CompanyAccount\Model\CompanyFactory $companyModel
    ) {
        $this->companyResource = $companyResource;
        $this->companyModel = $companyModel;
    }

    /**
     * Detaches customer entities from company entity and deletes it.
     *
     * @param \Lambda\CompanyAccount\Api\Data\CompanyInterface $company Company
     *
     * @return void
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\State\InputMismatchException
     * @throws \Exception
     */
    public function delete(\Lambda\CompanyAccount\Api\Data\CompanyInterface $company)
    {
        $companyModel = $this->companyModel->create();
        $this->companyResource->load($companyModel, $company->getId());
        $this->companyResource->delete($companyModel);
    }
}
