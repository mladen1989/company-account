<?php
/**
 * File Save
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Model\Company;

use Lambda\CompanyAccount\Api\Data\CompanyInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Lambda\CompanyAccount\Model\ResourceModel\Company;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Lambda\CompanyAccount\Model\ResourceModel\Company\CollectionFactory as CompanyCollectionFactory;

/**
 * Class Save
 */
class Save
{
    /**
     * Company resource model
     *
     * @var Company
     */
    // @codingStandardsIgnoreStart
    private $companyResource;
    // @codingStandardsIgnoreEnd

    /**
     * Company factory
     *
     * @var \Lambda\CompanyAccount\Model\CompanyFactory
     */
    // @codingStandardsIgnoreStart
    private $companyFactory;
    // @codingStandardsIgnoreEnd

    /** @var ExtensibleDataObjectConverter  */
    private $extensibleDataObjectConverter;

    /**
     * @param Company $companyResource Company resource model
     * @param CompanyCollectionFactory $companyCollectionFactory Company Collection Factory
     * @param \Lambda\CompanyAccount\Model\CompanyFactory $companyFactory Company factory
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter Extensible Data Object Converter
     */
    public function __construct(
        Company $companyResource,
        \Lambda\CompanyAccount\Model\CompanyFactory $companyFactory,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->companyResource = $companyResource;
        $this->companyFactory = $companyFactory;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * Checks if provided data for a company is correct, saves the company entity
     *
     * @param CompanyInterface $company
     * @return \Lambda\CompanyAccount\Model\Company
     * @throws AlreadyExistsException
     */
    public function save(CompanyInterface $company)
    {
        $companyModel = $this->companyFactory->create();
        if ($company->getId()) {
            $this->companyResource->load($companyModel, $company->getId());
            $this->validateIsNameChanged($companyModel->getId(), $company->getName());
        }
        $origBillingAddresses = $company->getCompanyBillingAddress();
        $company->setCompanyBillingAddress(null);
        $companyData = $this->extensibleDataObjectConverter->toNestedArray($company, [], CompanyInterface::class);
        $company->setCompanyBillingAddress($origBillingAddresses);
        if (!$company->getParentCompanyId()) {
            $companyData['parent_company_id'] = null;
        }
        $companyModel->addData($companyData);

        $this->companyResource->save($companyModel);

        if ($origBillingAddresses && !empty($origBillingAddresses->getData())) {
            $origBillingAddresses->saveCompanyBillingAddress($companyModel->getId(), $origBillingAddresses->getData());
        }
        return $companyModel;
    }

    /**
     * @param $companyId
     * @param $name
     * @throws AlreadyExistsException
     */
    private function validateIsNameChanged($companyId, $name)
    {
        $companyModel = $this->companyFactory->create();
        $this->companyResource->loadByName($companyModel, $name);
        if ($companyModel->getId() && $companyModel->getId() != $companyId) {
            throw new AlreadyExistsException(
                __('Could not save company, because company with such name already existed.')
            );
        }
    }
}
