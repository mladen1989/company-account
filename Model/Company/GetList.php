<?php
/**
 * File GetList
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Model\Company;

use Lambda\CompanyAccount\Model\ResourceModel\Company\CollectionFactory;
use Lambda\CompanyAccount\Api\Data\CompanySearchResultsInterfaceFactory;
use \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Lambda\CompanyAccount\Api\Data\CompanyInterface;

/**
 * Class GetList
 */
class GetList
{
    /**
     * Company collection factory
     *
     * @var \Lambda\CompanyAccount\Model\ResourceModel\Company\CollectionFactory
     */
    // @codingStandardsIgnoreStart
    private $companyCollectionFactory;
    // @codingStandardsIgnoreEnd

    /**
     * Search result factory
     *
     * @var \Lambda\CompanyAccount\Api\Data\CompanySearchResultsInterfaceFactory
     */
    // @codingStandardsIgnoreStart
    private $searchResultsFactory;
    // @codingStandardsIgnoreEnd

    /**
     * Extension attributes join processor
     *
     * @var \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface
     */
    // @codingStandardsIgnoreStart
    private $extensionAttributesJoinProcessor;
    // @codingStandardsIgnoreEnd

    /**
     * Search criteria collection processor
     *
     * @var \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface
     */
    // @codingStandardsIgnoreStart
    private $collectionProcessor;
    // @codingStandardsIgnoreEnd

    /**
     * GetList constructor.
     *
     * @param CollectionFactory                    $companyCollectionFactory         Company collection factory
     * @param CompanySearchResultsInterfaceFactory $searchResultsFactory             Search results factory
     * @param JoinProcessorInterface               $extensionAttributesJoinProcessor Ext. attr. join processor
     * @param CollectionProcessorInterface         $collectionProcessor              Collection processor
     */
    public function __construct(
        CollectionFactory $companyCollectionFactory,
        CompanySearchResultsInterfaceFactory $searchResultsFactory,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->companyCollectionFactory = $companyCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * Gets a list of companies.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria Search criteria
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanySearchResultsInterface
     *
     * @throws \InvalidArgumentException
     * @throws \LogicException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        /**
         * Company resource module collection
         *
         * @var \Lambda\CompanyAccount\Model\ResourceModel\Company\Collection $collection
         */
        $collection = $this->companyCollectionFactory->create();
        $this->extensionAttributesJoinProcessor->process($collection, CompanyInterface::class);

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults->setTotalCount($collection->getSize());
        $companies = [];
        /** @var \Lambda\CompanyAccount\Model\Company $companyModel */
        foreach ($collection as $companyModel) {
            $companies[] = $companyModel->getDataModel();
        }
        $searchResults->setItems($companies);
        return $searchResults;
    }
}
