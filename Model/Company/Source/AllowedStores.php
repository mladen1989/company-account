<?php
/**
 * File AllowedStores
 *
 * PHP version 7
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Model\Company\Source;

use Magento\Store\Model\StoreManagerInterface;

/**
 * Class AllowedStores
 *
 * @category   Lambda
 * @package    Lambda\CompanyAccount\Model\Company\Source
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */
class AllowedStores implements \Magento\Framework\Data\OptionSourceInterface
{
    /** @var StoreManagerInterface */
    private $_storeManager;

    public function __construct(
        StoreManagerInterface $storeManager
    ) {
        $this->_storeManager = $storeManager;
    }

    /**
    * Get options
    *
    * @return array
    */
    public function toOptionArray()
    {
        $options = [];
        $storeManagerDataList = $this->_storeManager->getStores();
        foreach ($storeManagerDataList as $key => $value) {
            $options[] = ['label' => $value['name'], 'value' => $key];
        }
        return $options;
    }
}
