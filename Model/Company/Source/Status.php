<?php
/**
 * File Status
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Model\Company\Source;

use Lambda\CompanyAccount\Api\Data\CompanyInterface;

/**
 * Class Status
 */
class Status implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            ['label' => __('Active'), 'value' => CompanyInterface::STATUS_ACTIVE],
            ['label' => __('Disabled'), 'value' => CompanyInterface::STATUS_DISABLED]
        ];
        return $options;
    }
}
