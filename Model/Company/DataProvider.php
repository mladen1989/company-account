<?php
/**
 * File DataProvider
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Model\Company;

use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Form\Fieldset;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Lambda\CompanyAccount\Api\Data\CompanyInterface;
use Lambda\CompanyAccount\Model\Company;
use Lambda\CompanyAccount\Model\ResourceModel\Company\CollectionFactory as CompanyCollectionFactory;
use Magento\Ui\DataProvider\Modifier\PoolInterface;
use Lambda\CompanyAccount\Api\Data\CompanyBillingAddressInterface;
use Lambda\CompanyAccount\Model\ResourceModel\StoreAssignedCompanies\CollectionFactory as StoreAssignedCompaniesCollection;

/**
 * Class DataProvider
 */
class DataProvider extends AbstractDataProvider
{
    /**
     * General scope name
     *
     * @var string
     */
    const DATA_SCOPE_GENERAL = 'general';

    /**
     * Company billing address scope name
     *
     * @var string
     */
    const DATA_COMPANY_BILLING_ADDRESS = 'company_billing';

    /**
     * Custom company attributes scope name
     *
     * @var string
     */
    const DATA_SCOPE_CUSTOM_ATTRIBUTES = 'company_custom_attributes';

    /**
     * Sub companies scope name
     *
     * @var string
     */
    const DATA_SCOPE_COMPANIES_RELATION = 'companies_relation';

    /**
     * Loaded data
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    private $loadedData;
    // @codingStandardsIgnoreEnd

    /** @var CompanyBillingAddressInterface */
    private $billingAddress;

    /** @var StoreAssignedCompaniesCollection */
    private $storeCompaniesCollection;

    /**
     * Extension attributes join processor
     *
     * @var JoinProcessorInterface
     */
    // @codingStandardsIgnoreStart
    private $extensionAttributesJoinProcessor;
    // @codingStandardsIgnoreEnd

    /**
     * @var SearchCriteriaBuilderFactory
     */
    protected $_searchCriteriaBuilderFactory;

    /**
     * @var AttributeRepositoryInterface
     */
    protected $_attributeRepository;

    /**
     * DataProvider constructor.
     *
     * @param string                            $name                               Name
     * @param string                            $primaryFieldName                   Primary field name
     * @param string                            $requestFieldName                   Request field name
     * @param CompanyCollectionFactory          $companyCollectionFactory           Company collection
     * @param JoinProcessorInterface            $extensionAttributesJoinProcessor   Ext. attr. join processor
     * @param PoolInterface                     $poolInterface                      Pool Interface
     * @param CompanyBillingAddressInterface    $billingAddress                     Company Billing Address Interface
     * @param StoreAssignedCompaniesCollection  $storeCompaniesCollection           Store Companies Collection
     * @param SearchCriteriaBuilderFactory      $searchCriteriaBuilderFactory       Search Criteria Builder Factory
     * @param AttributeRepositoryInterface      $attributeRepository                Attribute Repository Interface
     * @param array                             $meta                               Meta
     * @param array                             $data                               Data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CompanyCollectionFactory $companyCollectionFactory,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        PoolInterface $poolInterface,
        CompanyBillingAddressInterface $billingAddress,
        StoreAssignedCompaniesCollection $storeCompaniesCollection,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
        AttributeRepositoryInterface $attributeRepository,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $companyCollectionFactory->create();
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->pool = $poolInterface;
        $this->billingAddress = $billingAddress;
        $this->storeCompaniesCollection = $storeCompaniesCollection;
        $this->_searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
        $this->_attributeRepository = $attributeRepository;
    }

    /**
     * Get company general data.
     *
     * @param Company $company Company
     *
     * @return array
     */
    public function getGeneralData(Company $company): array
    {
        return [
            CompanyInterface::STORE => $this->getAssignedCompaniesList($company),
            CompanyInterface::NAME => $company->getName(),
            CompanyInterface::STATUS => $company->getStatus(),
            CompanyInterface::STREET => $company->getStreet(),
            CompanyInterface::CITY => $company->getCity(),
            CompanyInterface::POSTCODE => $company->getPostcode(),
            CompanyInterface::COUNTRY_ID => $company->getCountryId(),
            CompanyInterface::REGION_ID => $company->getRegionId(),
            CompanyInterface::REGION => $company->getRegion(),
            CompanyInterface::TELEPHONE => $company->getTelephone()
        ];
    }

    /**
     * @param Company $company
     * @return array
     */
    protected function getAssignedCompaniesList(Company $company)
    {
        $collection = $this->storeCompaniesCollection->create();
        return $collection
            ->addFieldToFilter(\Lambda\CompanyAccount\Model\StoreAssignedCompanies::COMPANY_ID, $company->getId())
            ->addFieldToSelect(\Lambda\CompanyAccount\Model\StoreAssignedCompanies::STORE_ID)
            ->getColumnValues(\Lambda\CompanyAccount\Model\StoreAssignedCompanies::STORE_ID);
    }

    /**
     * Get company billing address.
     *
     * @param Company $company Company
     *
     * @return array
     */
    public function getCompanyBillingAddress(Company $company): array
    {
        $result = [];
        $billingAddressCollection = $this->billingAddress->getCompanyBillingAddressByCompanyId($company->getId());
        if ($billingId = $billingAddressCollection->getId()) {
            $billingAddressModel = $billingAddressCollection->load($billingId);
            return [
                CompanyBillingAddressInterface::BILLING_NAME => $billingAddressModel->getCompanyBillingName(),
                CompanyBillingAddressInterface::BILLING_STREET => $billingAddressModel->getCompanyBillingStreet(),
                CompanyBillingAddressInterface::BILLING_CITY => $billingAddressModel->getCompanyBillingCity(),
                CompanyBillingAddressInterface::BILLING_COUNTRY_ID => $billingAddressModel->getCompanyBillingCountryId(),
                CompanyBillingAddressInterface::BILLING_REGION_ID => $billingAddressModel->getCompanyBillingRegionId(),
                CompanyBillingAddressInterface::BILLING_REGION => $billingAddressModel->getCompanyBillingRegion(),
                CompanyBillingAddressInterface::BILLING_POSTCODE => $billingAddressModel->getCompanyBillingPostcode(),
                CompanyBillingAddressInterface::BILLING_TELEPHONE => $billingAddressModel->getCompanyBillingTelephone(),
                CompanyBillingAddressInterface::BILLING_EMAIL => $billingAddressModel->getCompanyBillingEmail(),
                CompanyBillingAddressInterface::BILLING_EMAIL_CC => $billingAddressModel->getCompanyBillingEmailCc(),
            ];
        }
        return $result;

    }

    /**
     * Get company sub_companies data
     *
     * @param Company $company Company
     *
     * @return array
     */
    public function getParentCompanyData($company): array
    {
        return [
            CompanyInterface::PARENT_COMPANY_ID => $company->getParentCompanyId()
        ];
    }

    /**
     * Get data
     *
     * @return array
     *
     * @throws \LogicException
     */
    public function getData(): array
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $this->extensionAttributesJoinProcessor->process($this->collection, CompanyInterface::class);
        $items = $this->collection->getItems();
        $this->loadedData = [];
        /**
         * Company
         *
         * @var Company $company
         */
        foreach ($items as $item) {
            $companyId = $item->getEntityId();
            $company = $item->load($companyId);

            $this->loadedData[$company->getId()] = $this->getCompanyResultData($company);
        }
        return $this->loadedData;
    }

    /**
     * Get company result data.
     *
     * @param Company $company Company
     *
     * @return array
     */
    public function getCompanyResultData(Company $company): array
    {
        $result = [
            self::DATA_SCOPE_GENERAL => $this->getGeneralData($company),
            self::DATA_SCOPE_COMPANIES_RELATION => $this->getParentCompanyData($company),
            self::DATA_COMPANY_BILLING_ADDRESS => $this->getCompanyBillingAddress($company),
            self::DATA_SCOPE_CUSTOM_ATTRIBUTES => $this->getCompanyAttributes($company)
        ];
        $result['id'] = $company->getId();
        return $result;
    }

    /**
     * Get attributes by company
     *
     * @param Company $company
     *
     * @return array
     */
    private function getCompanyAttributes(Company $company): array
    {
        $attributes = [];
        $companyAttributes = $company->getDataModel()->getCustomAttributes();
        foreach ($companyAttributes as $attribute) {
           $attributes[$attribute->getAttributeCode()] = $attribute->getValue();
        }
        return $attributes;
    }

    /**
     * @inheritdoc
     */
    public function getMeta()
    {
        $meta = parent::getMeta();

        if($this->getCompanyCustomAttributes()->getTotalCount()) {

            $meta['company_custom_attributes'] = [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'label' => __('Custom Company Attributes'),
                            'dataScope' => self::DATA_SCOPE_CUSTOM_ATTRIBUTES,
                            'sortOrder' => 40,
                            'collapsible' => true,
                            'componentType' => Fieldset::NAME
                        ]
                    ]
                ],
                'children' => $this->prepareCompanyCustomFieldsByAttributes()
            ];
        }

        return $meta;
    }

    /**
     * Prepare custom fileds by attribute
     *
     * @return array
     */
    private function prepareCompanyCustomFieldsByAttributes() {

        $attributes = [];

        foreach($this->getCompanyCustomAttributes()->getItems() as $companyAttribute)
        {
            $attributes[$companyAttribute->getAttributeCode()] = $this->getAttributeField($companyAttribute);
        }

        return $attributes;
    }

    /**
     * Get cmpany custom attributes
     */
    private function getCompanyCustomAttributes() {

        $searchCriteria = $this->_searchCriteriaBuilderFactory->create()
            ->addFilter('is_custom_created', 1, 'eq')
            ->create();

        return $this->_attributeRepository->getList('company', $searchCriteria);
    }

    /**
     * Get attribute field based on frontend type
     * @throws LocalizedException
     */
    private function getAttributeField($attribute) {

        switch($attribute->getFrontendInput())
        {
            case 'text':
                $attributeField = $this->getInputField($attribute);
                break;
            case 'textarea':
                $attributeField = $this->getTextAreaField($attribute);
                break;
            case 'date':
                $attributeField = $this->getDateField($attribute);
                break;
            case 'datetime':
                $attributeField = $this->getDateTimeField($attribute);
                break;
            case 'select':
                $attributeField = $this->getSelectField($attribute);
                break;
            case 'multiselect':
                $attributeField = $this->getMultipleSelectField($attribute);
                break;
            case 'boolean':
                $attributeField = $this->getCheckboxField($attribute);
                break;
            default:
                throw new LocalizedException(
                    new Phrase(__('Unknown custom customer attribute type'))
                );
        }
        return $attributeField;
    }

    /**
     * Input field for attribute
     */
    private function getInputField($attribute) {

        $inputField = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'formElement' => 'input',
                        'componentType' => Field::NAME,
                        'default' => $attribute->getDefaultValue(),
                        'visible' => 1,
                        'label' => __($attribute->getFrontendLabel()),
                        'validation' => [
                            'required-entry' => (bool) $attribute->getIsRequired()
                        ]
                    ]
                ]
            ]
        ];

        if($attribute->getNote()){
            $inputField['arguments']['data']['config']['tooltip'] = ['description' => $attribute->getNote()];
        }

        return $inputField;
    }

    /**
     * Input textarea field for attribute
     */
    private function getTextAreaField($attribute) {

        $inputTextareaField = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'formElement' => 'textarea',
                        'componentType' => Field::NAME,
                        'default' => $attribute->getDefaultValue(),
                        'visible' => 1,
                        'label' => __($attribute->getFrontendLabel()),
                        'validation' => [
                            'required-entry' => (bool) $attribute->getIsRequired()
                        ]
                    ]
                ]
            ]
        ];

        if($attribute->getNote()){
            $inputTextareaField['arguments']['data']['config']['tooltip'] = ['description' => $attribute->getNote()];
        }

        return $inputTextareaField;
    }

    /**
     * Date field for attribute
     */
    private function getDateField($attribute) {

        $dateField = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'formElement' => 'date',
                        'componentType' => Field::NAME,
                        'default' => $attribute->getDefaultValue(),
                        'visible' => 1,
                        'label' => __($attribute->getFrontendLabel()),
                        'validation' => [
                            'required-entry' => (bool) $attribute->getIsRequired()
                        ]
                    ]
                ]
            ]
        ];

        if($attribute->getNote()){
            $dateField['arguments']['data']['config']['tooltip'] = ['description' => $attribute->getNote()];
        }

        return $dateField;
    }

    /**
     * Datetime field for attribute
     */
    private function getDateTimeField($attribute) {

        $datetimeField = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'formElement' => 'input',
                        'componentType' => Field::NAME,
                        'default' => $attribute->getDefaultValue(),
                        'visible' => 1,
                        'label' => __($attribute->getFrontendLabel()),
                        'validation' => [
                            'required-entry' => (bool) $attribute->getIsRequired()
                        ]
                    ]
                ]
            ]
        ];

        if($attribute->getNote()){
            $datetimeField['arguments']['data']['config']['tooltip'] = ['description' => $attribute->getNote()];
        }

        return $datetimeField;
    }

    /**
     * Select field for attribute
     */
    private function getSelectField($attribute) {

        $selectField = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'formElement' => 'select',
                        'componentType' => Field::NAME,
                        'default' => $attribute->getDefaultValue(),
                        'visible' => 1,
                        'label' => __($attribute->getFrontendLabel()),
                        'validation' => [
                            'required-entry' => (bool) $attribute->getIsRequired()
                        ],
                        'options' => $attribute->getSource()->getAllOptions()
                    ]
                ]
            ]
        ];

        if($attribute->getNote()){
            $selectField['arguments']['data']['config']['tooltip'] = ['description' => $attribute->getNote()];
        }

        return $selectField;
    }

    /**
     * Multiselect field for attribute
     */
    private function getMultipleSelectField($attribute) {

        $multiselectField = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'formElement' => 'multiselect',
                        'componentType' => Field::NAME,
                        'default' => $attribute->getDefaultValue(),
                        'visible' => 1,
                        'label' => __($attribute->getFrontendLabel()),
                        'validation' => [
                            'required-entry' => (bool) $attribute->getIsRequired()
                        ],
                        'options' => $attribute->getSource()->getAllOptions()
                    ]
                ]
            ]
        ];

        if($attribute->getNote()){
            $multiselectField['arguments']['data']['config']['tooltip'] = ['description' => $attribute->getNote()];
        }

        return $multiselectField;
    }

    /**
     * Checkbox field for attribute
     */
    private function getCheckboxField($attribute) {

        $checkboxField = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'formElement' => 'checkbox',
                        'dataType' => 'boolean',
                        'componentType' => Field::NAME,
                        'default' => $attribute->getDefaultValue(),
                        'visible' => 1,
                        'label' => __($attribute->getFrontendLabel()),
                        'validation' => [
                            'required-entry' => (bool) $attribute->getIsRequired()
                        ],
                        'prefer' => 'toggle',
                        'valueMap' => [
                            'true' => '1',
                            'false' => '0'
                        ]
                    ]
                ]
            ]
        ];

        if($attribute->getNote()){
            $checkboxField['arguments']['data']['config']['tooltip'] = ['description' => $attribute->getNote()];
        }

        return $checkboxField;
    }
}
