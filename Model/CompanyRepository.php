<?php
/**
 * File CompanyRepository
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Model;

use Lambda\CompanyAccount\Api\CompanyRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Lambda\CompanyAccount\Model\Company\{Delete, GetList, Save};
use \Lambda\CompanyAccount\Api\Data\CompanyInterface;

/**
 * Class CompanyRepository
 */
class CompanyRepository implements CompanyRepositoryInterface
{
    /**
     * Instances
     *
     * @var CompanyInterface[]
     */
    // @codingStandardsIgnoreStart
    private $instances = [];
    // @codingStandardsIgnoreEnd

    /**
     * Company factory
     *
     * @var \Lambda\CompanyAccount\Model\CompanyFactory
     */
    // @codingStandardsIgnoreStart
    private $companyFactory;
    // @codingStandardsIgnoreEnd

    /**
     * Company deleter
     *
     * @var \Lambda\CompanyAccount\Model\Company\Delete
     */
    // @codingStandardsIgnoreStart
    private $companyDeleter;
    // @codingStandardsIgnoreEnd

    /**
     * Company List getter
     *
     * @var \Lambda\CompanyAccount\Model\Company\GetList
     */
    // @codingStandardsIgnoreStart
    private $companyListGetter;
    // @codingStandardsIgnoreEnd

    /**
     * Company saver
     *
     * @var \Lambda\CompanyAccount\Model\Company\Save
     */
    // @codingStandardsIgnoreStart
    private $companySaver;
    // @codingStandardsIgnoreEnd
    private ResourceModel\Company $companyResource;

    /**
     * CompanyRepository constructor.
     *
     * @param \Lambda\CompanyAccount\Model\CompanyFactory $companyFactory    Company saver
     * @param \Lambda\CompanyAccount\Model\ResourceModel\Company $companyResource    Company Resource
     * @param Delete                                                  $companyDeleter    Company deleter
     * @param GetList                                                 $companyListGetter Company list getter
     * @param Save                                                    $companySaver      Company saver
     */
    public function __construct(
        \Lambda\CompanyAccount\Model\CompanyFactory $companyFactory,
        \Lambda\CompanyAccount\Model\ResourceModel\Company $companyResource,
        Delete $companyDeleter,
        GetList $companyListGetter,
        Save $companySaver
    ) {
        $this->companyFactory = $companyFactory;
        $this->companyDeleter = $companyDeleter;
        $this->companyListGetter = $companyListGetter;
        $this->companySaver = $companySaver;
        $this->companyResource = $companyResource;
    }

    /**
     * Create or update a company account.
     *
     * @param \Lambda\CompanyAccount\Api\Data\CompanyInterface $company Company
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     *
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\InputException
     */
    public function save(CompanyInterface $company)
    {
        unset($this->instances[$company->getId()]);
        $savedCompany = $this->companySaver->save($company);
        return $this->get($savedCompany->getId());

    }

    /**
     * Returns company details.
     *
     * @param integer $companyId Company id
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($companyId)
    {
        if (!isset($this->instances[$companyId])) {
            /**
             * Company
             *
             * @var Company $company
             */
            $company = $this->companyFactory->create();
            $company->load($companyId);
            if (!$company->getId()) {
                throw NoSuchEntityException::singleField('id', $companyId);
            }
            $this->instances[$companyId] = $company;
        }
        return $this->instances[$companyId]->getDataModel();
    }

    /**
     * Returns company details.
     *
     * @param string $companyName Company name
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function getByName($companyName)
    {
        /**
         * Company
         *
         * @var Company $company
         */
        $company = $this->companyFactory->create();
        $this->companyResource->loadByName($company, $companyName);
        return $company->getDataModel();
    }

    /**
     * Returns company details.
     *
     * @param integer $groupId Customer Group Id
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function getByGroupId($groupId)
    {
        /**
         * Company
         *
         * @var Company $company
         */
        $company = $this->companyFactory->create();
        $this->companyResource->loadByCustomerGroupId($company, $groupId);
        return $company->getDataModel();
    }

    /**
     * Removes company entity and all the related links from the system.
     *
     * @param \Lambda\CompanyAccount\Api\Data\CompanyInterface $company Company
     *
     * @return boolean
     *
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(CompanyInterface $company)
    {
        $companyId = $company->getId();
        try {
            unset($this->instances[$companyId]);
            $this->companyDeleter->delete($company);
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(
                __(
                    'Cannot delete company with id %1',
                    $companyId
                ),
                $e
            );
        }
        return true;
    }

    /**
     * Delete a company. Customers belonging to a company are not deleted with this request.
     *
     * @param int $companyId Comapny id
     *
     * @return boolean
     */
    public function deleteById($companyId)
    {
        $company = $this->get($companyId);
        return $this->delete($company);
    }

    /**
     * Returns the list of companies.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria Search criteria
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanySearchResultsInterface
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        return $this->companyListGetter->getList($searchCriteria);
    }
}
