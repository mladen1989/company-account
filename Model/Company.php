<?php
/**
 * Copyright © Lambda Solutions. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Lambda\CompanyAccount\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\DataObject\IdentityInterface;
use \Lambda\CompanyAccount\Api\Data\CompanyInterface;
use Magento\Framework\Model\AbstractModel;
use Lambda\CompanyAccount\Model\ResourceModel\CompanyBillingAddress as CompanyBillingAddressResourceModel;
use Lambda\CompanyAccount\Api\Data\CompanyBillingAddressInterfaceFactory;
use Lambda\CompanyAccount\Model\Data\CompanyFactory as CompanyDataFactory;

/**
 * Class Company
 */
class Company extends AbstractModel implements IdentityInterface
{
    const NAME = 'name';

    const CACHE_TAG = 'lambda_company';

    /** @var string */
    protected $_cacheTag = 'lambda_company';

    /**
     * Model event name prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'lambda_company';

    /**
     * Parameter name in event
     *
     * In observe method you can use $observer->getEvent()->getObject() in this case
     *
     * @var string
     */
    protected $_eventObject = 'company';

    /**
     * List of required fields, that Company Admin should fill, for create a new company
     *
     * @var array
     */
    const LIST_OF_REQUIRED_FIELDS = [
        self::NAME
    ];

    /** @var DataObjectHelper  */
    private $dataObjectHelper;

    /** @var CompanyDataFactory  */
    private $companyDataFactory;

    /** @var CompanyBillingAddressInterfaceFactory  */
    private $companyBillingAddress;

    /** @var CompanyBillingAddressResourceModel  */
    private $companyBillingAddressResourceModel;


    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        DataObjectHelper $dataObjectHelper,
        CompanyDataFactory $companyDataFactory,
        CompanyBillingAddressInterfaceFactory $companyBillingAddress,
        CompanyBillingAddressResourceModel $companyBillingAddressResourceModel,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->dataObjectHelper = $dataObjectHelper;
        $this->companyDataFactory = $companyDataFactory;
        $this->companyBillingAddress = $companyBillingAddress;
        $this->companyBillingAddressResourceModel = $companyBillingAddressResourceModel;
    }
    /**
     * Initialize resource model
     *
     * @return void
     */
    // @codingStandardsIgnoreStart
    public function _construct()
    {// @codingStandardsIgnoreEnd
        $this->_init(\Lambda\CompanyAccount\Model\ResourceModel\Company::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get company billing address
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyBillingAddressInterface
     */
    public function getCompanyBillingAddress()
    {
        $companyBillingAddress = $this->companyBillingAddress->create();
        if ($this->getId()) {
            $this->companyBillingAddressResourceModel->load($companyBillingAddress, $this->getId(), 'billing_company_id');
        }
        return $companyBillingAddress;
    }

    /**
     * @return Data\Company
     */
    public function getDataModel()
    {
        $companyCustomAttributesData = $this->getData();
        $companyCustomAttributesDataObject = $this->companyDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $companyCustomAttributesDataObject,
            $companyCustomAttributesData,
            CompanyInterface::class
        );
        $companyCustomAttributesDataObject->setId($this->getId());
        $companyCustomAttributesDataObject->setCompanyBillingAddress($this->getCompanyBillingAddress());
        return $companyCustomAttributesDataObject;
    }
}
