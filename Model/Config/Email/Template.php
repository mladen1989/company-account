<?php
/**
 * File Template
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Model\Config\Email;

use Magento\Email\Model\ResourceModel\Template\CollectionFactory;

/**
 * Class Template
 */
class Template extends \Magento\Framework\DataObject implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Default email template identifier
     *
     * @var string
     */
    const DEFAULT_EMAIL_IDENTIFIER = 'set_password_for_new_account_email';

    /**
     * Email Templates factory
     *
     * @var \Magento\Email\Model\ResourceModel\Template\CollectionFactory
     */
    protected $templatesFactory;

    /**
     * Template constructor.
     *
     * @param \Magento\Email\Model\ResourceModel\Template\CollectionFactory $templatesFactory
     * @param array $data
     */
    public function __construct(
        CollectionFactory $templatesFactory,
        array $data = []
    ) {
        parent::__construct($data);
        $this->templatesFactory = $templatesFactory;
    }

    /**
     * Generate list of email templates
     *
     * @return array
     */
    public function toOptionArray()
    {
        /**
         * Collection
         *
         * @var $collection \Magento\Email\Model\ResourceModel\Template\Collection
         */
        $collection = $this->templatesFactory->create();
        $collection->load();
        $options = $collection->toOptionArray();
        array_unshift($options, ['value' => 'set_password_for_new_account_email', 'label' => __('Account Confirmation (Default)')]);
        return $options;
    }
}
