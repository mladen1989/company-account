<?php
/**
 * File Company
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Lambda\CompanyAccount\Model\ResourceModel\Company\CollectionFactory;

/**
 * Class CompaniesRelationInterface
 */
class Company extends \Magento\Framework\DataObject implements OptionSourceInterface
{
    /**
     * Company resource model
     *
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Company constructor.
     *
     * @param CollectionFactory $collectionFactory Companies collection
     * @param array             $data              Additional data
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        array $data = []
    ) {
        parent::__construct($data);
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }

    /**
     * Get all options (list of companies)
     *
     * @return array
     */
    public function getAllOptions()
    {
        $options = [];
        $options[] = ['label' => __('Please select Company'), 'value' => ''];
        foreach ($this->getCompanies() as $item) {
            /**
             * Company
             *
             * @var \Lambda\CompanyAccount\Model\Company $item
             */
            $options[] = [
                'value' => $item->getId(),
                'label' => $item->getName()
            ];
        }

        return $options;
    }

    /**
     * Get companies
     *
     * @return \Lambda\CompanyAccount\Model\ResourceModel\Company\Collection
     */
    protected function getCompanies()
    {
        $collection = $this->collectionFactory->create();
        return $collection->addFieldToSelect(['entity_id', 'name'])
            ->addFieldToFilter(
                \Lambda\CompanyAccount\Api\Data\CompanyInterface::STATUS,
                \Lambda\CompanyAccount\Api\Data\CompanyInterface::STATUS_ACTIVE
            );
    }
}
