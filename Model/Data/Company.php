<?php
/**
 * Copyright © Lambda Solutions. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Lambda\CompanyAccount\Model\Data;

use Lambda\CompanyAccount\Api\Data\CompanyInterface;
use Magento\Framework\Api\AbstractExtensibleObject;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Lambda\CompanyAccount\Api\Data\CompanyBillingAddressInterface;

class Company extends AbstractExtensibleObject implements CompanyInterface
{
    const IS_CUSTOM_CREATED = 'is_custom_created';

    /** @var SearchCriteriaBuilderFactory  */
    private $_searchCriteriaBuilderFactory;

    /** @var AttributeRepositoryInterface  */
    private $_attributeRepository;

    public function __construct(
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
        AttributeRepositoryInterface $attributeRepository,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $attributeValueFactory,
        $data = []
    ) {
        parent::__construct($extensionFactory, $attributeValueFactory, $data);
        $this->_searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
        $this->_attributeRepository = $attributeRepository;
    }

    /**
     * @return array
     */
    protected function getCustomAttributesCodes()
    {
        return $this->getCompanyCustomAttributesCodes();
    }

    /**
     * @return array
     */
    public function getCompanyCustomAttributesCodes(): array
    {
        $attr_codes = [];
        $searchCriteria = $this->_searchCriteriaBuilderFactory->create()->create();
        $searchResult = $this->_attributeRepository->getList(self::ENTITY_TYPE_CODE, $searchCriteria);
        foreach ($searchResult->getItems() as $attribute) {
            $attr_codes[] = $attribute->getAttributeCode();
        }
        return $attr_codes;
    }

    /**
     * Get Id.
     *
     * @return integer|null
     */
    public function getId()
    {
        return $this->_get(self::COMPANY_ID);
    }

    /**
     * Get company entity id
     *
     * @return int|null
     */
    public function getEntityTypeId()
    {
        return $this->_get(self::ENTITY_TYPE_ID);
    }

    /**
     * Get attribute set id
     *
     * @return int|null
     */
    public function getAttributeSetId()
    {
        return $this->_get(self::ATTRIBUTE_SET_ID);
    }

    /**
     * Get created at time
     *
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Get updated at time
     *
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->_get(self::UPDATED_AT);
    }

    /**
     * Get parent company id
     *
     * @return integer|null
     */
    public function getParentCompanyId()
    {
        return $this->_get(self::PARENT_COMPANY_ID);
    }

    /**
     * Get customer group id
     *
     * @return integer|null
     */
    public function getCustomerGroupId()
    {
        return $this->_get(self::CUSTOMER_GROUP_ID);
    }

    /**
     * Get status.
     *
     * @return integer|null
     */
    public function getStatus()
    {
        return $this->_get(self::STATUS);
    }

    /**
     * Get company name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Get street.
     *
     * @return string|null
     */
    public function getStreet()
    {
        return $this->_get(self::STREET);
    }

    /**
     * Get city.
     *
     * @return string|null
     */
    public function getCity()
    {
        return $this->_get(self::CITY);
    }

    /**
     * Get postcode.
     *
     * @return string|null
     */
    public function getPostcode()
    {
        return $this->_get(self::POSTCODE);
    }

    /**
     * Get country.
     *
     * @return string|null
     */
    public function getCountryId()
    {
        return $this->_get(self::COUNTRY_ID);
    }

    /**
     * Get country region id.
     *
     * @return string|null
     */
    public function getRegionId()
    {
        return $this->_get(self::REGION_ID);
    }

    /**
     * Get country region.
     *
     * @return string|null
     */
    public function getRegion()
    {
        return $this->_get(self::REGION);
    }

    /**
     * Get telephone.
     *
     * @return string|null
     */
    public function getTelephone()
    {
        return $this->_get(self::TELEPHONE);
    }

    /**
     * Get Company billing address object
     *
     * @return CompanyBillingAddressInterface
     */
    public function getCompanyBillingAddress()
    {
        return $this->_get(self::COMPANY_BILLING_ADDRESS);
    }


    /**
     * @inheritdoc
     */
    public function getSalesforceAccountId()
    {
        return $this->_get(self::SALESFORCE_ACCOUNT_ID);
    }

    /**
     * Set Id.
     *
     * @param integer $id Id
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setId($id)
    {
        return $this->setData(self::COMPANY_ID, $id);
    }

    /**
     * Set parent company id
     *
     * @param null|integer|null $parentCompanyId Parent company id
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setParentCompanyId($parentCompanyId)
    {
        return $this->setData(self::PARENT_COMPANY_ID, $parentCompanyId);
    }

    /**
     * Set customer group id
     *
     * @param integer $customerGroupId Customer group id
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setCustomerGroupId($customerGroupId)
    {
        return $this->setData(self::CUSTOMER_GROUP_ID, $customerGroupId);
    }

    /**
     * Set status.
     *
     * @param integer $status Status
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Set company name.
     *
     * @param string $name Name
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Set street.
     *
     * @param string $street Street
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setStreet($street)
    {
        return $this->setData(self::STREET, $street);
    }

    /**
     * Set city.
     *
     * @param string $city City
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setCity($city)
    {
        return $this->setData(self::CITY, $city);
    }

    /**
     * Set postcode.
     *
     * @param string $postcode Postcode
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setPostcode($postcode)
    {
        return $this->setData(self::POSTCODE, $postcode);
    }

    /**
     * Set country.
     *
     * @param string $country Country
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setCountryId($country)
    {
        return $this->setData(self::COUNTRY_ID, $country);
    }

    /**
     * Set region id.
     *
     * @param string $regionId Country
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setRegionId($regionId)
    {
        return $this->setData(self::REGION_ID, $regionId);
    }

    /**
     * Set region.
     *
     * @param string $region Country
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setRegion($region)
    {
        return $this->setData(self::REGION, $region);
    }

    /**
     * Set telephone.
     *
     * @param string $telephone Telephone
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setTelephone($telephone)
    {
        return $this->setData(self::TELEPHONE, $telephone);
    }

    /**
     * Set Company billing address object.
     *
     * @param CompanyBillingAddressInterface $billingAddress Company Billing Address
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyInterface
     */
    public function setCompanyBillingAddress($billingAddress)
    {
        return $this->setData(self::COMPANY_BILLING_ADDRESS, $billingAddress);
    }

    /**
     * @inheritdoc
     */
    public function setSalesforceAccountId($salesforceAccountId)
    {
        return $this->setData(self::SALESFORCE_ACCOUNT_ID, $salesforceAccountId);
    }

    /**
     * Set company entity Type id
     *
     * @param int $entityTypeId
     * @return $this
     */
    public function setEntityTypeId($entityTypeId)
    {
        return $this->setData(self::ENTITY_TYPE_ID, $entityTypeId);
    }

    /**
     * Set attribute set id
     *
     * @param int $attributeSetId
     * @return $this
     */
    public function setAttributeSetId($attributeSetId)
    {
        return $this->setData(self::ATTRIBUTE_SET_ID, $attributeSetId);
    }

    /**
     * Set created at time
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Set updated at time
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     *
     * @return \Lambda\CompanyAccount\Api\Data\CompanyExtensionInterface|null
     */
     public function getExtensionAttributes()
     {
         return $this->_getExtensionAttributes();
     }

    /**
     * Set an extension attributes object.
     *
     * @param \Lambda\CompanyAccount\Api\Data\CompanyExtensionInterface $extensionAttributes Extension attributes
     *
     * @return $this
     */
    public function setExtensionAttributes(
        \Lambda\CompanyAccount\Api\Data\CompanyExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}
