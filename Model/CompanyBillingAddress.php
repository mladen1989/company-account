<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Lambda\CompanyAccount\Model;

use Lambda\CompanyAccount\Api\Data\CompanyBillingAddressInterface;
use Magento\Framework\Model\AbstractModel;

class CompanyBillingAddress extends AbstractModel implements CompanyBillingAddressInterface
{

    public function _construct()
    {
        $this->_init(\Lambda\CompanyAccount\Model\ResourceModel\CompanyBillingAddress::class);
    }

    public function getCompanyBillingAddressByCompanyId($companyId)
    {
        $collection = $this->getCollection();
        $collection->addFieldToSelect('*');
        $collection->addFieldToFilter('billing_company_id', $companyId);

        return $collection->getFirstItem();
    }

    public function saveCompanyBillingAddress($companyId, $data)
    {
        $this->setCompanyBillingId($companyId);
        $this->setCompanyBillingName($data['billing_name']);
        $this->setCompanyBillingStreet($data['billing_street']);
        $this->setCompanyBillingCity($data['billing_city']);
        $this->setCompanyBillingCountryId($data['billing_country_id']);
        $this->setCompanyBillingRegionId($data['billing_region_id']);
        $this->setCompanyBillingRegion($data['billing_region']);
        $this->setCompanyBillingPostcode($data['billing_postcode']);
        $this->setCompanyBillingTelephone($data['billing_telephone']);
        $this->setCompanyBillingEmail($data['billing_email']);
        $this->setCompanyBillingEmailCc($data['billing_email_cc']);
        $this->save();
    }

    /**
     * @return int
     */
    public function getCompanyBillingId()
    {
        return $this->getData(self::BILLING_COMPANY_ID);
    }

    /**
     * @return string|null
     */
    public function getCompanyBillingName()
    {
        return $this->getData(self::BILLING_NAME);
    }

    /**
     * @return string|null
     */
    public function getCompanyBillingStreet()
    {
        return $this->getData(self::BILLING_STREET);
    }

    /**
     * @return string|null
     */
    public function getCompanyBillingCity()
    {
        return $this->getData(self::BILLING_CITY);
    }

    /**
     * @return string|null
     */
    public function getCompanyBillingCountryId()
    {
        return $this->getData(self::BILLING_COUNTRY_ID);
    }

    /**
     * @return int
     */
    public function getCompanyBillingRegionId()
    {
        return $this->getData(self::BILLING_REGION_ID);
    }

    /**
     * @return string|null
     */
    public function getCompanyBillingRegion()
    {
        return $this->getData(self::BILLING_REGION);
    }

    /**
     * @return string|null
     */
    public function getCompanyBillingPostcode()
    {
        return $this->getData(self::BILLING_POSTCODE);
    }

    /**
     * @return string|null
     */
    public function getCompanyBillingTelephone()
    {
        return $this->getData(self::BILLING_TELEPHONE);
    }

    /**
     * @return string|null
     */
    public function getCompanyBillingEmail()
    {
        return $this->getData(self::BILLING_EMAIL);
    }

    /**
     * @return string|null
     */
    public function getCompanyBillingEmailCc()
    {
        return $this->getData(self::BILLING_EMAIL_CC);
    }

    /**
     * @param int $companyid
     * @return CompanyBillingAddressInterface
     */
    public function setCompanyBillingId($companyid)
    {
        return $this->setData(self::BILLING_COMPANY_ID, $companyid);
    }

    /**
     * @param string $billingName
     * @return CompanyBillingAddressInterface
     */
    public function setCompanyBillingName($billingName)
    {
        return $this->setData(self::BILLING_NAME, $billingName);
    }

    /**
     * @param string $billingStreet
     * @return CompanyBillingAddressInterface
     */
    public function setCompanyBillingStreet($billingStreet)
    {
        return $this->setData(self::BILLING_STREET, $billingStreet);
    }

    /**
     * @param string $billingCity
     * @return CompanyBillingAddressInterface
     */
    public function setCompanyBillingCity($billingCity)
    {
        return $this->setData(self::BILLING_CITY, $billingCity);
    }

    /**
     * @param string $billingCountryId
     * @return CompanyBillingAddressInterface
     */
    public function setCompanyBillingCountryId($billingCountryId)
    {
        return $this->setData(self::BILLING_COUNTRY_ID, $billingCountryId);
    }

    /**
     * @param int $billingRegionId
     * @return CompanyBillingAddressInterface
     */
    public function setCompanyBillingRegionId($billingRegionId)
    {
        return $this->setData(self::BILLING_REGION_ID, $billingRegionId);
    }

    /**
     * @param string $billingRegion
     * @return CompanyBillingAddressInterface
     */
    public function setCompanyBillingRegion($billingRegion)
    {
        return $this->setData(self::BILLING_REGION, $billingRegion);
    }

    /**
     * @param string $billingPostcode
     * @return CompanyBillingAddressInterface
     */
    public function setCompanyBillingPostcode($billingPostcode)
    {
        return $this->setData(self::BILLING_POSTCODE, $billingPostcode);
    }

    /**
     * @param string $billingTelephone
     * @return CompanyBillingAddressInterface
     */
    public function setCompanyBillingTelephone($billingTelephone)
    {
        return $this->setData(self::BILLING_TELEPHONE, $billingTelephone);
    }

    /**
     * @param $billingEmail
     * @return CompanyBillingAddressInterface
     */
    public function setCompanyBillingEmail($billingEmail)
    {
        return $this->setData(self::BILLING_EMAIL, $billingEmail);
    }

    /**
     * @param $billingEmailCc
     * @return CompanyBillingAddressInterface
     */
    public function setCompanyBillingEmailCc($billingEmailCc)
    {
        return $this->setData(self::BILLING_EMAIL_CC, $billingEmailCc);
    }
}
