<?php
/**
 * File CustomerGroup
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Model;

use Magento\Customer\Model\Group;

/**
 * Class CustomerGroup
 */
class CustomerGroup extends Group
{
    /**
     * Customer group code attribute code
     *
     * @var string
     */
    const GROUP_CODE_ATTRIBUTE_CODE = 'customer_group_code';

    /**
     * Increased max length of the customer group code
     */
    const GROUP_CODE_MAX_LENGTH = 256;

    /**
     * Prepare customer group data
     *
     * @return $this
     */
    // @codingStandardsIgnoreStart
    protected function _prepareData()
    {// @codingStandardsIgnoreEnd
        $this->setCode(substr($this->getCode(), 0, self::GROUP_CODE_MAX_LENGTH));
        return $this;
    }
}
