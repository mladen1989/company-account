<?php
/**
 * File StoreAssignedCompanies
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Model;

use \Magento\Framework\Model\AbstractModel;
use Lambda\CompanyAccount\Api\Data\StoreAssignedCompaniesInterface;
use Lambda\CompanyAccount\Model\ResourceModel\StoreAssignedCompanies as StoreAssignedCompaniesResModel;

/**
 * Class StoreAssignedCompanies
 */
class StoreAssignedCompanies extends AbstractModel implements StoreAssignedCompaniesInterface
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    // @codingStandardsIgnoreStart
    public function _construct()
    {// @codingStandardsIgnoreEnd
        $this->_init(StoreAssignedCompaniesResModel::class);
    }

    /**
     * Get Id.
     *
     * @return integer|null
     */
    public function getId()
    {
        return $this->getData(self::RELATION_ID);
    }

    /**
     * Get store id
     *
     * @return integer
     */
    public function getStoreId()
    {
        return $this->getData(self::STORE_ID);
    }

    /**
     * Get company id
     *
     * @return integer
     */
    public function getCompanyId()
    {
        return $this->getData(self::COMPANY_ID);
    }

    /**
     * Get created at
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Get updated at
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * Set store id
     *
     * @param integer $storeId Store id
     *
     * @return \Lambda\CompanyAccount\Api\Data\StoreAssignedCompaniesInterface
     */
    public function setStoreId($storeId)
    {
        return $this->setData(self::STORE_ID, $storeId);
    }

    /**
     * Set company id
     *
     * @param integer $companyId Company id
     *
     * @return \Lambda\CompanyAccount\Api\Data\StoreAssignedCompaniesInterface
     */
    public function setCompanyId($companyId)
    {
        return $this->setData(self::COMPANY_ID, $companyId);
    }
}
