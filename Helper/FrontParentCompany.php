<?php
/**
 * File FrontParentCompany
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Helper;

use Magento\Framework\App\Helper\{AbstractHelper, Context};
use Magento\Framework\UrlInterface;
use Lambda\CompanyAccount\Model\ResourceModel\Company\CollectionFactory as CompaniesCollection;
use Lambda\CompanyAccount\Api\Data\CompanyInterface;

/**
 * Class FrontParentCompany
 */
class FrontParentCompany extends AbstractHelper
{
    /**
     * Companies collection
     *
     * @var CompaniesCollection
     */
    protected $companiesCollection;

    /**
     * Url builder
     *
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * FrontParentCompany constructor.
     *
     * @param Context             $context             Context
     * @param CompaniesCollection $companiesCollection Companies collection
     * @param UrlInterface        $urlBuilder          Url builder
     */
    public function __construct(
        Context $context,
        CompaniesCollection $companiesCollection,
        UrlInterface $urlBuilder
    ) {
        parent::__construct($context);
        $this->companiesCollection = $companiesCollection;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * If customer selected company has a TOP, as the parent, so get list of all companies
     *
     * @return array
     */
    public function getAllTopCompaniesWithTheirChildrenWithStatuses()
    {
        $companies = [];
        $companiesCollection = $this->companiesCollection->create();
        foreach ($companiesCollection as $company) {
            /**
             * @var $company \Lambda\CompanyAccount\Model\Company
             */
            $companies[$company->getId()] = [
                'value' => $company->getId(),
                'name' => $company->getName(),
                'status' => $company->getStatus()
            ];
        }
        return $companies;
    }

    /**
     * Get all children companies by parent one
     *
     * @param integer $parentCompanyId Parent company id
     *
     * @return array
     */
    public function getAllChildrenByParentCompany($parentCompanyId)
    {
        static $companies = [];
        $childrenCollection = $this->companiesCollection->create()
            ->addFieldToFilter(CompanyInterface::PARENT_COMPANY_ID, $parentCompanyId);
        foreach ($childrenCollection as $company) {
            /**
             * @var $company \Lambda\CompanyAccount\Api\Data\CompanyInterface
             */
            if (in_array($company->getId(), $companies)) {
                //case, if there are circular recursiveness
                break;
            }
            /**
             * Relation
             *
             * @var $company CompanyInterface
             */
            $companies[] = $company->getId();
            $this->getAllChildrenByParentCompany($company->getId());
        }

        return $companies;
    }

    /**
     * Get companies by their ids
     *
     * @param array $companiesIds Companies ids
     *
     * @return array
     */
    public function getCompaniesByIds(array $companiesIds)
    {
        $companiesCollection = $this->companiesCollection->create()
            ->addFieldToFilter(CompanyInterface::COMPANY_ID, ['in' => $companiesIds]);
        $result = [];
        foreach ($companiesCollection as $company) {
            /**
             * @var $company \Lambda\CompanyAccount\Model\Company
             */
            $result[$company->getId()] = [
                'value' => $company->getId(),
                'name' => $company->getName(),
                'status' => $company->getStatus(),
            ];
        }

        return $result;
    }

    /**
     * Get companies by their ids
     *
     * @param array $companiesIds Companies ids
     *
     * @return array
     */
    public function getDetailedCompaniesInfoByIds(array $companiesIds)
    {
        $companiesCollection = $this->companiesCollection->create()
            ->addFieldToFilter(CompanyInterface::COMPANY_ID, ['in' => $companiesIds]);
        $result = [];
        foreach ($companiesCollection as $company) {
            /**
             * Company
             *
             * @var \Lambda\CompanyAccount\Model\Company $company
             */
            $companyData = [
                CompanyInterface::NAME => $company->getName(),
                CompanyInterface::STATUS => $company->getStatus(),
                CompanyInterface::STREET => $company->getStreet(),
                CompanyInterface::CITY => $company->getCity(),
                CompanyInterface::POSTCODE => $company->getPostcode(),
                CompanyInterface::TELEPHONE => $company->getTelephone(),
                'edit_link' => $this->urlBuilder->getUrl('company/profile/edit', ['id' => $company->getId()]),
                'delete_link' => $this->urlBuilder->getUrl('company/profile/delete', ['id' => $company->getId()]),
            ];
            $result[] = $companyData;
        }

        return $result;
    }
}
