<?php
/**
 * Copyright © Lambda Solutions. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Lambda\CompanyAccount\Helper;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Backend\Model\Auth\Session;


class EmailSender extends \Magento\Framework\App\Helper\AbstractHelper
{

    protected TransportBuilder $transportBuilder;

    protected StoreManagerInterface $storeManager;

    protected StateInterface $inlineTranslation;

    protected $contractLearnerEmailFactory;

    /**
     * @var CustomerRepositoryInterface
     */
    protected CustomerRepositoryInterface $_customerRepository;
    private Session $session;

    /**
     * Constructor
     *
     * @param Context $context
     * @param TransportBuilder $transportBuilder
     * @param StoreManagerInterface $storeManager
     * @param StateInterface $state
     * @param CustomerRepositoryInterface $customerRepository
     * @param Session $session
     */
    public function __construct(
        Context $context,
        TransportBuilder $transportBuilder,
        StoreManagerInterface $storeManager,
        StateInterface $state,
        CustomerRepositoryInterface $customerRepository,
        Session $session
    ) {
        $this->transportBuilder = $transportBuilder;
        $this->storeManager = $storeManager;
        $this->inlineTranslation = $state;
        $this->_customerRepository = $customerRepository;
        parent::__construct($context);
        $this->session = $session;
    }

    public function sendEmail($emailTo, $companyName, $templateId)
    {
        $fromEmail = 'noreply@ifse-dev.lambdastore.net';
        $user = $this->session->getUser();

        try {
            $templateVars = [
                'name' => $companyName,
                'userName' => $user->getFirstName() . ' ' . $user->getLastName(),
            ];

            $storeId = $this->storeManager->getStore()->getId();

            $from = ['email' => $fromEmail, 'name' => 'IFSE-Admin'];
            $this->inlineTranslation->suspend();

            $templateOptions = [
                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store' => $storeId
            ];
            $transport = $this->transportBuilder->setTemplateIdentifier($templateId)
                ->setTemplateOptions($templateOptions)
                ->setTemplateVars($templateVars)
                ->setFrom($from)
                ->addTo($emailTo)
                ->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->_logger->info($e->getMessage());
        }
        return $this;
    }
}
