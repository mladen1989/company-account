<?php
/**
 * File Data
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Helper;

/**
 * Class UpgradeData
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Customer attribute to define, is customer admin of the selected company ir no
     *
     * @var string
     */
    const CUSTOMER_ATTRIBUTE_IS_COMPANY_ADMIN = 'is_company_admin';

    /**
     * Customer attribute to define, selected by customer company
     *
     * @var string
     */
    const CUSTOMER_ATTRIBUTE_COMPANY_ID = 'company_id';

    /**
     * Customer attribute to define, selected by customer company
     *
     * @var string
     */
    const CUSTOMER_ATTRIBUTE_GROUP_ID = 'group_id';

    /**
     * Available for import file formats
     *
     * @var array
     */
    const AVAILABLE_FILE_FORMATS = [
        'csv',
    ];

    /**
     * Generate Random string
     *
     * @param integer $chars Number of chars
     *
     * @return string
     */
    static public function generateRandomString($chars = 7)
    {
        return (string)substr(str_shuffle('1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz'), 0, $chars);
    }
}
