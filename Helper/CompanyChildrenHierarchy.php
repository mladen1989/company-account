<?php
/**
 * File CompanyChildrenHierarchy
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Helper;

use Magento\Framework\App\Helper\{AbstractHelper, Context};
use Lambda\CompanyAccount\Api\Data\CompanyInterface;
use Lambda\CompanyAccount\Model\ResourceModel\Company\CollectionFactory as CompaniesCollection;

/**
 * Class CompanyChildrenHierarchy
 */
class CompanyChildrenHierarchy extends AbstractHelper
{
    const MAX_CYCLE_NESTING_VALUE = 30;

    /**
     * Companies collection
     *
     * @var CompaniesCollection
     */
    protected $companiesCollection;

    /**
     * CompanyParentHierarchy constructor.
     *
     * @param Context                           $context                  Context
     * @param CompaniesCollection               $companiesCollection
     */
    public function __construct(
        Context $context,
        CompaniesCollection $companiesCollection
    ) {
        parent::__construct($context);
        $this->companiesCollection = $companiesCollection;
    }

    /**
     * Get all children sub companies customer_group_id column
     *
     * @param CompanyInterface $company Companyn
     *
     * @return array
     */
    public function getAllChildrenCompaniesCustomerGroupIdRecursively(CompanyInterface $company)
    {
        $allChildrenCompaniesCustomerGroupId = [];
        $allCompaniesId = [];
        $loadingCompaniesId = [$company->getId()];
        $cycleNumber = 0;

        while (true) {
            $childrenCompanies = $this->getChildrenCompaniesCustomerGroupId($loadingCompaniesId);
            if (empty($childrenCompanies)) {
                break;
            }
            $loadingCompaniesId = [];
            foreach ($childrenCompanies as $childrenCompany) {
                $allCompaniesId[] = $childrenCompany[CompanyInterface::COMPANY_ID];
                $allChildrenCompaniesCustomerGroupId[] = $childrenCompany[CompanyInterface::CUSTOMER_GROUP_ID];
                $loadingCompaniesId[] = $childrenCompany[CompanyInterface::COMPANY_ID];
            }
            $cycleNumber++;
            //exclude case of infinite cycling
            if ($cycleNumber > self::MAX_CYCLE_NESTING_VALUE) {
                break;
            }
        }

        return $allChildrenCompaniesCustomerGroupId;
    }

    /**
     * Get company children customer_group_id
     *
     * @param array $companyId Company id
     *
     * @return array
     */
    protected function getChildrenCompaniesCustomerGroupId($companyId)
    {
        $companiesCollection = $this->companiesCollection->create();
        return $companiesCollection->addFieldToFilter(CompanyInterface::PARENT_COMPANY_ID, ['in' => $companyId])
            ->addFieldToSelect([CompanyInterface::COMPANY_ID, CompanyInterface::CUSTOMER_GROUP_ID])
            ->toArray([CompanyInterface::COMPANY_ID, CompanyInterface::CUSTOMER_GROUP_ID]);
    }
}
