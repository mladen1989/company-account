<?php
/**
 * File CurrentStoreCompaniesList
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Lambda\CompanyAccount\Model\ResourceModel\StoreAssignedCompanies\CollectionFactory as AssignmentsCollection;
use Lambda\CompanyAccount\Model\ResourceModel\Company\CollectionFactory as CompaniesCollection;
use Lambda\CompanyAccount\Model\Company;
use Lambda\CompanyAccount\Api\Data\CompanyInterface;

/**
 * Class CreatePostPlugin
 */
class CurrentStoreCompaniesList extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Current store id
     *
     * @var integer
     */
    protected $currentStoreId;

    /**
     * Assignments collection
     *
     * @var AssignmentsCollection
     */
    protected $assignmentsCollection;

    /**
     * Companies collection
     *
     * @var CompaniesCollection
     */
    protected $companiesCollection;

    /**
     * CompaniesList constructor.
     *
     * @param AssignmentsCollection $assignmentsCollection Assignments collection
     * @param CompaniesCollection   $companiesCollection   Companies collection
     * @param StoreManagerInterface $storeManager          Current store
     * @param Context               $context               Context
     */
    public function __construct(
        AssignmentsCollection $assignmentsCollection,
        CompaniesCollection $companiesCollection,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        Context $context
    ) {
        parent::__construct($context);
        $this->currentStoreId = $storeManager->getStore()->getId();
        $this->assignmentsCollection = $assignmentsCollection;
        $this->companiesCollection = $companiesCollection;
    }

    /**
     * Get current store companies list
     *
     * @return array
     */
    public function getCurrentStoreCompanies(): array
    {
        $companiesList = [];
        if (!$this->currentStoreId) {
            return $companiesList;
        }
        $collection = $this->assignmentsCollection->create();
        $assignments = $collection->addFieldToFilter('store_id', $this->currentStoreId)
            ->addFieldToSelect(\Lambda\CompanyAccount\Api\Data\StoreAssignedCompaniesInterface::COMPANY_ID)
            ->toArray();
        if (!$assignments['totalRecords']) {
            return $companiesList;
        }
        $companiesIds = array_map(
            function ($company) {
                return $company['company_id'];
            },
            $assignments['items']
        );
        $companiesCollection = $this->companiesCollection->create();
        $companiesCollection
            ->addFieldToFilter(CompanyInterface::COMPANY_ID, ['in' => $companiesIds])
            ->addFieldToSelect(
                [
                    CompanyInterface::COMPANY_ID,
                    CompanyInterface::NAME,
                    CompanyInterface::STATUS
                ]
            );
        $companiesList = array_flip($companiesList);
        foreach ($companiesCollection as $company) {
            /**
             * Company
             *
             * @var \Lambda\CompanyAccount\Model\Company $company
             */
            $companiesList[$company->getId()] = [
                'value' => $company->getId(),
                'name' => $company->getName(),
                'status' => (int)$company->getStatus(),
            ];
        }

        return $companiesList;
    }

    /**
     * Filter companies by active status
     *
     * @return array
     */
    public function getCurrentStoreActiveCompanies(): array
    {
        $activeCompanies = [];
        foreach ($this->getCurrentStoreCompanies() as $company) {
            if ($company['status']) {
                $activeCompanies[$company['value']] = $company;
            }
        }
        return $activeCompanies;
    }
}
