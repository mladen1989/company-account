<?php
/**
 * File Customers
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Helper;

use Magento\Framework\App\Helper\{AbstractHelper, Context};
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollection;

/**
 * Class Customers
 */
class Customers extends AbstractHelper
{
    /**
     * Customer collection
     *
     * @var CustomerCollection
     */
    protected $customersCollectionFactory;

    /**
     * Customers constructor.
     *
     * @param Context            $context                    Context
     * @param CustomerCollection $customersCollectionFactory Customer collection
     */
    public function __construct(
        Context $context,
        CustomerCollection $customersCollectionFactory
    ) {
        parent::__construct($context);
        $this->customersCollectionFactory = $customersCollectionFactory;
    }

    /**
     * Get customers ids, belongs to the provided companies
     *
     * @param array $companiesIds Companies ids
     *
     * @return array
     */
    public function getCustomersIdsByCompaniesIds(array $companiesIds): array
    {
        $result = [];
        $customersCollection = $this->customersCollectionFactory->create();
        $customersCollection
            ->addAttributeToFilter(Data::CUSTOMER_ATTRIBUTE_COMPANY_ID, ['in' => $companiesIds]);
        foreach ($customersCollection as $customer) {
            /**
             * Customer
             *
             * @var $customer \Magento\Customer\Model\Customer
             */
            $result[] = $customer->getId();
        }
        return $result;
    }
}
