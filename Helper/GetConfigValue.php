<?php
declare(strict_types=1);

namespace Lambda\CompanyAccount\Helper;

class GetConfigValue extends \Magento\Framework\App\Helper\AbstractHelper
{
    public function getConfig($config_path)
    {
        return $this->scopeConfig->getValue(
            $config_path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

}
