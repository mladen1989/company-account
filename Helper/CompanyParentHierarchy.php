<?php
/**
 * File CompanyParentHierarchy
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Helper;

use Magento\Framework\App\Helper\{AbstractHelper, Context};
use Lambda\CompanyAccount\Api\Data\CompanyInterface;
use Lambda\CompanyAccount\Model\Company;
use Lambda\CompanyAccount\Model\CompanyFactory;
use Lambda\CompanyAccount\Model\ResourceModel\Company\CollectionFactory as CompaniesCollection;

/**
 * Class CompanyParentHierarchy
 */
class CompanyParentHierarchy extends AbstractHelper
{
    /**
     * Companies collection
     *
     * @var CompaniesCollection
     */
    protected $companiesCollection;

    /**
     * Company model
     *
     * @var Company
     */
    protected $companyModel;

    /**
     * CompanyParentHierarchy constructor.
     *
     * @param Context                           $context                  Context
     * @param CompaniesCollection               $companiesCollection
     * @param CompanyFactory           $companyModel
     */
    public function __construct(
        Context $context,
        CompaniesCollection $companiesCollection,
        CompanyFactory $companyModel
    ) {
        parent::__construct($context);
        $this->companiesCollection = $companiesCollection;
        $this->companyModel = $companyModel;
    }

    /**
     * Get Company by id
     *
     * @param integer $companyId Company Id
     *
     * @return CompanyInterface
     */
    public function getCompanyById($companyId): CompanyInterface
    {
        /**
         * Company model
         *
         * @var $companyModel Company
         */
        $companyModel = $this->companyModel->create();
        return $companyModel->load($companyId)->getDataModel();
    }

    /**
     * Prepare customer group name, based on hierarchy of the particular company
     *
     * @param CompanyInterface $company Company
     *
     * @return string
     */
    public function companyBelongsToCustomerGroupName(CompanyInterface $company): string
    {
        $parentCompaniesIds = [];
        $loadingCompanyId = $company->getId();
        do {
            $parentCompanyId = $this->getCompanyParent($loadingCompanyId);
            $loadingCompanyId = $parentCompanyId;
            if ($parentCompanyId) {
                if (in_array($loadingCompanyId, $parentCompaniesIds) || ($company->getId() == $loadingCompanyId)) {
                    break;
                }
                $parentCompaniesIds[$parentCompanyId] = $parentCompanyId;
            }
        } while ($parentCompanyId);

        $companiesCollection = $this->companiesCollection->create();
        $companies = $companiesCollection->addFieldToFilter(CompanyInterface::COMPANY_ID, ['in' => $parentCompaniesIds])
            ->addFieldToSelect([CompanyInterface::COMPANY_ID, CompanyInterface::NAME])
            ->toArray([CompanyInterface::COMPANY_ID, CompanyInterface::NAME]);

        if (empty($companies)) {
            return $company->getName();
        }

        foreach ($companies as $item) {
            $parentCompaniesIds[$item['entity_id']] = $item['name'];
        }

        return implode(' - ', array_reverse($parentCompaniesIds)) . ' - ' . $company->getName();
    }

    /**
     * Get company parent
     *
     * @param integer $currCompanyId Company Id
     *
     * @return integer
     */
    private function getCompanyParent($currCompanyId)
    {
        /**
         * Company model
         *
         * @var $companyModel Company
         */
        $companyModel = $this->companyModel->create();
        return $companyModel->load($currCompanyId)->getParentCompanyId();
    }
}
