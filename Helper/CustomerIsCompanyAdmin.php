<?php
/**
 * File CustomerIsCompanyAdmin
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Helper;

use Lambda\CompanyAccount\Api\Data\CompanyInterface;
use Lambda\CompanyAccount\Model\Company;
use Magento\Framework\App\Helper\{AbstractHelper, Context};
use Magento\Customer\Model\Session;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Lambda\CompanyAccount\Helper\Data;
use Lambda\CompanyAccount\Model\CompanyRepository;

/**
 * Class CustomerIsCompanyAdmin
 */
class CustomerIsCompanyAdmin extends AbstractHelper
{
    /**
     * Customer session
     *
     * @var Session
     */
    protected $customerSession;

    /**
     * Customer repository
     *
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * Admin customer company id
     *
     * @var null|integer
     */
    private $adminCompanyId;

    /**
     * Company repository
     *
     * @var CompanyRepository
     */
    protected $companyRepository;

    /**
     * CustomerIsCompanyAdmin constructor.
     *
     * @param Context                     $context            Context
     * @param Session                     $customerSession    Customer session
     * @param CustomerRepositoryInterface $customerRepository Customer repository
     * @param CompanyRepository           $companyRepository  Company repository
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        CustomerRepositoryInterface $customerRepository,
        CompanyRepository $companyRepository
    ) {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
        $this->companyRepository = $companyRepository;
        $this->isCustomerAdmin();
    }

    /**
     * Define, is customer an admin and admin of the selected company
     *
     * @return boolean
     */
    public function isCustomerAdmin(): bool
    {
        $customerId = $this->customerSession->getCustomerId();
        try {
            if (!$customerId) {
                throw new \Exception('There is no customer id');
            }
            $customer = $this->customerRepository->getById($customerId);
            $customerIsAdminAttr = $customer->getCustomAttribute(Data::CUSTOMER_ATTRIBUTE_IS_COMPANY_ADMIN);
            $customerCompanyAttr = $customer->getCustomAttribute(Data::CUSTOMER_ATTRIBUTE_COMPANY_ID);
            if (!$customerIsAdminAttr || !$customerCompanyAttr) {
                throw new \Exception('There is no customer company attribute');
            }
            if (!$customerIsAdminAttr->getValue() || !$customerCompanyAttr->getValue()) {
                throw new \Exception('There is no customer company attribute value');
            }
            $this->adminCompanyId = (int)$customerCompanyAttr->getValue();
            $company = $this->companyRepository->get($this->adminCompanyId);
            if (!$company->getId() || ($company->getId() && (CompanyInterface::STATUS_ACTIVE != $company->getStatus()))) {
                throw new \Exception('Company not exists, or not active');
            }
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * Get customer selected company
     *
     * @return null|integer
     */
    public function getAdminCompanyId()
    {
        return $this->adminCompanyId;
    }

    /**
     * Get Customer id
     *
     * @return integer|null
     */
    public function getCustomerId()
    {
        return $this->customerSession->getCustomerId();
    }
}
