<?php
/**
 * File UpgradeData
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

declare(strict_types=1);

namespace Lambda\CompanyAccount\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\App\DeploymentConfig;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Model\Config;
use Magento\Customer\Model\Customer;
use Lambda\CompanyAccount\Helper\Data;
use \Psr\Log\LoggerInterface;

/**
 * Class UpgradeData
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * Logger
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Eav setup factory
     *
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * Eav config
     *
     * @var Config
     */
    protected $eavConfig;

    /**
     * Config
     *
     * @var DeploymentConfig
     */
    protected $deploymentConfig;

    /**
     * UpgradeData constructor.
     *
     * @param EavSetupFactory          $eavSetupFactory  Eav setup factory
     * @param Config                   $eavConfig        Eav config
     * @param LoggerInterface          $logger           Logger
     * @param DeploymentConfig         $deploymentConfig Config
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        Config $eavConfig,
        LoggerInterface $logger,
        DeploymentConfig $deploymentConfig
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig = $eavConfig;
        $this->logger = $logger;
        $this->deploymentConfig = $deploymentConfig;
    }

    /**
     * Main upgrade method
     *
     * @param ModuleDataSetupInterface $setup   Setup
     * @param ModuleContextInterface   $context Context
     *
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context): void
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '0.0.2') < 0) {
            $this->upgradeVersionZeroZeroTwo($setup);
        }
        if (version_compare($context->getVersion(), '0.0.3') < 0) {
            $this->upgradeVersionZeroZeroThree($setup);
        }
        $setup->endSetup();
    }

    /**
     * Upgrade to 0.0.2 version
     *
     * @param ModuleDataSetupInterface $setup Module data setup
     *
     * @return void
     */
    protected function upgradeVersionZeroZeroTwo(ModuleDataSetupInterface $setup): void
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->removeAttribute(
            Customer::ENTITY,
            Data::CUSTOMER_ATTRIBUTE_COMPANY_ID
        );
        $eavSetup->addAttribute(
            Customer::ENTITY,
            Data::CUSTOMER_ATTRIBUTE_COMPANY_ID,
            [
                'type'         => 'int',
                'label'        => 'Company',
                'input'        => 'select',
                'source'       => 'Lambda\CompanyAccount\Model\Source\Company',
                'required'     => false,
                'visible'      => true,
                'user_defined' => false,
                'position'     => 199,
                'system'       => 0,
                'default'      => ''
            ]
        );
        $sampleAttribute = $this->eavConfig->getAttribute(
            Customer::ENTITY,
            Data::CUSTOMER_ATTRIBUTE_COMPANY_ID
        );
        $sampleAttribute->setData(
            'used_in_forms',
            ['adminhtml_customer']
        );
        $sampleAttribute->save();
        $this->logger->info('Lambda_CompanyAccount extension version has been updated to 0.0.2');
    }

    /**
     * Upgrade to 0.0.3 version
     *
     * @param ModuleDataSetupInterface $setup Module data setup
     *
     * @return void
     */
    protected function upgradeVersionZeroZeroThree(ModuleDataSetupInterface $setup): void
    {
        /**
         * Eav setup
         *
         * @var \Magento\Eav\Setup\EavSetup $eavSetup
         */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->removeAttribute(
            Customer::ENTITY,
            Data::CUSTOMER_ATTRIBUTE_IS_COMPANY_ADMIN
        );
        $eavSetup->addAttribute(
            Customer::ENTITY,
            Data::CUSTOMER_ATTRIBUTE_IS_COMPANY_ADMIN,
            [
                'type'         => 'int',
                'label'        => 'Is Admin',
                'input'        => 'boolean',
                'source'       => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'required'     => false,
                'visible'      => true,
                'user_defined' => false,
                'position'     => 200,
                'system'       => 0,
                'note'         => 'Is Admin of the selected Company',
                'default'      => ''
            ]
        );
        $attribute = $this->eavConfig->getAttribute(
            Customer::ENTITY,
            Data::CUSTOMER_ATTRIBUTE_IS_COMPANY_ADMIN
        );
        $attribute->setData(
            'used_in_forms',
            ['adminhtml_customer']
        );
        $attribute->save();
        $this->logger->info('Lambda_CompanyAccount extension version has been updated to 0.0.3');
    }
}
