<?php
/**
 * Copyright © Lambda Solutions. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Lambda\CompanyAccount\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Lambda\CompanyAccount\Api\Data\CompanyInterface;
use Psr\Log\LoggerInterface;

/**
 * Set company entity
 */
class SetCompanyEntity implements DataPatchInterface, PatchRevertableInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     *  @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        LoggerInterface $logger
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $entities = [
            CompanyInterface::ENTITY_TYPE_CODE => [
                'entity_model' => 'Lambda\CompanyAccount\Model\ResourceModel\Company',
                'attribute_model' => 'Lambda\CompanyAccount\Model\ResourceModel\Eav\Attribute',
                'table' => 'lambda_company',
                'increment_model' => null,
                'additional_attribute_table' => 'lambda_company_eav_attribute',
                'entity_attribute_collection' => 'Lambda\CompanyAccount\Model\ResourceModel\Attribute\Collection',
                'attributes' => [
                    'parent_company_id' => [
                        'type' => 'static',
                        'label' => 'Parent Company Id',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 900,
                        'visible' => false,
                        'system' => false,
                        'position' => 900,
                    ],
                    'customer_group_id' => [
                        'type' => 'static',
                        'label' => 'Customer Group Id',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 910,
                        'visible' => false,
                        'system' => false,
                        'position' => 910,
                    ],
                    'status' => [
                        'type' => 'static',
                        'label' => 'Company Status',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 915,
                        'visible' => false,
                        'system' => false,
                        'position' => 915,
                    ],
                    'name' => [
                        'type' => 'static',
                        'label' => 'Company Name',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 920,
                        'visible' => false,
                        'system' => false,
                        'position' => 920,
                    ],
                    'street' => [
                        'type' => 'static',
                        'label' => 'Company Street',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 925,
                        'visible' => false,
                        'system' => false,
                        'position' => 925,
                    ],
                    'city' => [
                        'type' => 'static',
                        'label' => 'Company City',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 930,
                        'visible' => false,
                        'system' => false,
                        'position' => 930,
                    ],
                    'postcode' => [
                        'type' => 'static',
                        'label' => 'Company Postcode',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 935,
                        'visible' => false,
                        'system' => false,
                        'position' => 935,
                    ],
                    'country_id' => [
                        'type' => 'static',
                        'label' => 'Company Country Id',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 940,
                        'visible' => false,
                        'system' => false,
                        'position' => 940,
                    ],
                    'region_id' => [
                        'type' => 'static',
                        'label' => 'Company region Id',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 945,
                        'visible' => false,
                        'system' => false,
                        'position' => 945,
                    ],
                    'region' => [
                        'type' => 'static',
                        'label' => 'Company Region',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 950,
                        'visible' => false,
                        'system' => false,
                        'position' => 950,
                    ],
                    'telephone' => [
                        'type' => 'static',
                        'label' => 'Company Telephone',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 955,
                        'visible' => false,
                        'system' => false,
                        'position' => 955,
                    ],
                    'created_at' => [
                        'type' => 'static',
                        'label' => 'Created At',
                        'input' => 'date',
                        'required' => false,
                        'sort_order' => 960,
                        'visible' => false,
                        'system' => false,
                    ]
                ]
            ],
        ];

        $eavSetup->installEntities($entities);

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function revert()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }
}
