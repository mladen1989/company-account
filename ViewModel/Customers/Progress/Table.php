<?php
declare(strict_types=1);

namespace Lambda\CompanyAccount\ViewModel\Customers\Progress;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Lambda\LearnConnector\Api\CustomerLearnConnectorInterface as LearnConnector;
use Lambda\CompanyAccount\Helper\Data;
use Lambda\CompanyAccount\Helper\FrontParentCompany;
use Lambda\CompanyAccount\Helper\CustomerIsCompanyAdmin;
use Lambda\CompanyAccount\Helper\Customers;

/**
 * Data for customers courses
 *
 * @author mladen
 */
class Table implements ArgumentInterface
{

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @var Customers
     */
    private $companyCustomersHelper;

    /**
     * @var CustomerIsCompanyAdmin
     */
    private $companyAdminHelper;

    /**
     * @var FrontParentCompany
     */
    private $frontParentCompanyHelper;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var LearnConnector
     */
    private $learnConnector;

    public function __construct(
        LearnConnector $learnConnector,
        CustomerRepositoryInterface $customerRepository,
        FrontParentCompany $frontParentCompanyHelper,
        CustomerIsCompanyAdmin $companyAdminHelper,
        Customers $companyCustomersHelper,
        ManagerInterface $messageManager
    ) {
        $this->learnConnector = $learnConnector;
        $this->customerRepository = $customerRepository;
        $this->frontParentCompanyHelper = $frontParentCompanyHelper;
        $this->companyAdminHelper = $companyAdminHelper;
        $this->companyCustomersHelper = $companyCustomersHelper;
        $this->messageManager = $messageManager;
    }

    /**
     * Get list of companies and sub companies
     *
     * @return int[]
     */
    private function getCompaniesIds(): array
    {
        $adminCompanyId = $this->companyAdminHelper->getAdminCompanyId();

        // Get list of children companies.
        $companiesIds = $this->frontParentCompanyHelper->getAllChildrenByParentCompany($adminCompanyId);

        // Add the company itself to the list
        $companiesIds[] = $adminCompanyId;

        return $companiesIds;
    }

    private function getProgressForCustomer(CustomerInterface $customer): array
    {
        $courses = [];

        try {
            $enrollments = $this->learnConnector->getEnrollmentDetails($customer);
            foreach ($enrollments as $enrollment) {
                $course = [
                    'course' => $enrollment->getName(),
                    'status' => $enrollment->getStatus(),
                    'datestarted' => $enrollment->getDateAssigned(),
                    'datecompleted' => $enrollment->getDateCompleted()
                ];
                $courses[] = $course;
            }
        } catch (\Magento\Framework\Exception\LocalizedException $ex) {
            $this->messageManager->addErrorMessage(__(
                "Could not get progress for customer %1. Reason: %2",
                $customer->getEmail(),
                $ex->getMessage()
            ));
        }

        return $courses;
    }

    public function getCustomersProgress(): array
    {

        $myId = $this->companyAdminHelper->getCustomerId();
        if (empty($myId)) {
            return [];
        }

        $progress = [];
        $companiesIds = $this->getCompaniesIds();
        $companiesData = $this->frontParentCompanyHelper->getCompaniesByIds($companiesIds);
        $customersIds = $this->companyCustomersHelper->getCustomersIdsByCompaniesIds($companiesIds);

        foreach ($customersIds as $customerId) {
            if ($customerId == $myId) {
                // Skip current user.
                continue;
            }
            $customer = $this->customerRepository->getById($customerId);
            $companyAttr = $customer->getCustomAttribute(Data::CUSTOMER_ATTRIBUTE_COMPANY_ID);
            if (!empty($companyAttr) && !empty($companyAttr->getValue())) {
                $companyId = $companyAttr->getValue();
                if (array_key_exists($companyId, $companiesData)) {
                    $companyName = $companiesData[$companyId]['name'];
                }
            }
            $data = [
                'name' => $customer->getFirstname() . ' ' . $customer->getLastname(),
                'email' => $customer->getEmail(),
                'company' => $companyName ?? '',
                'progress' => $this->getProgressForCustomer($customer)
            ];

            $progress[] = $data;
        }

        return $progress;
    }
}
