<?php
/**
 * File Status
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Ui\Component\Listing\Column;

use Lambda\CompanyAccount\Api\Data\CompanyInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Lambda\CompanyAccount\Model\Company;

/**
 * Class Status
 */
class Status extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * Status constructor.
     *
     * @param ContextInterface   $context            Context
     * @param UiComponentFactory $uiComponentFactory Ui component
     * @param array              $components         Components
     * @param array              $data               Data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource Data source
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item[$fieldName])) {
                    $item[$fieldName] = $this->setStatusLabel($item[$fieldName]);
                }
            }
        }

        return $dataSource;
    }

    /**
     * Set status label
     *
     * @param integer $key Status label
     *
     * @return string
     */
    protected function setStatusLabel($key)
    {
        $labels = [
            CompanyInterface::STATUS_DISABLED => __('Disabled'),
            CompanyInterface::STATUS_ACTIVE => __('Active')
        ];

        return $labels[$key];
    }
}
