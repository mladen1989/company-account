<?php
/**
 * File Options
 *
 * @category   Lambda
 * @package    Lambda_CompanyAccount
 * @author     Mladen Mandic <mladen.m@lambdasolutions.net>
 * @copyrights 2021 Lambda Solutions Inc.
 * @link       https://www.lambdasolutions.net/
 */

namespace Lambda\CompanyAccount\Ui\Component\Form\Field;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\RequestInterface;
use Lambda\CompanyAccount\Model\CompanyRepository;
use Lambda\CompanyAccount\Helper\FrontParentCompany;

/**
 * Class Options
 */
class Options implements OptionSourceInterface
{
    /**
     * Request
     *
     * @var RequestInterface
     */
    protected $request;

    /**
     * Company repository
     *
     * @var CompanyRepository
     */
    protected $companyRepository;

    /**
     * Filter builder
     *
     * @var FilterBuilder
     */
    protected $filterBuilder;

    /**
     * Search criteria builder
     *
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * Helper for get list of possible parent/child companies
     *
     * @var FrontParentCompany
     */
    protected $frontParentCompanyHelper;

    /**
     * Options constructor.
     *
     * @param RequestInterface      $request                  Request
     * @param CompanyRepository     $companyRepository        Company repository
     * @param FilterBuilder         $filterBuilder            Filter builder
     * @param SearchCriteriaBuilder $searchCriteriaBuilder    Search criteria builder
     * @param FrontParentCompany    $frontParentCompanyHelper Helper for get list of possible parent companies
     */
    public function __construct(
        RequestInterface $request,
        CompanyRepository $companyRepository,
        FilterBuilder $filterBuilder,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FrontParentCompany $frontParentCompanyHelper
    ) {
        $this->request = $request;
        $this->companyRepository = $companyRepository;
        $this->filterBuilder = $filterBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->frontParentCompanyHelper = $frontParentCompanyHelper;
    }

    /**
     * Get options (companies list, depends on conditions)
     *
     * @return array
     */
    public function toOptionArray()
    {
        $currentCompanyId = $this->request->getParam('id');
        if ($currentCompanyId) {
            $currentCompany = $this->companyRepository->get($currentCompanyId);
            $excludedCompaniesIds = [$currentCompanyId];
            if ($currentCompany->getParentCompanyId()) {
                $excludedCompaniesIds = array_merge(
                    $excludedCompaniesIds,
                    $this->frontParentCompanyHelper->getAllChildrenByParentCompany($currentCompanyId)
                );
            }
            $filter = $this->filterBuilder
                ->setConditionType('nin')
                ->setField('entity_id')
                ->setValue($excludedCompaniesIds)
                ->create();
            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilters([$filter])
                ->create();
        } else {
            $searchCriteria = $this->searchCriteriaBuilder->create();
        }

        $allCompanies = $this->companyRepository->getList($searchCriteria);
        $allCompanies->getItems();
        $companiesList = [];
        foreach ($allCompanies->getItems() as $company) {
            $companiesList[] = [
                'label' => $company->getName(),
                'value' => $company->getId(),
            ];
        }
        return $companiesList;
    }
}
