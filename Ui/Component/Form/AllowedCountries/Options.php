<?php
declare(strict_types=1);

namespace Lambda\CompanyAccount\Ui\Component\Form\AllowedCountries;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Directory\Helper\Data as DirectoryHelper;

class Options extends AbstractSource
{
    /** @var DirectoryHelper */
    private DirectoryHelper $directoryHelper;

    public function __construct(
        DirectoryHelper $directoryHelper
    ) {
        $this->directoryHelper = $directoryHelper;
    }

    public function getAllOptions(): ?array
    {
       return $this->directoryHelper->getCountryCollection()->toOptionArray();
    }

}
